/** @type {import('next').NextConfig} */
const { i18n } = require('./next-i18next.config');

const nextConfig = {
  reactStrictMode: true,
  i18n,
  images: {
    domains: ['171.244.51.190', '171.244.51.26', '172.16.15.12'],
  },
};

module.exports = nextConfig;
