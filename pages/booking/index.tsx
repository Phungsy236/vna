import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import { useAppDispatch, useAppSelector } from '@/shared/hooks/useRedux';
import useTrans from '@/shared/hooks/useTrans';
import { APP_THEME } from '@/shared/utils/constants/appTheme';
import { Affix, Breadcrumb, Progress, Steps } from 'antd';
// import LayoutStep1 from '@/shared/components/business/booking/step1/LayoutStep1';
// import { LayoutStepTwo } from '@/shared/components/business/booking/step2/LayoutStepTwo';
// import LayoutStep3 from '@/shared/components/business/booking/step3/LayoutStep3';
import { setStep } from '@/shared/stores/bookingSlice';
import dynamic from 'next/dynamic';

const Step1 = dynamic(() => import('@/shared/components/business/booking/step1/LayoutStep1'))
const Step2 = dynamic(() => import('@/shared/components/business/booking/step2/LayoutStepTwo').then(module => module.LayoutStepTwo))
const Step3 = dynamic(() => import('@/shared/components/business/booking/step3/LayoutStep3'))
const Step4 = dynamic(() => import('@/shared/components/business/booking/step4/LayoutStep4'))
const Step5 = dynamic(() => import('@/shared/components/business/booking/step5/LayoutStep5'))

const Booking = () => {
  const dispatch = useAppDispatch()
  const { trans } = useTrans();
  const { currentStep } = useAppSelector(state => state.bookingSlice);
  // const currentStep = 2
  const StepContent = () => {
    switch (currentStep) {
      case 1:
        return <Step1 />;
      case 2:
        return <Step2 />;
      case 3:
        return <Step3 />;
      case 4:
        return <Step4 />;
      case 5:
        return <Step5 />;
      default:
        return <Step1 />;
    }
  };
  return (
    <>
      <Affix offsetTop={0}>
        <div style={{ backgroundColor: '#fff' }}>
          <Progress
            percent={(currentStep / 5) * 100}
            strokeWidth={4}
            strokeColor={APP_THEME.color.PRIMARYCOLOR}
            showInfo={false}
          />
          <Steps
            current={currentStep - 1}
            items={[
              {
                title: trans.page.booking.step1._,
                className: 'cursor-pointer',
                onClick: () => dispatch(setStep(1))
              },
              {
                title: trans.page.booking.step2._,
                className: 'cursor-pointer',
                onClick: () => dispatch(setStep(2))
              },
              {
                title: trans.page.booking.step3._,
                className: 'cursor-pointer',
                onClick: () => dispatch(setStep(3))
              },
              {
                title: trans.page.booking.step4._,
                className: 'cursor-pointer',
                onClick: () => dispatch(setStep(4))
              },
              {
                title: trans.page.booking.step5._,
                className: 'cursor-pointer',
                onClick: () => dispatch(setStep(5))
              },
            ]}
          />
        </div>
      </Affix>
      <StepContent />
    </>
  );
};

const BreadcrumbCpn = () => {
  const { trans } = useTrans()
  return (<>
    <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.booking}</div>
    <div className='mx-6 mt-5 mobile:m-4'>
      <Breadcrumb
        items={[
          {
            title: 'Agent Portal',
          },
          {
            title: trans.menu.booking,
          },

        ]}
      />
    </div>
  </>)
}


Booking.getLayout = (children: React.ReactNode) =>
  <DashBoardLayout breadcrumb={<BreadcrumbCpn />}>{children}</DashBoardLayout>
  ;

export default Booking;
