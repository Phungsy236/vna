import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import { useLogin } from '@/shared/schema/models/IAppUser';
import { Button, Checkbox, Col, Form, Input, Row, Typography } from 'antd';
import { useRouter } from 'next/router';

const ChangePassword = () => {
  const router = useRouter();
  const doLogin = useLogin();

  function handleLogin(value: any) {
    doLogin.mutate(value);
  }

  return (
    <>
      <Row justify={"space-between"} gutter={[16, 16]} align={'middle'}>
        <Col span={24}>
          <Typography.Title level={2} className='mx-auto center'>
            Change password
          </Typography.Title>
        </Col>
        <Col span={24}>
          <Form
            layout='vertical'
            name='basic'
            style={{ maxWidth: 600 }}
            initialValues={{ remember: true }}
            onFinish={handleLogin}
            autoComplete='off'
          >
            <Form.Item
              label='Current Password'
              name='oldpassword'
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              label='New Password'
              name='newPassword1'
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              label='Confirm New Password'
              name='newPassword2'
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>

            <Row align={"middle"} gutter={16}>
              <Col span={12}>
                <Form.Item className='w-full'>
                  <Button className='w-full' type='primary' htmlType='button' onClick={() => router.push("/")}>
                    Cancel
                  </Button>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item className='w-full'>
                  <Button className='w-full' type='primary' htmlType='submit' loading={doLogin.isLoading}>
                    Submit
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </>
  );
};
ChangePassword.getLayout = (children: React.ReactNode) => <DashBoardLayout>{children}</DashBoardLayout>;
export default ChangePassword;
