import BlankLayout from '@/shared/components/layouts/BlankLayout';
import { useLogin } from '@/shared/schema/models/IAppUser';
import { Button, Checkbox, Col, Divider, Form, Input, Row, Typography } from 'antd';
import { useRouter } from 'next/router';

const ForgotPassword = () => {
  const router = useRouter();
  const doLogin = useLogin();

  function handleLogin(value: any) {
    doLogin.mutate(value);
  }

  return (
    <Row className='h-full w-full' align={'middle'} justify={'space-between'} gutter={24}>
      <Col className='m-0 h-full w-full p-0' span={12}>
        <div
          className='h-full w-full'
          style={{
            backgroundImage: `url("https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg")`,
            backgroundOrigin: 'initial',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            opacity: 1,
          }}
        ></div>
      </Col>
      <Col span={11}>
        <Row gutter={[16, 16]} align={'middle'}>
          <Col span={24}>
            <Typography.Title level={2} className='mx-auto'>
              Forgot password
            </Typography.Title>
          </Col>
          <Col span={24}>
            <Form
              name='basic'
              style={{ maxWidth: 600 }}
              initialValues={{ remember: true }}
              onFinish={handleLogin}
              autoComplete='off'
            >
              <Form.Item
                label='Username'
                name='username'
                rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input />
              </Form.Item>

              <p className='cursor-pointer' onClick={() => router.push('/login')}>
                Back to login
              </p>

              <Form.Item className='w-full'>
                <Button className='w-full' type='primary' htmlType='submit' loading={doLogin.isLoading}>
                  Submit
                </Button>
              </Form.Item>

              <Divider />
              <p>
                Enter your username or email address and we will send you instructions on how to create a new password.
              </p>
            </Form>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
ForgotPassword.getLayout = (children: React.ReactNode) => <BlankLayout>{children}</BlankLayout>;
export default ForgotPassword;
