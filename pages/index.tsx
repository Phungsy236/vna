import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useTrans from '@/shared/hooks/useTrans';
import { Breadcrumb } from 'antd';
import News from './news';

const Home = () => {
  // <News />;
};

const BreadcrumbCpn = () => {
  const { trans } = useTrans();
  return (
    <>
      <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.home}</div>
      <div className='mx-6 mt-5 mobile:m-4'>
        <Breadcrumb
          items={[
            {
              title: 'Agent Portal',
            },
            {
              title: trans.menu.home,
            },
          ]}
        />
      </div>
    </>
  );
};

Home.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout breadcrumb={<BreadcrumbCpn />}>{children}</DashBoardLayout>
);
export default Home;
