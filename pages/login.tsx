import BlankLayout from '@/shared/components/layouts/BlankLayout';
import { useLogin } from '@/shared/schema/models/IAppUser';
import { Button, Card, Checkbox, Col, Form, Input, Row, Typography } from 'antd';
import Image from 'next/image';
import { useRouter } from 'next/router';

const Login = () => {
  const router = useRouter();
  const doLogin = useLogin();

  function handleLogin(value: any) {
    doLogin.mutate(value);
  }

  return (
    <div className="relative h-full flex-col items-center justify-center md:grid lg:max-w-none lg:grid-cols-2 lg:px-0">

      <div className="relative md:h-full flex-col bg-muted p-10 text-white dark:border-r lg:flex">
        <div className="absolute inset-0 bg-zinc-900 login-background" />
        <div className="relative z-20 h-10 flex justify-start text-lg font-medium">
          {/* <Image src="/logo_vna.png" alt="Logo" fill className='object-contain position-left' /> */}
          <b>VIETNAME AIRLINE</b>
        </div>
        <div className="relative z-20 mt-auto">
          <h1 className="text-4xl font-semibold tracking-tight">
            <div className='text-yellow-400'>VIETNAM</div>
            <div>AIRLINE</div>
          </h1>
          <p className="mt-4 text-lg">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis nostrum architecto neque. Enim voluptas recusandae necessitatibus officia vero porro alias facere non atque aut, adipisci dolores sit libero asperiores explicabo.
          </p>
        </div>
        <div className="relative z-20 mt-auto">
          <p className="text-lg">Copyright &copy; VietnamAirline  2023</p>
        </div>
      </div>
      <div className="lg:p-8">
        <div className="mx-auto flex w-full flex-col justify-center space-y-6 sm:w-[350px]">
          <div className='font-bold text-3xl text-center w-full'>Đăng nhập</div>
          <Card className='shadow' size='default'>
            <Form
              name='basic'
              style={{ maxWidth: 600 }}
              initialValues={{ remember: true }}
              onFinish={handleLogin}
              autoComplete='off'
            >
              <Form.Item
                label='Username'
                name='username'
                rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input size='large' />
              </Form.Item>

              <Form.Item
                label='Password'
                name='password'
                rules={[{ required: true, message: 'Please input your password!' }]}
              >
                <Input.Password size='large' />
              </Form.Item>

              <Row gutter={16} justify={"space-between"}>
                <Col span={12}>
                  <Form.Item name='remember' valuePropName='checked'>
                    <Checkbox>Remember me</Checkbox>
                  </Form.Item>
                </Col>
                <Col className='text-right' span={12}>
                  <p onClick={() => router.push("/forgot-password")} className='m-0 p-0'>
                    Forgot password?
                  </p>
                </Col>
              </Row>

              <Form.Item className='w-full'>
                <Button className='w-full' type='primary' htmlType='submit' loading={doLogin.isLoading}>
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </div>
      </div>
    </div>
  );
};



Login.getLayout = (children: React.ReactNode) => <BlankLayout>{children}</BlankLayout>;
export default Login;