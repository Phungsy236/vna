import FormArticle from '@/shared/components/business/news/FormArticle';
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useTrans from '@/shared/hooks/useTrans';
import { URLS } from '@/shared/utils/constants/appMenu';
import { Breadcrumb } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import Link from 'next/link';

const CreateArticle = () => {
  const [formCreate] = useForm();

  return <FormArticle form={formCreate} key='create'/>;
};


const BreadcrumbCpn = () => {
  const { trans } = useTrans()
  return (<>
    <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.page.article.formName}</div>
    <div className='mx-6 mt-5 mobile:m-4'>
      <Breadcrumb
        items={[
          {
            title: 'Agent Portal',
          },
          {
            title: trans.menu.newsManagement,
          },
          {
            title: <Link href={URLS.NEWS.ARTICLE}>{trans.page.article._}</Link>,
          },
          {
            title: trans.page.article.formName,
          },

        ]}
      />
    </div>
  </>)
}

CreateArticle.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout
    breadcrumb={
      <BreadcrumbCpn/>
    }
  >
    {children}
  </DashBoardLayout>
);
export default CreateArticle;
