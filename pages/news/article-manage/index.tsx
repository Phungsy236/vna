import { PreImage } from '@/shared/components/common/PreImage';
import ExportIcon from '@/shared/components/icons/ExportIcon';
import PlusIcon from '@/shared/components/icons/PlusIcon';
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useHeaderSearch from '@/shared/hooks/useHeaderSearch';
import useTrans from '@/shared/hooks/useTrans';
import { IArticle, useChangeStatusArticle, useDeleteArticle, useGetListArticle } from '@/shared/schema/models/IArticle';
import { TIME_FORMAT } from '@/shared/utils/constants/appContants';
import { URLS } from '@/shared/utils/constants/appMenu';
import exportExcel from '@/shared/utils/functions/exportExel';
import { Breadcrumb, Button, Dropdown, Modal, Switch, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import dayjs from 'dayjs';
import { DeleteIcon, Edit2Icon, MoreVertical } from 'lucide-react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useState } from 'react';

const { confirm } = Modal;

const ArticleManage = () => {
  const router = useRouter()
  const [modal, contextHolder] = Modal.useModal();
  const { data, pageSize, tablePaginationConfig, isLoading, onChangeSearchParams, getFieldValueOnSearchParam } =
    useGetListArticle();
  const { getColumnSearchProps } = useHeaderSearch<IArticle>(onChangeSearchParams);
  const [selectedRowKey, setSelectedRowKey] = useState<React.Key>();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const useDelete = useDeleteArticle(() => Modal.destroyAll());
  const useChangeStatus = useChangeStatusArticle(() => Modal.destroyAll());


  const { trans } = useTrans();
  const columns: ColumnsType<IArticle> = [
    {
      title: 'Id',
      dataIndex: 'Id',
      key: "Id",
      render(value, record, index) {
        return <p>{index}</p>
      },
    },
    {
      title: trans.page.article.articleTitle,
      dataIndex: 'title',
      key: 'title',
      ...getColumnSearchProps({ dataIndex: 'title', fieldType: 'STRING', operation: 'LIKE' }),
      render(value, record, index) {
        return <p className='truncate w-[300px]'>{record.title}</p>
      },
    },
    {
      title: trans.page.article.description,
      dataIndex: 'description',
      width: 200,
      key: 'description',
      render(value, record, index) {
        return <p className='truncate w-[300px]'>{record.description}</p>
      },
    },
    {
      title: trans.page.article.image,
      dataIndex: 'image',
      key: 'image',
      render(value, record, index) {
        return <PreImage src={record.image} height={100} />
      },
    },
    {
      title: trans.page.article.author,
      dataIndex: 'author',
      key: 'author',
      ...getColumnSearchProps({ dataIndex: 'author', fieldType: 'STRING', operation: 'LIKE' }),
    },
    {
      title: trans.page.article.catagory,
      dataIndex: 'category',
      key: 'category',
      ...getColumnSearchProps({ dataIndex: 'category', fieldType: 'STRING', operation: 'LIKE' }),
    },
    {
      title: trans.page.article.preview,
      dataIndex: 'preview',
      key: 'preview',
      render(value, record, index) {
        return (
          <a className='cursor-pointer underline' href={record.preview} target='_blank'>
            {record.preview}
          </a>
        );
      },
    },
    {
      title: trans.common.createAt,
      dataIndex: 'createdDate',
      key: 'createdDate',
      ...getColumnSearchProps({ dataIndex: 'createdDate', fieldType: 'STRING', operation: 'LIKE' }),
      render: (value, record, index) => {
        return dayjs(value).format(TIME_FORMAT)
      },
    },
    {
      title: trans.common.updateAt,
      dataIndex: 'updatedDate',
      key: 'updatedDate',
      ...getColumnSearchProps({ dataIndex: 'updatedDate', fieldType: 'STRING', operation: 'LIKE' }),
      render: (value, record, index) => {
        return dayjs(value).format(TIME_FORMAT)
      },
    },
    {
      title: trans.common.status,
      dataIndex: 'isActive',
      key: 'isActive',
      filters: [
        {
          text: 'Active',
          value: true,
        },
        {
          text: 'InActive',
          value: false,
        },
      ],
      ...getColumnSearchProps({ dataIndex: 'isActive', fieldType: 'STRING', operation: 'LIKE' }),
      render: (value, record) => (
        <Switch
          checked={value}
          onClick={() => {
            modal.info({
              width: 600,
              footer: null,
              icon: null,
              wrapClassName: 'flex justify-center',
              content: (
                <div className='text-center'>
                  <div className=',b-2 text-2xl font-bold'>{trans.common.changeStatus}</div>
                  <div>{trans.common.notify.warningChangeStatus(trans.menu.article)}</div>
                  <div className='mt-12 flex justify-center gap-4'>
                    <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                    <Button type='primary' onClick={() => useChangeStatus.mutate({ id: record.id })}>
                    {trans.common.yes}
                    </Button>
                  </div>
                </div>
              ),
            });
          }}
        />
      ),
    },
    {
      title: trans.common.action,
      key: 'action',
      render(_value, record) {
        return (
          <div>
            <Dropdown
              menu={{
                items: [
                  {
                    key: 1,
                    label: (
                      <div
                        className='flex items-center gap-4'
                        onClick={() => {
                          router.push(`/news/article-manage/update/${record.id}`)
                          setSelectedRowKey(record.id);
                        }}
                      >
                        {' '}
                        <Edit2Icon size={16} />
                        {trans.common.edit}
                      </div>
                    ),
                  },
                  {
                    key: 2,
                    label: (
                      <div
                        className='flex items-center gap-4'
                        onClick={() => {
                          confirm({
                            title: trans.common.notify.warningDelete(trans.page.article._),
                            content: 'Some description',
                            onOk() {
                              useDelete.mutate({ listIds: [record.id as string] })
                            },
                          });
                        }}
                      >
                        <DeleteIcon size={16} /> {trans.common.delete}
                      </div>
                    ),
                  },
                ],
              }}
              placement='bottomLeft'
              arrow
            >
              <Button type='text'>
                <MoreVertical />
              </Button>
            </Dropdown>
          </div>
        );
      },
    },
  ];

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  return (
    <>
      {contextHolder}
      <div className='mb-4 flex justify-between' id='#tbCate'>
        <Button danger type='primary' disabled={selectedRowKeys.length === 0}
          onClick={() => {
            modal.error({
              width: 600,
              footer: null,
              icon: null,
              wrapClassName: 'flex justify-center',
              content: <div className='text-center'>
                <div className='font-bold text-2xl ,b-2'>{trans.common.deleteSpecific(trans.menu.article)}</div>
                <div>{trans.common.notify.warningDelete(trans.menu.article)}</div>
                <div className='mt-12 flex gap-4 justify-center'>
                  <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                  <Button type='primary' danger onClick={() => useDelete.mutate({ listIds: selectedRowKeys })}>{trans.common.yes}</Button>
                </div>
              </div>
            })
          }}>
          {trans.common.delete}
        </Button>
        <div className='flex gap-4'>
          <Button
            icon={<PlusIcon />}
            className='flex justify-between'
            type='primary'
            onClick={() => router.push("/news/article-manage/create")}
          >
            {trans.common.create}
          </Button>
          <Dropdown.Button
            icon={<ExportIcon />}
            menu={{
              items: [
                {
                  key: 1,
                  label: (
                    <div className='flex items-center gap-4'>
                      {' '}
                      <Edit2Icon size={16} />
                      {trans.common.export.all}
                    </div>
                  ),
                  onClick: () => {
                    exportExcel({ data: data?.data.content!, fileName: 'todo' });
                  },
                },
                {
                  key: 2,
                  label: (
                    <div className='flex items-center gap-4'>
                      <DeleteIcon size={16} /> {trans.common.export.currentPage}
                    </div>
                  ),
                },
                {
                  key: 3,
                  label: (
                    <div className='flex items-center gap-4'>
                      <DeleteIcon size={16} /> {trans.common.export.currentSelect}
                    </div>
                  ),
                },
              ],
            }}
            placement='bottomLeft'
            arrow
          >
            {trans.common.export._}
          </Dropdown.Button>
        </div>
      </div>
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data?.data && data?.data.content?.map(item => ({ ...item, key: item.id }))}
        pagination={tablePaginationConfig}
        scroll={{ x: 'max-content' }}
      />
      <div className='flex justify-end gap-4'>
        <Button>{trans.common.close}</Button>
      </div>
    </>
  );
};
const BreadcrumbCpn = () => {
  const { trans } = useTrans()
  return (<>
    <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.page.article._}</div>
    <div className='mx-6 mt-5 mobile:m-4'>
      <Breadcrumb
        items={[
          {
            title: 'Agent Portal',
          },
          {
            title: trans.menu.newsManagement,
          },
          {
            title: trans.page.article._,
          },
        ]}
      />
    </div>
  </>)
}


ArticleManage.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout
    breadcrumb={
      <BreadcrumbCpn />
    }
  >
    {children}
  </DashBoardLayout>
);
export default ArticleManage;
