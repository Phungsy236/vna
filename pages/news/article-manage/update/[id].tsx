import FormArticle from '@/shared/components/business/news/FormArticle';
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useTrans from '@/shared/hooks/useTrans';
import { URLS } from '@/shared/utils/constants/appMenu';
import { Breadcrumb } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { GetServerSideProps } from 'next';
import Link from 'next/link';

const UpdateArticle = ({ id }: { id: React.Key | undefined }) => {
  const [form] = useForm();
  return <>{id && <FormArticle editId={id as string} form={form} key={'update ' + id} />}</>;
};

const BreadcrumbCpn = () => {
  const { trans } = useTrans()
  return (<>
    <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.newsManagement}</div>
    <div className='mx-6 mt-5 mobile:m-4'>
      <Breadcrumb
        items={[
          {
            title: 'Agent Portal',
          },
          {
            title: <Link href={URLS.NEWS.ARTICLE}>{trans.menu.newsManagement}</Link>,
          },
          {
            title: trans.menu.updateNews,
          },

        ]}
      />
    </div>
  </>)
}

UpdateArticle.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout
    breadcrumb={
      <BreadcrumbCpn />
    }
  >
    {children}
  </DashBoardLayout>
);
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  return {
    props: {
      id: ctx.query.id
    }
  }
}
export default UpdateArticle;
