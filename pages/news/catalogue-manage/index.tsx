import { Breadcrumb, Button, Dropdown, Modal, Switch, Table} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import type { ColumnsType } from 'antd/es/table';
import { DeleteIcon, Edit2Icon, MoreVertical } from 'lucide-react';
import React, { useState } from 'react';
import useTrans from '@/shared/hooks/useTrans';
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import PlusIcon from '@/shared/components/icons/PlusIcon';
import Link from 'next/link';
import { URLS } from '@/shared/utils/constants/appMenu';
import {
  ICategory,
  useChangeStatus,
  useDeleteCategory,
  useGetAllListCategory,
} from '@/shared/schema/models/ICategory.mock';
import dayjs from 'dayjs';
import { TIME_FORMAT } from '@/shared/utils/constants/appContants';
import FormCategory from '@/shared/components/business/news/FormCategory';
import { router } from 'json-server';
import { useRouter } from 'next/router';

const { confirm } = Modal;

const CategoryManage = () => {
  const { trans } = useTrans();
  const router = useRouter();
  const [form] = useForm();
  const [modal, contextHolder] = Modal.useModal();
  const [selectedRowKey, setSelectedRowKey] = useState<React.Key>();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const { data } = useGetAllListCategory();
  const useDelete = useDeleteCategory();
  const useModifyStatus = useChangeStatus();

  //   const { getColumnSearchProps } = useHeaderSearch<IArticle>(onChangeParams);

  const columns: ColumnsType<ICategory> = [
    {
      title: 'ID',
      dataIndex: 'ID',
      key: "id",
      render(value, record, index) {
        return <p>{index}</p>
      },
    },
    {
      title: trans.menu.category,
      dataIndex: 'categoryName',
      key: 'categoryName',
    },
    {
      title: trans.page.category.creator,
      dataIndex: 'createdBy',
      key: 'createdBy',
    },
    {
      title: trans.common.createAt,
      dataIndex: 'createdDate',
      key: 'createdDate',
      render(value, record, index) {
        return <p>{dayjs(record.createdDate).format(TIME_FORMAT)}</p>;
      },
    },
    {
      title: trans.common.updateAt,
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value, record, index) {
        return <p>{dayjs(record.updatedDate).format(TIME_FORMAT)}</p>;
      },
    },
    {
      title: trans.common.status,
      dataIndex: 'active',
      key: 'active',
      render(value, record, index) {
        return (
          <Switch
            checked={value}
            onClick={() => {
              modal.info({
                width: 600,
                footer: null,
                icon: null,
                wrapClassName: 'flex justify-center',

                content: (
                  <div className='text-center'>
                    <div className=',b-2 text-2xl font-bold'>{trans.common.changeStatus}</div>
                    <div>{trans.common.notify.warningChangeStatus(trans.menu.category)}</div>
                    <div className='mt-12 flex justify-center gap-4'>
                      <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                      <Button type='primary' onClick={() => useModifyStatus.mutate(record.id)}>
                      {trans.common.yes}
                      </Button>
                    </div>
                  </div>
                ),
              });
            }}
          />
        );
      },
    },
    {
      title: trans.common.action,
      key: 'action',
      render(_value, record) {
        return (
          <div>
            <Dropdown
              menu={{
                items: [
                  {
                    key: 1,
                    label: (
                      <div
                        className='flex items-center gap-4'
                        onClick={() => {
                          modal.info({
                            width: 600,
                            footer: null,
                            icon: null,
                            wrapClassName: 'flex justify-start items-start',
                            content: (
                              <div className='text-start'>
                                <FormCategory form={form} editId={record.id as string} ></FormCategory>
                              </div>
                            ),
                          });
                        }}
                      >
                        <Edit2Icon size={16} />
                        {trans.common.edit}
                      </div>
                    ),
                  },
                  {
                    key: 2,
                    label: (
                      <div
                        className='flex items-center gap-4'
                        onClick={() => {
                          confirm({
                            title: trans.common.notify.warningDelete(trans.menu.category),
                            content: 'Some description',
                            onOk() {
                              useDelete.mutate({listCategoryIds: [record.id]});
                            },
                          });
                        }}
                      >
                        <DeleteIcon size={16} /> {trans.common.delete}
                      </div>
                    ),
                  },
                ],
              }}
              placement='bottomLeft'
              arrow
            >
              <Button type='text'>
                <MoreVertical />
              </Button>
            </Dropdown>
          </div>
        );
      },
    },
  ];

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  return (
    <>
      {contextHolder}
      <div className='mb-4 flex justify-between' id='#tbCate'>
        <Button danger type='primary' disabled={selectedRowKeys.length === 0}>
          {trans.common.delete}
        </Button>
        <div className='flex gap-4'>
          <Button
            icon={<PlusIcon className='vertial-algin' />}
            className='flex justify-between'
            type='primary'
            onClick={() => {
              modal.info({
                width: 600,
                footer: null,
                icon: null,
                wrapClassName: 'flex justify-start items-start',
                content: (
                  <div className='text-start'>
                    <FormCategory form={form} />
                  </div>
                ),
              });
            }}
          >
            {trans.common.create}
          </Button>
        </div>
      </div>
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data?.map(item => ({ ...item, key: item.id }))}
        scroll={{ x: 'max-content' }}
      />
    </>
  );
};
const BreadcrumbCpn = () => {
  const { trans } = useTrans();
  return (
    <>
      <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.page.category._}</div>
      <div className='mx-6 mt-5 mobile:m-4'>
        <Breadcrumb
          items={[
            {
              title: 'Agent Portal',
            },
            {
              title: trans.menu.newsManagement,
            },
            {
              title: trans.page.category._,
            },
          ]}
        />
      </div>
    </>
  );
};

CategoryManage.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout breadcrumb={<BreadcrumbCpn />}>{children}</DashBoardLayout>
);
export default CategoryManage;
