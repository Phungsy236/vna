import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useTrans from '@/shared/hooks/useTrans';
import { useGetAllListCategory } from '@/shared/schema/models/ICategory.mock';
import { Col, Row, Breadcrumb, Space, Affix } from 'antd';
import { useGetListArticle } from '@/shared/schema/models/IArticle';
import { TopNews } from '@/shared/components/business/news/TopNews';
import { ItemNews } from '@/shared/components/business/news/ItemNews';
import { useEffect, useState } from 'react';
import { TagCategory } from '@/shared/components/business/news/TagCategory';
import { Filter } from '@/shared/schema/typedef/ISearchParams';

const News = () => {
  const [isCategoryName, setIsCategoryName] = useState<string>('Internal News');
  const { trans } = useTrans()
  const defaultFilter: Filter[] = [
    {
      field: 'category',
      op: 'EQUAL',
      value: isCategoryName,
      fieldType: 'STRING',
    },
  ];
  const { data: news, refetch } = useGetListArticle(defaultFilter);
  const { data: listCategory } = useGetAllListCategory();
  const listCategoryActive = listCategory?.filter(category => category.active === true);
  useEffect(() => {
    refetch();
  }, [isCategoryName]);
  return (
    <>
      {news && (
        <Row gutter={24}>
          <Col md={24} lg={16}>
            <TopNews data={news.data.content.slice(0, 1)[0]} />
            <Space></Space>
            <Row gutter={[16, 16]}>
              {news.data.content.slice(1, 7).map((item, index) => {
                return (
                  <Col xs={12} sm={12} lg={8} key={index}>
                    <ItemNews data={item} />
                  </Col>
                );
              })}
            </Row>
          </Col>
          <Col md={24} lg={8}>
            <Affix offsetTop={10}>
              <div>
                <h1 className='pb-5'>{trans.menu.category}</h1>
                <Space size={[0, 8]} wrap>
                  {listCategoryActive && (
                    <TagCategory setIsCategoryName={setIsCategoryName} data={listCategoryActive} />
                  )}
                </Space>
              </div>
            </Affix>
          </Col>
        </Row>
      )}
    </>
  );
};
const BreadcrumbCpn = () => {
  const { trans } = useTrans();
  return (
    <>
      <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.news}</div>
      <div className='mx-6 mt-5 mobile:m-4'>
        <Breadcrumb
          items={[
            {
              title: 'Agent Portal',
            },
            {
              title: trans.menu.news,
            },
          ]}
        />
      </div>
    </>
  );
};

News.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout breadcrumb={<BreadcrumbCpn />}>{children}</DashBoardLayout>
);
export default News;
