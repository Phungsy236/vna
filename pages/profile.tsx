import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import { useAppSelector } from '@/shared/hooks/useRedux';
import useTrans from '@/shared/hooks/useTrans';
import { APP_SAVE_KEY } from '@/shared/utils/constants/appContants';
import { Breadcrumb, Row, Col, Form, Input } from 'antd';
import { GetServerSideProps } from 'next';

const Profile = () => {
  const { user } = useAppSelector(state => state.appSlice);
  return (
    <Row className='h-screen w-full' gutter={[16, 16]}>
      <Col className='m-0 h-full w-full p-0' span={12}>
        <div
          className=' w-full h-screen mobile:h-full'
          style={{
            backgroundImage: `url("https://el2xhw7czl.cdn.hostvn.net/Data/Sites/1/media/2021/pkd/vna-bay-den-my/637726478777307360.png")`,
            backgroundOrigin: 'initial',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            opacity: 1,
          }}
        ></div>
      </Col>
      <Col span={12}>
        <Form
          name='basic'
          layout='vertical'
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          autoComplete='off'
        >
          <Form.Item label='FullName' name='fullname'>
            <Input disabled placeholder={user?.user.fullName} />
          </Form.Item>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label='Phone Number' name='phoneNumber'>
                <Input disabled placeholder={user?.user.phoneNumber} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label='Email' name='email'>
                <Input disabled placeholder={user?.user.email} />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label='UserName' name='userName'>
            <Input disabled placeholder={user?.user?.userName} />
          </Form.Item>
          <Form.Item label='Agent' name='agent'>
            <Input disabled placeholder={user?.agentDto.agentName} />
          </Form.Item>
          <Form.Item label='TimeZone' name='timeZone'>
            <Input disabled placeholder={user?.agentDto.timeZone} />
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

const BreadcrumbCpn = () => {
  const { trans } = useTrans()
  return (<>
    <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.profile}</div>
    <div className='mx-6 mt-5 mobile:m-4'>
      <Breadcrumb
        items={[
          {
            title: 'Agent Portal',
          },
          {
            title: trans.menu.profile,
          },


        ]}
      />
    </div>
  </>)
}


Profile.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout
    breadcrumb={
      <BreadcrumbCpn
      />
    }
  >
    {children}
  </DashBoardLayout>
);
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const shouldRedirect = ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY]
  return shouldRedirect ? {
    props: {
    },

  } : {
    props: {

    },
    redirect: { permanent: true, destination: '/login' }
  }
}
export default Profile;
