import FormAgent from '@/shared/components/business/agent/FormAgent'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import useTrans from '@/shared/hooks/useTrans'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { Breadcrumb } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { ReactNode } from 'react'

const CreateAgent = () => {
    const [form] = useForm()
    const { trans } = useTrans()

    return (
        <>
            <Head>
                <title>{trans.menu.agentCreation}</title>
            </Head>
            <div className='p-4'>
                <FormAgent form={form} />
            </div>
        </>

    )
}


const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.agentCreation}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement,
                    },
                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.AGENT_MANAGE} >{trans.menu.agentManagement}</Link>,
                    },
                    {
                        title: trans.menu.agentCreation,
                    },
                ]}
            />
        </div>
    </>)
}


CreateAgent.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>
export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.AGENT_MANAGE, type: 'create' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: {},
            redirect: shouldRedirect
        }
        : { props: {} }
}

export default CreateAgent 