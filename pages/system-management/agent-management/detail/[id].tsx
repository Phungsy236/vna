import FormAgent from '@/shared/components/business/agent/FormAgent'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import useTrans from '@/shared/hooks/useTrans'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { Breadcrumb } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'

const AgentDetail = ({ editId }: { editId: React.Key | undefined }) => {
    const [form] = useForm()
    const { trans } = useTrans()
    return (
        <div>
            <Head>
                <title>{trans.menu.agentDetail}</title>
            </Head>
            <FormAgent form={form} editId={editId as string} viewOnly></FormAgent>
        </div>
    )
}


const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.agentDetail}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement,
                    },
                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.AGENT_MANAGE} >{trans.menu.agentManagement}</Link>,
                    },
                    {
                        title: trans.menu.agentDetail,
                    },
                ]}
            />
        </div>
    </>)
}
AgentDetail.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>
export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.AGENT_MANAGE, type: 'view' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect ? {
        props: {
            editId: ctx.query.id
        },
        redirect: shouldRedirect
    } : {
        props: {
            editId: ctx.query.id
        }
    }
}
export default AgentDetail