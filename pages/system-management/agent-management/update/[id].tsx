import FormAgent from '@/shared/components/business/agent/FormAgent'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import { URLS } from '@/shared/utils/constants/appMenu'
import { Breadcrumb } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import useTrans from '@/shared/hooks/useTrans'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'

const AgentUpdate = ({ editId }: { editId: React.Key | undefined }) => {

    const [form] = useForm()
    const { trans } = useTrans()
    return (
        <div>
            <Head>
                <title>{trans.menu.agentUpdation}</title>
            </Head>
            <FormAgent form={form} editId={editId as string} ></FormAgent>
        </div>
    )
}


const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.agentUpdation}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement,
                    },
                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.AGENT_MANAGE} >{trans.menu.agentManagement}</Link>,
                    },
                    {
                        title: trans.menu.agentUpdation,
                    },
                ]}
            />
        </div>
    </>)
}

AgentUpdate.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>

 

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.AGENT_MANAGE, type: 'create' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: {editId: ctx.query.id},
            redirect: shouldRedirect
        }
        : { props: {editId: ctx.query.id} }
 
}

export default AgentUpdate