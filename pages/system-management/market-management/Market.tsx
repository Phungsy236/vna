import { AbilityContext } from '@/pages/_app';
import FormMarket from '@/shared/components/business/market/FormMarket';
import FilterInputRender from '@/shared/components/common/filterTable/FilterInputRender';
import FilterTable, { InputType } from '@/shared/components/common/filterTable/FilterTable';
import useHeaderSearch from '@/shared/hooks/useHeaderSearch';
import useTrans from '@/shared/hooks/useTrans';
import {
  IMarket,
  useChangeStatusMarket,
  useDeleteMarket,
  useExportDataMarketMutation,
  useGetListMarket,
} from '@/shared/schema/models/IMarket';
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants';
import { URLS } from '@/shared/utils/constants/appMenu';
import exportExcel from '@/shared/utils/functions/exportExel';
import { checkPermission } from '@/shared/utils/functions/serverAuthen';
import { Can, useAbility } from '@casl/react';
import { Button, Dropdown, Modal, Pagination, Switch, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useForm } from 'antd/lib/form/Form';
import {
  CopyCheckIcon,
  CopySlashIcon,
  Edit2Icon,
  MoreVertical,
  SquareStackIcon,
} from 'lucide-react';
import Head from 'next/head';
import React, { useState } from 'react';

const Market = () => {
  const [form] = useForm();
  const {
    data,
    onChangeParams,
    pageSize,
    tablePaginationConfig,
    isLoading,
    onChangeSearchParams,
    getFieldValueOnSearchParam,
    onChangeMultiSearchParams
  } = useGetListMarket();
  const { getColumnSearchProps } = useHeaderSearch<IMarket>(onChangeSearchParams);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const useDelete = useDeleteMarket(() => {
    Modal.destroyAll();
    setSelectedRowKeys([]);
  });
  const useChangeStatus = useChangeStatusMarket(() => Modal.destroyAll());
  const [modal, contextHolder] = Modal.useModal();
  const { trans } = useTrans();
  const ability = useAbility(AbilityContext);
  const columns: ColumnsType<IMarket> = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Market Name',
      dataIndex: 'marketName',
      key: 'marketName',
      ellipsis: true,
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      ellipsis: true,
    },
    {
      title: 'Group Market',
      dataIndex: 'groupMarket',
      key: 'groupMarket',
      ellipsis: true,

      render(value, record, index) {
        return <p>{record && record.groupMarket ? record.groupMarket.groupMarketName : ''}</p>;
      },
    },
    {
      title: 'Created By',
      dataIndex: 'createdBy',
      key: 'createdBy',
    },
    {
      title: 'Updated By',
      dataIndex: 'updatedBy',
      key: 'updatedBy',
    },
    {
      title: 'Update At',
      dataIndex: 'updatedDate',
      key: 'updatedDate',

      ellipsis: true,
    },
    {
      title: trans.common.status,
      dataIndex: 'active',
      key: 'active',
      filters: [
        {
          text: 'Active',
          value: true,
        },
        {
          text: 'InActive',
          value: false,
        },
      ],
      render: (value, record) => (
        <Switch
          checked={value}
          onClick={() => {
            if (ability.can('create', PERMISSION_CODES.USER_MANAGE)) {
              modal.info({
                width: 600,
                footer: null,
                icon: null,
                wrapClassName: 'flex justify-center',
                content: (
                  <div className='text-center'>
                    <div className=',b-2 text-2xl font-bold'>{trans.common.changeStatus}</div>
                    <div>{trans.common.notify.warningChangeStatus(trans.page.users._)}</div>
                    <div className='mt-12 flex justify-center gap-4'>
                      <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                      <Button type='primary' onClick={() => useChangeStatus.mutate(record.id)}>
                        {trans.common.yes}
                      </Button>
                    </div>
                  </div>
                ),
              });
            }
          }}
        />
      ),
    },
    {
      title: trans.common.action,
      key: 'action',
      render(_value, record) {
        return (
          <div>
            <Dropdown
              menu={{
                items: ability.can('create', PERMISSION_CODES.USER_MANAGE)
                  ? [
                      {
                        key: 1,
                        label: (
                          <div
                            className='flex items-center gap-4'
                            onClick={() => {
                              modal.info({
                                width: 600,
                                footer: null,
                                icon: null,
                                wrapClassName: 'flex justify-start items-start',
                                content: (
                                  <div className='text-start'>
                                    <FormMarket editId={record.id} form={form}></FormMarket>
                                  </div>
                                ),
                              });
                            }}
                          >
                            {' '}
                            <Edit2Icon size={16} />
                            {trans.common.edit}
                          </div>
                        ),
                      },
                    ]
                  : []
              }}
              placement='bottomLeft'
              arrow
            >
              <Button type='text'>
                <MoreVertical />
              </Button>
            </Dropdown>
          </div>
        );
      },
    },
  ];
  const useExportExcel = useExportDataMarketMutation();
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const [inputs, setInputs] = useState<InputType[]> ([
    {
      id: 1,
      field: 'marketName',
      label: 'Market Name',
      active: true,
      inputType: 'text',
      fieldType: 'STRING',
      op: 'LIKE',
      value: getFieldValueOnSearchParam('marketName')
    },
  ])
  return (
    <>
      <Head>
        <title>Market Management</title>
      </Head>
      {contextHolder}
      <div className='mb-4 flex justify-between' id='#tbCate'>
        <Button
          danger
          type='primary'
          disabled={selectedRowKeys.length === 0}
          onClick={() => {
            // if (ability.can('delete', 'Chiu')) {
              modal.error({
                width: 600,
                footer: null,
                icon: null,
                wrapClassName: 'flex justify-center',
                content: (
                  <div className='text-center'>
                    <div className=',b-2 text-2xl font-bold'>Market Management</div>
                    <div>Market Management</div>
                    <div className='mt-12 flex justify-center gap-4'>
                      <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                      <Button type='primary' danger onClick={() => useDelete.mutate({ listIds: selectedRowKeys })}>
                        {trans.common.yes}
                      </Button>
                    </div>
                  </div>
                ),
              });
            // }
          }}
        >
          {trans.common.delete}
        </Button>
        <div className='flex gap-4'>
          <FilterTable inputs={inputs} setInputs={setInputs} />
          {/* <Can I='create' a={"I dont know"} ability={ability}> */}
          <Button
            type='primary'
            onClick={() => {
              modal.info({
                width: 600,
                footer: null,
                icon: null,
                wrapClassName: 'flex justify-start items-start',
                content: (
                  <div className='text-start'>
                    <FormMarket form={form}></FormMarket>
                  </div>
                ),
              });
            }}
          >
            {trans.common.create}
          </Button>
          {/* </Can> */}

          <Dropdown.Button
            menu={{
              items: [
                {
                  key: 1,
                  label: (
                    <div className='flex items-center gap-4'>
                      {' '}
                      <SquareStackIcon size={16} />
                      {trans.common.export.all}
                    </div>
                  ),
                  onClick: () => {
                    useExportExcel.mutate('all');
                  },
                },
                {
                  key: 2,
                  label: (
                    <div className='flex items-center gap-4'>
                      <CopySlashIcon size={16} /> {trans.common.export.currentPage}
                    </div>
                  ),
                  onClick: () => {
                    useExportExcel.mutate('current');
                  },
                },
                {
                  key: 3,
                  label: (
                    <div
                      className={`
                        flex items-center gap-4 ${selectedRowKeys.length === 0 && 'cursor-not-allowed'}`}
                    >
                      <CopyCheckIcon size={16} /> {trans.common.export.currentSelect}
                    </div>
                  ),
                  onClick: () => {
                    if (selectedRowKeys.length === 0) return;
                    else
                      exportExcel({
                        data:
                          data?.data?.content.filter(item => selectedRowKeys.some(selectId => selectId === item.id)) ||
                          [],
                        fileName: 'CA datas',
                      });
                  },
                },
              ],
            }}
            placement='bottomLeft'
            arrow
          >
            {trans.common.export._}
          </Dropdown.Button>
        </div>
      </div>
      {inputs.filter(item => item.active).length > 0 && (
            <div className='my-4 border'>
                <FilterInputRender setInputs={setInputs} inputs={inputs} searchFunction={(value) => onChangeMultiSearchParams(value)} />
            </div>
        )}
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data?.data?.content?.map(item => ({ ...item, key: item.id }))}
        pagination={false}
        scroll={{ x: 'max-content' }}
        loading={isLoading}
      />
      <div className='mt-4 flex justify-between px-4'>
        <div>
          {`Showing ${tablePaginationConfig.total || 0 < pageSize ? tablePaginationConfig.total : pageSize} of ${
            tablePaginationConfig.total
          } elements`}
        </div>
        <Pagination {...tablePaginationConfig} />
      </div>
    </>
  );
};
export default Market;
