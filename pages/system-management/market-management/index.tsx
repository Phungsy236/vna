import React from 'react';
import { Breadcrumb, Tabs } from 'antd';
import type { TabsProps } from 'antd';
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useTrans from '@/shared/hooks/useTrans';
import GroupMarket from './GroupMarket';
import { useRouter } from 'next/router';
import Market from './Market';

const onChange = (key: string) => {
  console.log(key);
};

const items: TabsProps['items'] = [
  {
    key: '1',
    label: `Market`,
    children: <Market/>
  },
  {
    key: '2',
    label: `Group Market`,
    children: <GroupMarket></GroupMarket>,
  },
];
const MarketManagement = () => {
  const router = useRouter();
  
  return <Tabs items={items} onChange={onChange} />;
};

const BreadcrumbCpn = () => {
  const { trans } = useTrans();
  return (
    <>
      <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>Market Management</div>
      <div className='mx-6 mt-5 mobile:m-4'>
        <Breadcrumb
          items={[
            {
              title: 'Agent Portal',
            },
            {
              title: trans.menu.systemManagement,
            },
            {
              title: 'Market Management',
            },
          ]}
        />
      </div>
    </>
  );
};

MarketManagement.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout breadcrumb={<BreadcrumbCpn />}>{children}</DashBoardLayout>
);

export default MarketManagement;
