import FormRole from '@/shared/components/business/role/FormRole'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import { useAppDispatch, useAppSelector } from '@/shared/hooks/useRedux'
import useTrans from '@/shared/hooks/useTrans'
import { useGetAllActionPermissions } from '@/shared/schema/models/IRole'
import { setCatePermissions } from '@/shared/stores/roleSlice'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { Breadcrumb } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { useEffect } from 'react'

const CreateRole = () => {
    const [form] = useForm()
    const { trans } = useTrans()
    const dispatch = useAppDispatch()
    const reduxData = useAppSelector(state => state.roleSlice.catePermissions)
    const { data: dataTableRole } = useGetAllActionPermissions({ enabled: !!reduxData })
    useEffect(() => {
        if (dataTableRole) dispatch(setCatePermissions(dataTableRole))
    }, [dataTableRole, dispatch])
    return (<>
        <Head>
            <title>{trans.menu.roleCreate}</title>
        </Head>
        <FormRole form={form} />
    </>
    )
}
const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.roleCreate}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement
                    },

                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.ROLE_MANAGE} >{trans.menu.roleManagement}</Link>,
                    },
                    {
                        title: trans.menu.roleCreate,
                    },

                ]}
            />
        </div>
    </>)
}
CreateRole.getLayout = (children: React.ReactNode,) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>
export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.ROLE_MANAGE, type: 'create' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: {},
            redirect: shouldRedirect
        }
        : { props: {} }
}

export default CreateRole 