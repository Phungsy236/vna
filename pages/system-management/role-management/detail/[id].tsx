import FormRole from '@/shared/components/business/role/FormRole'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import useTrans from '@/shared/hooks/useTrans'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { Breadcrumb } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'

const RoleDetail = ({ id }: { id: React.Key | undefined }) => {
    const { trans } = useTrans()
    const [form] = useForm()
    return (
        <div>
            <Head>
                <title>{trans.menu.roleDetail}</title>
            </Head>
            <FormRole form={form} editId={id as string} viewOnly></FormRole>
        </div>
    )
}
const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.roleDetail}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement
                    },

                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.ROLE_MANAGE} >{trans.menu.roleManagement}</Link>,
                    },
                    {
                        title: trans.menu.roleDetail,
                    },

                ]}
            />
        </div>
    </>)
}
RoleDetail.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.ROLE_MANAGE, type: 'view' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: { id: ctx.query.id },
            redirect: shouldRedirect
        }
        : { props: { id: ctx.query.id } }

}

export default RoleDetail