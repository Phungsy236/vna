import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useHeaderSearch from '@/shared/hooks/useHeaderSearch';
import useTrans from '@/shared/hooks/useTrans';
import { IRoleList, useGetAllActionPermissions, useGetListRoles } from '@/shared/schema/models/IRole';
import { URLS } from '@/shared/utils/constants/appMenu';
import { APP_SAVE_KEY, PERMISSION_CODES, TIME_FORMAT } from '@/shared/utils/constants/appContants';
import { Breadcrumb, Button, Dropdown, Pagination, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import dayjs from 'dayjs';
import { ClipboardListIcon, Edit2Icon, MoreVertical } from 'lucide-react';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { checkPermission } from '@/shared/utils/functions/serverAuthen';
import { Can, useAbility } from '@casl/react';
import { AbilityContext } from '@/pages/_app';
import { useDispatch } from 'react-redux';
import { setCatePermissions } from '@/shared/stores/roleSlice';
import FilterTable, { InputType } from '@/shared/components/common/filterTable/FilterTable';
import FilterInputRender from '@/shared/components/common/filterTable/FilterInputRender';

const Role = () => {
  const { data, pageSize, tablePaginationConfig, isLoading, onChangeSearchParams, getFieldValueOnSearchParam, onChangeMultiSearchParams } = useGetListRoles()
  const { getColumnSearchProps } = useHeaderSearch<IRoleList>(onChangeSearchParams)
  const router = useRouter()
  const { trans } = useTrans()
  const ability = useAbility(AbilityContext)

  const dispatch = useDispatch()
  const { data: dataTableRole } = useGetAllActionPermissions()
  useEffect(() => {
    if (dataTableRole) dispatch(setCatePermissions(dataTableRole))
  }, [dataTableRole, dispatch])

  const columns: ColumnsType<IRoleList> = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: trans.page.role.roleName,
      dataIndex: 'roleName',
      key: 'roleName',
    },
    {
      title: trans.page.role.description,
      dataIndex: 'description',
      key: 'description',
    },

    {
      title: trans.common.updateAt,
      dataIndex: 'updatedDate',
      key: 'updatedDate',
      render: (value, record, index) => {
        return dayjs(value).format(TIME_FORMAT)
      },

    },
    {
      title: trans.common.action,
      key: 'action',
      render(_value, record) {
        return <div>
          <Dropdown menu={{
            items: ability.can('create', PERMISSION_CODES.ROLE_MANAGE) ? [
              {
                key: 1, label: <div className='flex items-center gap-4' onClick={() => {
                  router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE + '/update/' + record.id)
                }}> <Edit2Icon size={16} />{trans.common.edit}</div>
              },
              {
                key: 2, label: <div className='flex items-center gap-4' onClick={() => {
                  router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE + '/detail/' + record.id)
                }} ><ClipboardListIcon size={16}></ClipboardListIcon> {trans.common.detail}</div>
              },
            ] :
              [
                {
                  key: 2, label: <div className='flex items-center gap-4' onClick={() => {
                    router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE + '/detail/' + record.id)
                  }} ><ClipboardListIcon size={16}></ClipboardListIcon> {trans.common.detail}</div>
                },
              ]
          }} placement="bottomLeft" arrow>
            <Button type='text'>
              <MoreVertical />
            </Button>
          </Dropdown>
        </div>
      },
    }
  ];
  const [inputs, setInputs] = useState<InputType[]>([
    {
      id: 1,
      field: 'roleName',
      label: 'Tên vai trò',
      active: true,
      inputType: 'text',
      fieldType: 'STRING',
      op: 'LIKE',
      value: getFieldValueOnSearchParam('roleName')
    }
  ])
  return (
    <>
      <Head>
        <title>{trans.menu.roleManagement}</title>
      </Head>
      <div className='flex justify-end mb-4' id='#tbCate'>
        <div className='flex gap-4'>
          <FilterTable inputs={inputs} setInputs={setInputs} />
          <Can I='create' a={PERMISSION_CODES.ROLE_MANAGE} ability={ability}>
            <Button type='primary'
              onClick={() => {
                router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE + '/create')
              }}
            >{trans.common.create}</Button>

          </Can>

          <Button onClick={() => router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE + '/permissions')}>{trans.page.role.viewPermission}</Button>
        </div>
      </div>
      {inputs.filter(item => item.active).length > 0 && (
        <div className='my-4 border'>
          <FilterInputRender setInputs={setInputs} inputs={inputs} searchFunction={(value) => onChangeMultiSearchParams(value)} />
        </div>
      )}
      <Table columns={columns} dataSource={data?.data.content?.map(item => ({ ...item, key: item.id }))} pagination={false} scroll={{ x: 'max-content' }} loading={isLoading || !router.isReady} rowKey={record => record.id} />

      <div className='mt-4 flex justify-between px-4'>
        <div>{`Showing ${tablePaginationConfig.total || 0 < pageSize ? tablePaginationConfig.total : pageSize} of ${tablePaginationConfig.total} elements`}
        </div>
        <Pagination {...tablePaginationConfig} />
      </div>
    </>
  )
};
const BreadcrumbCpn = () => {
  const { trans } = useTrans()
  return (<>
    <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.roleManagement}</div>
    <div className='mx-6 mt-5 mobile:m-4'>
      <Breadcrumb
        items={[
          {
            title: 'Agent Portal',
          },
          {
            title: trans.menu.systemManagement
          },
          {
            title: trans.menu.roleManagement,
          },

        ]}
      />
    </div>
  </>)
}
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.ROLE_MANAGE, type: 'view' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
  return shouldRedirect
    ? {
      props: {},
      redirect: shouldRedirect
    }
    : { props: {} }
}

Role.getLayout = (children: React.ReactNode) => <DashBoardLayout
  breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>
export default Role;
