import { AbilityContext } from '@/pages/_app';
import FilterInputRender from '@/shared/components/common/filterTable/FilterInputRender';
import FilterTable, { InputType } from '@/shared/components/common/filterTable/FilterTable';
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import useHeaderSearch from '@/shared/hooks/useHeaderSearch';
import useTrans from '@/shared/hooks/useTrans';
import { IAgent, useChangeStatusAgent, useDeleteSubAgent, useExportDataSubAgentMutation, useGetListSubAgent } from '@/shared/schema/models/IAgent';
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants';
import { URLS } from '@/shared/utils/constants/appMenu';
import exportExcel from '@/shared/utils/functions/exportExel';
import { checkPermission } from '@/shared/utils/functions/serverAuthen';
import { Can, useAbility } from '@casl/react';
import { Breadcrumb, Button, Dropdown, Modal, Pagination, Switch, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { ClipboardListIcon, CopyCheckIcon, CopySlashIcon, Edit2Icon, MoreVertical, SquareStackIcon } from 'lucide-react';
import { GetServerSideProps } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React, { useState } from 'react';


const Agent = () => {
    const { data, onChangeParams, pageSize, tablePaginationConfig, isLoading, onChangeSearchParams, getFieldValueOnSearchParam, onChangeMultiSearchParams } = useGetListSubAgent()
    const { getColumnSearchProps } = useHeaderSearch<IAgent>(onChangeSearchParams)
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const useDelete = useDeleteSubAgent(() => {
        Modal.destroyAll()
        setSelectedRowKeys([])
    })
    const useChangeStatus = useChangeStatusAgent(() => Modal.destroyAll())
    const [modal, contextHolder] = Modal.useModal()
    const router = useRouter()
    const { trans } = useTrans()
    const ability = useAbility(AbilityContext)
    const columns: ColumnsType<IAgent> = [
        {
            title: 'id',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: trans.page.agent.subAgent,
            dataIndex: 'agentName',
            key: 'agentName',
            ellipsis: true,
            filteredValue: getFieldValueOnSearchParam('agentName'),
            ...getColumnSearchProps({ dataIndex: 'agentName', fieldType: 'STRING', operation: 'LIKE', }),

        },
        {
            title: trans.page.agent.country,
            dataIndex: 'country',
            key: 'country',
            ellipsis: true,
            filteredValue: getFieldValueOnSearchParam('country'),
            ...getColumnSearchProps({ dataIndex: 'country', fieldType: 'STRING', operation: 'LIKE', }),

        },
        {
            title: trans.page.agent.area,
            dataIndex: 'registrationArea',
            key: 'registrationArea',
            filteredValue: getFieldValueOnSearchParam('registrationArea'),
            ...getColumnSearchProps({ dataIndex: 'registrationArea', fieldType: 'STRING', operation: 'LIKE' }),
        },
        {
            title: trans.page.agent.timeZone,
            dataIndex: 'timeZone',
            key: 'timeZone',
            ellipsis: true,
            filteredValue: getFieldValueOnSearchParam('timeZone'),

            ...getColumnSearchProps({ dataIndex: 'timeZone', fieldType: 'STRING', operation: 'LIKE' }),
        },
        {
            title: trans.page.agent.minLimit,
            dataIndex: 'minimumLimit',
            key: 'minimumLimit',
            filteredValue: getFieldValueOnSearchParam('minimumLimit'),

            ellipsis: true,
            ...getColumnSearchProps({ dataIndex: 'minimumLimit', fieldType: 'INTEGER', operation: 'EQUAL' }),
        },
        {
            title: trans.page.agent.maxLimit,
            dataIndex: 'maximumLimit',
            key: 'maximumLimit',
            ellipsis: true,
            filteredValue: getFieldValueOnSearchParam('maximumLimit'),
            ...getColumnSearchProps({ dataIndex: 'maximumLimit', fieldType: 'INTEGER', operation: 'EQUAL' }),
        },

        {
            title: trans.common.updateAt,
            dataIndex: 'updatedDate',
            key: 'updatedDate',
            ...getColumnSearchProps({ dataIndex: 'updatedDate', isDate: true, fieldType: 'DATE', operation: 'EQUAL' }),

        },
        {
            title: trans.common.status,
            dataIndex: 'active',
            key: 'status',
            filters: [{
                text: "Active",
                value: true
            },
            {
                text: "InActive",
                value: false
            }
            ],
            ...getColumnSearchProps({ dataIndex: 'active', fieldType: 'BOOLEAN', operation: 'EQUAL' }),
            filteredValue: getFieldValueOnSearchParam('active'),

            render: (value, record) => <Switch checked={value} onClick={() => {
                if (ability.can('create', PERMISSION_CODES.SUBAGENT_MANAGE)) {
                    modal.info({
                        width: 600,
                        footer: null,
                        icon: null,
                        wrapClassName: 'flex justify-center',

                        content: <div className='text-center'>
                            <div className='font-bold text-2xl ,b-2'>{trans.common.changeStatus}</div>
                            <div>{trans.common.notify.warningChangeStatus(trans.page.agent._)}</div>
                            <div className='mt-12 flex gap-4 justify-center'>
                                <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                                <Button type='primary' onClick={() => useChangeStatus.mutate(record.id)}>{trans.common.yes}</Button>
                            </div>
                        </div>
                    })
                }

            }} />

        },

        {
            title: trans.common.action,
            key: 'action',
            render(_value, record) {
                return <div>
                    <Dropdown menu={{
                        items: ability.can('create', PERMISSION_CODES.SUBAGENT_MANAGE) ? [
                            {
                                key: 1, label: <div className='flex items-center gap-4' onClick={() => {
                                    router.push(URLS.SYSTEM_MANAGE.SUB_AGENT_MANAGE + '/update/' + record.id)
                                }}> <Edit2Icon size={16} />{trans.common.edit}</div>
                            },
                            {
                                key: 2, label: <div className='flex items-center gap-4' onClick={() => {
                                    router.push(URLS.SYSTEM_MANAGE.SUB_AGENT_MANAGE + '/detail/' + record.id)
                                }} ><ClipboardListIcon size={16} /> {trans.common.detail}</div>
                            },
                        ] : [

                            {
                                key: 2, label: <div className='flex items-center gap-4' onClick={() => {
                                    router.push(URLS.SYSTEM_MANAGE.SUB_AGENT_MANAGE + '/detail/' + record.id)
                                }} ><ClipboardListIcon size={16} /> {trans.common.detail}</div>
                            },
                        ]
                    }} placement="bottomLeft" arrow>
                        <Button type='text'>
                            <MoreVertical />
                        </Button>
                    </Dropdown>
                </div>
            },

        }
    ];
    const useExportExcel = useExportDataSubAgentMutation()
    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        setSelectedRowKeys(newSelectedRowKeys);
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,

    };
    const [inputs, setInputs] = useState<InputType[]> ([
        {
            id: 1,
            field: 'agentName',
            label: 'Tên đại lý',
            active: true,
            inputType: 'text',
            fieldType: 'STRING',
            op: 'LIKE',
            value: getFieldValueOnSearchParam('agentName')
        },
        {
            id: 2,
            field: 'country',
            label: 'Quốc gia',
            active: true,
            inputType: 'text',
            fieldType: 'STRING',
            op: 'LIKE',
            value: getFieldValueOnSearchParam('country')
        },
        {
            id: 3,
            field: 'registrationArea',
            label: 'Khu vực',
            active: true,
            inputType: 'text',
            fieldType: 'STRING',
            op: 'LIKE',
            value: getFieldValueOnSearchParam('registrationArea')
        },
    ])
    return (
        <>
            <Head>
                <title>{trans.menu.subAgentManagement}</title>
            </Head>
            {contextHolder}
            <div className='flex justify-between mb-4' id='#tbCate'>
                <Button danger type='primary' disabled={selectedRowKeys.length === 0} onClick={() => {
                    if (ability.can('delete', PERMISSION_CODES.SUBAGENT_MANAGE)) {
                        modal.error({
                            width: 600,
                            footer: null,
                            icon: null,
                            wrapClassName: 'flex justify-center',
                            content: <div className='text-center'>
                                <div className='font-bold text-2xl ,b-2'>{trans.common.deleteSpecific(trans.page.agent.subAgent)}</div>
                                <div>{trans.common.notify.warningDelete(trans.page.agent.subAgent)}</div>
                                <div className='mt-12 flex gap-4 justify-center'>
                                    <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                                    <Button type='primary' danger onClick={() => useDelete.mutate({ listIds: selectedRowKeys })}>{trans.common.yes}</Button>
                                </div>
                            </div>
                        })
                    }

                }}>{trans.common.delete}</Button>
                <div className='flex gap-4'>
                    <FilterTable inputs={inputs} setInputs={setInputs} />
                    <Can I='create' a={PERMISSION_CODES.SUBAGENT_MANAGE} ability={ability}>
                        <Button type='primary'
                            onClick={() => router.push(URLS.SYSTEM_MANAGE.SUB_AGENT_MANAGE + '/create')
                            }
                        >{trans.common.create}</Button>
                    </Can>

                    <Dropdown.Button menu={{
                        items: [
                            {
                                key: 1,
                                label: <div className='flex items-center gap-4'> <SquareStackIcon size={16} />{trans.common.export.all}</div>,
                                onClick: () => {
                                    useExportExcel.mutate('all')
                                }
                            },
                            {
                                key: 2,
                                label: <div className='flex items-center gap-4'><CopySlashIcon size={16} /> {trans.common.export.currentPage}</div>,
                                onClick: () => {
                                    useExportExcel.mutate('current')
                                }
                            },
                            {
                                key: 3,
                                label: <div className={`
                                flex items-center gap-4 ${selectedRowKeys.length === 0 && 'cursor-not-allowed'}`}  ><CopyCheckIcon size={16}
                                    /> {trans.common.export.currentSelect}</div>,
                                onClick: () => {
                                    if (selectedRowKeys.length === 0) return
                                    else
                                        exportExcel({
                                            data: data?.data?.content.filter(item => selectedRowKeys.some(selectId => selectId === item.id)) || [],
                                            fileName: 'CA datas'
                                        })
                                }
                            },
                        ]
                    }} placement="bottomLeft" arrow>
                        {trans.common.export._}
                    </Dropdown.Button>
                </div>
            </div>
            {inputs.filter(item => item.active).length > 0 && (
                <div className='my-4 border'>
                    <FilterInputRender setInputs={setInputs} inputs={inputs} searchFunction={(value) => onChangeMultiSearchParams(value)} />
                </div>
            )}
            <Table rowSelection={rowSelection} columns={columns} dataSource={data?.data?.content?.map(item => ({ ...item, key: item.id }))} pagination={false} scroll={{ x: 'max-content' }} loading={isLoading} />
            <div className='mt-4 flex justify-between px-4'>
                <div>{`Showing ${tablePaginationConfig.total || 0 < pageSize ? tablePaginationConfig.total : pageSize} of ${tablePaginationConfig.total} elements`}
                </div>
                <Pagination {...tablePaginationConfig} />
            </div>
        </>
    )
};

const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.subAgentManagement}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement
                    },
                    {
                        title: trans.menu.subAgentManagement,
                    },
                ]}
            />
        </div>
    </>)
}
export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.SUBAGENT_MANAGE, type: 'view' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: {},
            redirect: shouldRedirect
        }
        : { props: {} }
}

Agent.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>

export default Agent;
