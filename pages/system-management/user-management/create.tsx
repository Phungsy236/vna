import FormUser from '@/shared/components/business/user/FormUser'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import useTrans from '@/shared/hooks/useTrans'
import { useCreateUser } from '@/shared/schema/models/IAppUser'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { Affix, Breadcrumb, Button, Form } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import { GetServerSideProps } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { ReactNode } from 'react'

const CreateUser = () => {
    const [form] = useForm()
    const router = useRouter()
    const { trans } = useTrans()
    const createAccount = useCreateUser(() => router.push(URLS.SYSTEM_MANAGE.USER_MANAGE));
    function onFinish(value: any) {
        createAccount.mutate(value)
    }
    return (
        <div className='w-full p-4'>
            <Form
                autoComplete='false'
                preserve
                form={form}
                onFinish={onFinish}
                layout='vertical'
                labelWrap
                size='large'
                colon={false}
                scrollToFirstError
                className='grid grid-cols-3 gap-x-20 mobile:grid-cols-1 ipad:grid-cols-2'
            >

                <Form.Item className='col-span-3 mobile:col-span-1 ipad:col-span-2 '>
                    <Affix offsetTop={10}>
                        <div className='flex justify-between my-2'>
                            <div className='text-2xl font-bold'>Create user</div>
                            <div className='space-x-2'>
                                <Button
                                    type='primary'
                                    htmlType='submit'
                                    className='mr-2 !rounded-none'
                                    loading={createAccount.isLoading}
                                >
                                    {trans.common.save}
                                </Button>
                                <Button
                                    htmlType='reset'
                                    onClick={() => {
                                        form.resetFields();
                                        router.push(URLS.SYSTEM_MANAGE.USER_MANAGE);
                                    }}
                                    className='!rounded-none'
                                >
                                    {trans.common.close}
                                </Button>
                            </div>
                        </div>
                    </Affix>
                </Form.Item>


                <FormUser form={form} />
            </Form>
        </div>
    )
}

const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.userCreate}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement
                    },
                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.USER_MANAGE}>{trans.menu.userManagement}</Link>
                    },
                    {
                        title: trans.menu.userCreate,
                    },
                ]}
            />
        </div>
    </>)
}
export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.USER_MANAGE, type: 'create' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: {},
            redirect: shouldRedirect
        }
        : { props: {} }
}


CreateUser.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>
export default CreateUser 