import FormUser from '@/shared/components/business/user/FormUser'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import useTrans from '@/shared/hooks/useTrans'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { Affix, Breadcrumb, Button, Form } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import { GetServerSideProps } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'


const UserDetail = ({ id }: { id: React.Key | undefined }) => {
    const [form] = useForm()
    const router = useRouter()
    const { trans } = useTrans()
    return (
        <div className='w-full p-4'>
            <Affix offsetTop={10}>
                <div className='flex justify-between my-2'>
                    <div className='text-2xl font-bold'>Detail user</div>
                    <div className='space-x-2 text-right'>
                        <Button
                            onClick={() => {
                                router.push(URLS.SYSTEM_MANAGE.USER_MANAGE);
                            }}
                        >
                            {trans.common.close}
                        </Button>
                        <Button
                            type='primary'
                            onClick={() => {
                                router.push(URLS.SYSTEM_MANAGE.USER_MANAGE + '/update/' + id);
                            }}
                        >
                            {trans.common.edit}
                        </Button>
                    </div>
                </div>

            </Affix>
            <Form
                autoComplete='false'
                preserve
                form={form}
                // onFinish={onFinish}
                layout='vertical'
                disabled
                labelWrap
                size='large'
                colon={false}
                scrollToFirstError
                className='grid grid-cols-3 gap-x-20 mobile:grid-cols-1 ipad:grid-cols-2'
            >
                <FormUser isOnlyView form={form} editId={id} />
            </Form>
        </div>
    )
}


const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.userDetail}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement
                    },
                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.USER_MANAGE}>{trans.menu.userManagement}</Link>
                    },
                    {
                        title: trans.menu.userDetail,
                    },
                ]}
            />
        </div>
    </>)
}

UserDetail.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>  {children}</ DashBoardLayout>
export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.USER_MANAGE, type: 'view' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: { id: ctx.query.id },
            redirect: shouldRedirect
        }
        : { props: { id: ctx.query.id } }
}
export default UserDetail