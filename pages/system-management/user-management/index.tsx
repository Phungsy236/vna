import useHeaderSearch from '@/shared/hooks/useHeaderSearch';
import exportExcel from '@/shared/utils/functions/exportExel';
import { Breadcrumb, Button, Dropdown, Modal, Pagination, Switch, Table, Tag } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { ClipboardListIcon, DeleteIcon, Edit2Icon, MoreVertical } from 'lucide-react';
import React, { useState } from 'react';
import useTrans from '@/shared/hooks/useTrans';
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout';
import { IAppUser, useChangeStatusUser, useDeleteUser, useGetListUser } from '@/shared/schema/models/IAppUser';
import { useRouter } from 'next/router';
import { URLS } from '@/shared/utils/constants/appMenu';
import dayjs from 'dayjs';
import { APP_SAVE_KEY, PERMISSION_CODES, TIME_FORMAT } from '@/shared/utils/constants/appContants';
import { GetServerSideProps } from 'next';
import { checkPermission } from '@/shared/utils/functions/serverAuthen';
import { Can, useAbility } from '@casl/react';
import { AbilityContext } from '@/pages/_app';
import FilterTable, { InputType } from '@/shared/components/common/filterTable/FilterTable';
import FilterInputRender from '@/shared/components/common/filterTable/FilterInputRender';
import { useGetBasicActiveListRole } from '@/shared/schema/models/IRole';

const Accounts = () => {
  const { data, pageSize, tablePaginationConfig, isLoading, onChangeSearchParams, getFieldValueOnSearchParam, onChangeMultiSearchParams } =
    useGetListUser();
  const { getColumnSearchProps } = useHeaderSearch<IAppUser>(onChangeSearchParams);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const useDelete = useDeleteUser();
  const [modal, contextHolder] = Modal.useModal();
  const { trans } = useTrans();
  const useChangeStatus = useChangeStatusUser(() => Modal.destroyAll());
  const router = useRouter();
  const ability = useAbility(AbilityContext);
  const columns: ColumnsType<IAppUser> = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: trans.page.users.userName,
      dataIndex: 'userName',
      key: 'userName',
    },
    {
      title: trans.page.users.fullName,
      dataIndex: 'fullName',
      key: 'fullName',
      ellipsis: true,
    },
    {
      title: trans.page.users.phoneNumber,
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
      ellipsis: true,
    },
    {
      title: trans.page.users.email,
      dataIndex: 'email',
      key: 'email',
      ellipsis: true,
    },
    {
      title: trans.page.users.organizationID,
      dataIndex: 'organizationDto',
      key: 'organizationDto',
      ellipsis: true,
      render: (value, record, index) => {
        return record.organizationDto.organization.id;
      },
    },
    {
      title: trans.page.users.roles,
      dataIndex: 'roles',
      key: 'roles',
      ellipsis: true,
      render: (value, record, index) => {
        return record.roles && record.roles.map(role => <Tag key={role.id}>{role.roleName}</Tag>);
      },
    },
    {
      title: trans.common.status,
      dataIndex: 'active',
      key: 'active',
      filters: [
        {
          text: 'Active',
          value: true,
        },
        {
          text: 'InActive',
          value: false,
        },
      ],
      render: (value, record) => (
        <Switch
          checked={value}
          onClick={() => {
            if (ability.can('create', PERMISSION_CODES.USER_MANAGE)) {
              modal.info({
                width: 600,
                footer: null,
                icon: null,
                wrapClassName: 'flex justify-center',
                content: (
                  <div className='text-center'>
                    <div className=',b-2 text-2xl font-bold'>{trans.common.changeStatus}</div>
                    <div>{trans.common.notify.warningChangeStatus(trans.page.users._)}</div>
                    <div className='mt-12 flex justify-center gap-4'>
                      <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                      <Button type='primary' onClick={() => useChangeStatus.mutate(record.id)}>
                        {trans.common.yes}
                      </Button>
                    </div>
                  </div>
                ),
              });
            }
          }}
        />
      ),
    },
    {
      title: trans.common.updateAt,
      dataIndex: 'updatedDate',
      key: 'updatedDate',
      render: (value, record, index) => {
        return dayjs(value).format(TIME_FORMAT);
      },
    },
    {
      title: trans.common.action,
      key: 'action',
      fixed: 'right',
      render(_value, record) {
        return (
          <div>
            <Dropdown
              menu={{
                items: ability.can('create', PERMISSION_CODES.USER_MANAGE)
                  ? [
                    {
                      key: 1,
                      label: (
                        <div
                          className='flex items-center gap-4'
                          onClick={() => {
                            router.push(URLS.SYSTEM_MANAGE.USER_MANAGE + '/update/' + record.id);
                          }}
                        >
                          {' '}
                          <Edit2Icon size={16} />
                          {trans.common.edit}
                        </div>
                      ),
                    },
                    {
                      key: 2,
                      label: (
                        <div
                          className='flex items-center gap-4'
                          onClick={() => {
                            router.push(URLS.SYSTEM_MANAGE.USER_MANAGE + '/detail/' + record.id);
                          }}
                        >
                          <ClipboardListIcon size={16} /> {trans.common.detail}
                        </div>
                      ),
                    },
                  ]
                  : [
                    {
                      key: 2,
                      label: (
                        <div
                          className='flex items-center gap-4'
                          onClick={() => {
                            router.push(URLS.SYSTEM_MANAGE.USER_MANAGE + '/detail/' + record.id);
                          }}
                        >
                          <ClipboardListIcon size={16} /> {trans.common.detail}
                        </div>
                      ),
                    },
                  ],
              }}
              placement='bottomLeft'
              arrow
            >
              <Button type='text'>
                <MoreVertical />
              </Button>
            </Dropdown>
          </div>
        );
      },
    },
  ];

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const { data: roles } = useGetBasicActiveListRole()
  const listRoles: { key: React.Key, value: string }[] = []
  roles?.data.forEach(role => {
    listRoles.push({ key: role.id, value: role.roleName })
  })
  const [inputs, setInputs] = useState<InputType[]>([
    {
      id: 1,
      field: 'userName',
      label: 'Tên người dùng',
      active: true,
      inputType: 'text',
      fieldType: 'STRING',
      op: 'LIKE',
      value: getFieldValueOnSearchParam('username')
    },

    {
      id: 2,
      field: 'email',
      label: 'Email',
      active: true,
      inputType: 'text',
      fieldType: 'STRING',
      op: 'LIKE',
      value: getFieldValueOnSearchParam('email')

    },
    {
      id: 3,
      field: 'status',
      label: 'Trang thai',
      active: false,
      inputType: 'select',
      options: [
        { key: 1, value: 'active' },
        { key: 2, value: 'inactive' },
      ],
      fieldType: 'INTEGER',
      op: 'EQUAL',
    },
    {
      id: 4,
      field: 'phoneNumber',
      label: 'Số điện thoại',
      active: false,
      inputType: 'text',
      fieldType: 'STRING',
      op: 'LIKE',
      value: getFieldValueOnSearchParam('phoneNumber')

    },
    {
      id: 5,
      field: 'roles',
      label: 'Vai trò',
      active: false,
      inputType: 'select',
      options: listRoles,
      fieldType: 'STRING',
      op: 'EQUAL',
      value: getFieldValueOnSearchParam('Vai trò')

    },
  ]);
  return (
    <>
      {contextHolder}

      <div className='mb-4 flex justify-between' id='#tbCate'>
        <Can I='delete' a={PERMISSION_CODES.USER_MANAGE} ability={ability}>
          <Button
            danger
            type='primary'
            disabled={selectedRowKeys.length === 0}
            onClick={() => {
              if (ability.can('delete', PERMISSION_CODES.USER_MANAGE)) {
                modal.error({
                  width: 600,
                  footer: null,
                  icon: null,
                  wrapClassName: 'flex justify-center',
                  content: (
                    <div className='text-center'>
                      <div className=',b-2 text-2xl font-bold'>{trans.common.deleteSpecific(trans.page.users._)}</div>
                      <div>{trans.common.notify.warningDelete(trans.page.users._)}</div>
                      <div className='mt-12 flex justify-center gap-4'>
                        <Button onClick={() => Modal.destroyAll()}>{trans.common.no}</Button>
                        <Button type='primary' danger onClick={() => useDelete.mutate({ listIds: selectedRowKeys })}>
                          {trans.common.yes}
                        </Button>
                      </div>
                    </div>
                  ),
                });
              }
            }}
          >
            {trans.common.delete}
          </Button>
        </Can>

        <div className='flex gap-4'>
          <FilterTable inputs={inputs} setInputs={setInputs} />
          <Can I='create' a={PERMISSION_CODES.USER_MANAGE} ability={ability}>
            <Button type='primary' onClick={() => router.push(URLS.SYSTEM_MANAGE.USER_MANAGE + '/create')}>
              {trans.common.create}
            </Button>
          </Can>
          <Dropdown.Button
            menu={{
              items: [
                {
                  key: 1,
                  label: (
                    <div className='flex items-center gap-4'>
                      {' '}
                      <Edit2Icon size={16} />
                      {trans.common.export.all}
                    </div>
                  ),
                  onClick: () => {
                    exportExcel({ data: data?.data.content!, fileName: 'todo' });
                  },
                },
                {
                  key: 2,
                  label: (
                    <div className='flex items-center gap-4'>
                      <DeleteIcon size={16} /> {trans.common.export.currentPage}
                    </div>
                  ),
                },
                {
                  key: 3,
                  label: (
                    <div className='flex items-center gap-4'>
                      <DeleteIcon size={16} /> {trans.common.export.currentSelect}
                    </div>
                  ),
                },
              ],
            }}
            placement='bottomLeft'
            arrow
          >
            {trans.common.export._}
          </Dropdown.Button>
        </div>
      </div>
      {inputs.filter(item => item.active).length > 0 && (
        <div className='my-4 border'>
          <FilterInputRender setInputs={setInputs} inputs={inputs} searchFunction={(value) => onChangeMultiSearchParams(value)} />
        </div>
      )}
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data?.data && data?.data.content?.map(item => ({ ...item, key: item.id }))}
        pagination={false}
        scroll={{ x: 'max-content' }}
      />

      <div className='mt-4 flex justify-between px-4'>
        <div>
          {`Showing
          ${tablePaginationConfig.total || 0 < pageSize ? tablePaginationConfig.total : pageSize}
          of ${tablePaginationConfig.total} elements`}
        </div>
        <Pagination {...tablePaginationConfig} />
      </div>
    </>
  );
};

const BreadcrumbCpn = () => {
  const { trans } = useTrans();
  return (
    <>
      <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.userManagement}</div>
      <div className='mx-6 mt-5 mobile:m-4'>
        <Breadcrumb
          items={[
            {
              title: 'Agent Portal',
            },
            {
              title: trans.menu.systemManagement,
            },
            {
              title: trans.menu.userManagement,
            },
          ]}
        />
      </div>
    </>
  );
};
export const getServerSideProps: GetServerSideProps = async ctx => {
  const shouldRedirect = await checkPermission(
    { permissionCode: PERMISSION_CODES.SUBAGENT_MANAGE, type: 'view' },
    ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY],
  );
  return shouldRedirect
    ? {
      props: {},
      redirect: shouldRedirect,
    }
    : { props: {} };
};

Accounts.getLayout = (children: React.ReactNode) => (
  <DashBoardLayout breadcrumb={<BreadcrumbCpn />}>{children}</DashBoardLayout>
);

export default Accounts;
