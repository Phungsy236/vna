import FormUser from '@/shared/components/business/user/FormUser'
import DashBoardLayout from '@/shared/components/layouts/dashboard/DashboardLayout'
import useTrans from '@/shared/hooks/useTrans'
import { useUpdateUser } from '@/shared/schema/models/IAppUser'
import { APP_SAVE_KEY, PERMISSION_CODES } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { checkPermission } from '@/shared/utils/functions/serverAuthen'
import { Affix, Breadcrumb, Button, Form } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import { GetServerSideProps } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'

const UserUpdate = ({ id }: { id: React.Key | undefined }) => {
    const [form] = useForm()
    const router = useRouter()
    const updateAccount = useUpdateUser(() => router.push(URLS.SYSTEM_MANAGE.USER_MANAGE));
    const { trans } = useTrans()
    function onFinish(value: any) {
        updateAccount.mutate({ user: value, id: id! })
    }



    return (
        <div className='w-full p-4'>
            <Form
                autoComplete='false'
                preserve
                form={form}
                onFinish={onFinish}
                layout='vertical'
                labelWrap
                size='large'
                colon={false}
                scrollToFirstError
                className='grid grid-cols-3 gap-x-20 mobile:grid-cols-1 ipad:grid-cols-2'
            >

                <Form.Item className='col-span-3  mobile:col-span-1 ipad:col-span-2 '>
                    <Affix offsetTop={10}>
                        <div className='flex justify-between my-2'>
                            <div className='text-2xl font-bold'>Update user</div>
                            <div className='space-x-2'>
                                <Button
                                    type='primary'
                                    htmlType='submit'
                                    className='mr-2 !rounded-none'
                                    loading={updateAccount.isLoading}
                                >
                                    {trans.common.save}
                                </Button>
                                <Button
                                    htmlType='reset'
                                    onClick={() => {
                                        form.resetFields();
                                        router.push(URLS.SYSTEM_MANAGE.USER_MANAGE);
                                    }}
                                    className='!rounded-none'
                                >
                                    {trans.common.close}
                                </Button>
                            </div>
                        </div>
                    </Affix>
                </Form.Item>


                <FormUser form={form} />
            </Form>
        </div>
    )
}

const BreadcrumbCpn = () => {
    const { trans } = useTrans()
    return (<>
        <div className='mx-6 mt-5 text-2xl font-bold mobile:mx-4'>{trans.menu.userUpdate}</div>
        <div className='mx-6 mt-5 mobile:m-4'>
            <Breadcrumb
                items={[
                    {
                        title: 'Agent Portal',
                    },
                    {
                        title: trans.menu.systemManagement
                    },
                    {
                        title: <Link href={URLS.SYSTEM_MANAGE.USER_MANAGE}>{trans.menu.userManagement}</Link>
                    },
                    {
                        title: trans.menu.userUpdate,
                    },
                ]}
            />
        </div>
    </>)
}

UserUpdate.getLayout = (children: React.ReactNode) => <DashBoardLayout
    breadcrumb={<BreadcrumbCpn />}
>{children}</DashBoardLayout>

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const shouldRedirect = await checkPermission({ permissionCode: PERMISSION_CODES.USER_MANAGE, type: 'create' }, ctx.req.cookies[APP_SAVE_KEY.TOKEN_KEY])
    return shouldRedirect
        ? {
            props: { id: ctx.query.id },
            redirect: shouldRedirect
        }
        : { props: { id: ctx.query.id } }

}
export default UserUpdate