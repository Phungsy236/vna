import withTheme from '../shared/components/layouts/withTheme';
import { extractStyle } from '@ant-design/static-style-extract';
import fs from 'fs';

const outputPath = './public/antd.min.css';

// if (fs.existsSync(outputPath)) {
//     console.log('css generated ')
// } else {
//     const css = extractStyle(withTheme);
//     fs.writeFileSync(outputPath, css);
// }

const css = extractStyle(withTheme);
fs.writeFileSync(outputPath, css);
