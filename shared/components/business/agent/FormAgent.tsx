import useLocationForm from '@/shared/hooks/useLocationForm';
import { useAppSelector } from '@/shared/hooks/useRedux';
import useTrans from '@/shared/hooks/useTrans';
import { useCreateAgent, useGetDetailAgent, useUpdateAgent } from '@/shared/schema/models/IAgent';
import { APP_REGEX } from '@/shared/utils/constants/appContants';
import { URLS } from '@/shared/utils/constants/appMenu';
import { Button, Form, FormInstance, Input, Switch } from 'antd';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import CountryInput from '../../common/CountryInput';
import CurrentcyInput from '../../common/CurrentcyInput';
import LanguageInput from '../../common/LanguageInput';
import PhoneCodeInput from '../../common/PhoneCodeInput';
import TimezoneInput from '../../common/TimezoneInput';
import BankInput from '../../common/BankInput';

type Props = {
  form: FormInstance;
  editId?: React.Key;
  viewOnly?: boolean;
  isSubAgent?: boolean;
};

export default function FormAgent({ form, editId, viewOnly, isSubAgent = false }: Props) {
  const { trans } = useTrans();
  const router = useRouter();
  const { data } = useGetDetailAgent({ id: editId!, options: { enabled: !!editId && router.isReady } });
  const BACKURL = isSubAgent ? URLS.SYSTEM_MANAGE.SUB_AGENT_MANAGE : URLS.SYSTEM_MANAGE.AGENT_MANAGE;
  const createAgent = useCreateAgent(() => router.push(BACKURL));
  const updateAgent = useUpdateAgent(() => router.push(BACKURL));
  const { FormLocation } = useLocationForm({ form });
  const user = useAppSelector(state => state.appSlice.user);
  function onFinish(value: any) {
    const formValue = isSubAgent
      ? {
          ...value,
          isSubAgent: isSubAgent,
          parentId: user?.agentDto.id,
          id: editId,
          level: 2,
        }
      : { ...value, id: editId, isSubAgent: false, level: 1 };
    if (editId) {
      updateAgent.mutate({ id: editId, Agent: formValue });
    } else {
      createAgent.mutate(formValue);
    }
  }
  useEffect(() => {
    if (editId) {
      const tmp = {
        ...data,
        province: data?.province?.provinceId,
        district: data?.district?.districtId,
        ward: data?.ward?.wardId,
      };
      form.setFieldsValue(tmp);
    }
    return () => form.resetFields();
  }, [data, form, editId]);

  return (
    <div className='w-full'>
      <Form
        preserve={false}
        form={form}
        onFinish={onFinish}
        layout='vertical'
        labelWrap
        size='large'
        colon={false}
        scrollToFirstError
        disabled={viewOnly}
      >
        <div className='col-span-3 mb-4 text-xl font-bold'>{trans.page.agent.form.agentInformation}</div>
        <div className='grid grid-cols-3 gap-x-20 rounded-lg border border-solid border-gray-300 p-4 mobile:grid-cols-1 ipad:grid-cols-2'>
          <Form.Item
            name='agentName'
            label={trans.page.agent.agentName}
            rules={[
              { required: false, message: trans.common.form.require },
              {
                pattern: APP_REGEX.VietnameseRegex,
                message: 'Nhập tên tiếng Việt',
              },
            ]}
          >
            <Input placeholder='agentName' />
          </Form.Item>
          <CountryInput key='country' inputName='country' />
          <LanguageInput />
          {FormLocation()}
          <Form.Item
            name='phoneNumber'
            label={trans.page.agent.phoneNumber}
            rules={[
              { required: false, message: trans.common.form.require },
              // {
              //   pattern: APP_REGEX.VNPhoneRegex,
              //   message: trans.common.form.invalidPhone,
              // },
            ]}
          >
            <Input addonBefore={<PhoneCodeInput />} placeholder='phoneNumber' />
          </Form.Item>
          <Form.Item
            name='email'
            label={trans.page.agent.email}
            rules={[
              { required: false, message: trans.common.form.require },
              { type: 'email', message: trans.common.form.invalidEmail },
            ]}
          >
            <Input placeholder='email' />
          </Form.Item>

          <Form.Item
            name='address'
            label={trans.page.agent.address}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <Input placeholder='address' />
          </Form.Item>

          <Form.Item
            name='businessCode'
            label={trans.page.agent.agentInformation.businessCode}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <Input placeholder='Business registration No' />
          </Form.Item>

          <Form.Item
            name='legalRepresentation'
            label={trans.page.agent.agentInformation.legalRepresentation}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <Input placeholder='Input legal representation' />
          </Form.Item>

          <Form.Item
            name='bankAccount'
            label={trans.page.agent.agentInformation.bankAccount}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <Input placeholder='Input Bank account' />
          </Form.Item>

          <Form.Item
            name='taxCode'
            label={trans.page.agent.agentInformation.taxCode}
            rules={[
              { required: false, message: trans.common.form.require },
              {
                pattern: APP_REGEX.TaxRegex,
                message: 'Nhập đúng định dạng Tax',
              },
            ]}
          >
            <Input placeholder='Input tax code' />
          </Form.Item>

          <Form.Item
            name='position'
            label={trans.page.agent.agentInformation.position}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <Input placeholder='Input position' />
          </Form.Item>

          <Form.Item
            name='accountHolder'
            label={trans.page.agent.agentInformation.accountHolder}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <Input placeholder='Account holder' />
          </Form.Item>
          <Form.Item name='iataCode' label={trans.page.agent.agentInformation.arcCode}>
            <Input placeholder='Input IATA/ARC Code' />
          </Form.Item>
          <Form.Item
            name='website'
            label='Website'
            rules={[
              { required: false, message: trans.common.form.require },
              {
                pattern: APP_REGEX.WebsiteRegex,
                message: 'Nhập đúng định dạng website',
              },
            ]}
          >
            <Input placeholder='Input website link' />
          </Form.Item>
          <BankInput />
        </div>
        <div className='col-span-3 my-4 text-xl font-bold'>{trans.page.agent.form.registrationArea}</div>
        <div className='grid grid-cols-3 gap-x-20 rounded-lg border border-solid border-gray-300 p-4 mobile:grid-cols-1 ipad:grid-cols-2'>
          <CountryInput inputName='registrationCountry' key='registrationCountry' label={trans.page.agent.country} />
          <Form.Item
            name='registrationArea'
            label={trans.page.agent.area}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <Input placeholder='area' />
          </Form.Item>
          <TimezoneInput />
          <Form.Item
            name='active'
            label={trans.common.status}
            className='col-span-3 mobile:col-span-1 ipad:col-span-2'
            valuePropName='checked'
            rules={[{ required: false, message: trans.common.form.require }]}
            initialValue={true}
          >
            <Switch checkedChildren={trans.common.active} unCheckedChildren={trans.common.deActive} />
          </Form.Item>
        </div>

        <div className='col-span-3 my-4 text-xl font-bold'>{trans.page.agent.form.committedRevenue}</div>
        <div className='grid grid-cols-2 gap-x-20 rounded-lg border border-solid border-gray-300  p-4 mobile:grid-cols-1'>
          <Form.Item
            name='revenueInternational'
            label={trans.page.agent.committedRevenue.interRevenue}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <CurrentcyInput placeholder='Input International revenue' className='w-full' prefix='USD' />
          </Form.Item>
          <Form.Item
            name='domestic'
            label={trans.page.agent.committedRevenue.domRevenue}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <CurrentcyInput placeholder='Input Domestic revenue' className='w-full' prefix='VND' />
          </Form.Item>
        </div>

        <div className='col-span-3 my-4 text-xl  font-bold'>{trans.page.agent.form.accountCreditLimit}</div>
        <div className='grid grid-cols-2 gap-x-20 rounded-lg border border-solid border-gray-300 p-4 mobile:grid-cols-1'>
          <Form.Item
            name='minimumLimit'
            label={trans.page.agent.minLimit}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <CurrentcyInput placeholder='Input Minimum Limit' className='w-full' prefix='VND' />
          </Form.Item>
          <Form.Item
            name='maximumLimit'
            label={trans.page.agent.maxLimit}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <CurrentcyInput placeholder='Input Maximum Limit' className='w-full' prefix='VND' />
          </Form.Item>
        </div>

        {!viewOnly && (
          <Form.Item className='col-span-3 flex justify-end'>
            <Button
              htmlType='reset'
              className='!rounded-none'
              onClick={() => {
                form.resetFields();
                router.push(BACKURL);
              }}
            >
              {trans.common.back}
            </Button>

            <Button
              type='primary'
              className='ml-4 mt-4 !rounded-none'
              htmlType='submit'
              loading={createAgent.isLoading || updateAgent.isLoading}
            >
              {trans.common.save}
            </Button>
          </Form.Item>
        )}
      </Form>
      {viewOnly && (
        <div className='mt-4 flex justify-end gap-4'>
          <Button
            onClick={() => {
              router.push(BACKURL);
            }}
          >
            {trans.common.close}
          </Button>
          <Button
            type='primary'
            onClick={() => {
              router.push(BACKURL + '/update/' + editId);
            }}
          >
            {trans.common.edit}
          </Button>
        </div>
      )}
    </div>
  );
}
