import React from 'react'
import Box from './Box'
import { Collapse } from 'antd'

type Props = {
    className?: string
}
const panelStyle = {
    marginBottom: 24,
    borderRadius: 8,
    border: '1px solid lightgray',
};
export default function BaggageRule({ className }: Props) {
    return (
        <Box className={className + ' p-4'}>
            <div className='mb-4 text-lg font-bold'>
                Free Baggage Allowance
                Description
            </div>
            <Collapse bordered={false} ghost items={[
                {
                    key: '1',
                    label: <b>General Fare Rule</b>,
                    children: <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel neque, temporibus unde hic dignissimos sint qui ex assumenda labore autem blanditiis reprehenderit voluptatibus deserunt laudantium. Magni accusamus numquam itaque quia?</p>,
                    style: panelStyle
                },
                {
                    key: '2',
                    style: panelStyle,

                    label: <b>Carry On Baggage</b>,
                    children: <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel neque, temporibus unde hic dignissimos sint qui ex assumenda labore autem blanditiis reprehenderit voluptatibus deserunt laudantium. Magni accusamus numquam itaque quia?</p>,
                },
            ]}>
            </Collapse>
        </Box >
    )
}