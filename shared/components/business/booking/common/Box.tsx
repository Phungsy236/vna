import React from 'react'
import { twMerge } from 'tailwind-merge'

type Props = {
    children: React.ReactNode
    className?: string
}

export default function Box({ children, className }: Props) {
    return (
        <div className={twMerge('shadow border border-solid border-slate-200', className)}>
            {children}
        </div>
    )
}