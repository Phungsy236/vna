import React from 'react'
import Box from './Box'

type Props = {}

export default function CACode({ }: Props) {
    return (
        <Box className='flex justify-between text-lg font-bold py-4 px-5'>
            <>
                <span>CA code</span>
                <span>14046018 JJZ-BB</span>
            </>
        </Box>
    )
}