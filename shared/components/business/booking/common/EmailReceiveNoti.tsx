import React from 'react'
import Box from './Box'
import { Form, Input } from 'antd'

type Props = {
    className?: string
}

export default function EmailReceiveNoti({ className }: Props) {
    return (
        <Box className={className + ' p-4'}>
            <div className='mb-4 text-lg font-bold'>
                Add email to receive notifications
            </div>
            <Form layout='vertical' size='large'>
                <Form.Item label="Email">
                    <Input placeholder='enter email to receive notify'></Input>
                </Form.Item>
            </Form>
        </Box>

    )
}