import NavArrowDownIcon from '@/shared/components/icons/NavArrowDownIcon'
import TakeOffIcon from '@/shared/components/icons/TakeOffIcon'
import ToLandIcon from '@/shared/components/icons/ToLandIcon'
import { Flight } from '@/shared/schema/models/IFlight'
import { Card, Col, Collapse, CollapseProps, Row, Timeline } from 'antd'
import React from 'react'
import Box from './Box'

type Props = {
    flight: Flight
}
export default function FlightCardCollapse() {
    const items: CollapseProps['items'] = [
        {
            key: '1',
            label: 'Flight details',
            children: (
                <Box className='w-full'>
                    <Timeline
                        items={[
                            {
                                children: (
                                    <>
                                        <strong>10/5/2023, 05:00 Hanoi</strong>
                                        <p>Hanoi (HAN) &rarr; Ho Chi Minh (SGN)</p>
                                        <Row>
                                            <Col span={2}>Image</Col>
                                            <Col span={2}>VN 205</Col>
                                            <Col span={8}>Operated by VietNam Airlines</Col>
                                        </Row>
                                        <p>Aircraft:<strong>Airbus A321</strong></p>
                                        <p>Travel time:<strong>2 tiếng 15 phút</strong></p>
                                        <p>Departure terminal:<strong>1</strong></p>
                                        <p>Arrival terminal<strong>1</strong></p>
                                    </>
                                ),

                                dot: <div className='rounded-full border-4 border-indigo-600'><TakeOffIcon /></div>,
                            },
                            {
                                children: (
                                    <>
                                        <strong>21/5/2023, 21:00 Hanoi</strong>
                                        <p>Hanoi (HAN) &rarr; Ho Chi Minh (SGN)</p>
                                    </>
                                ),
                                dot: <ToLandIcon />,
                            },
                        ]}
                    />
                </Box>
            ),
        },
    ];
    return (
        <Collapse
            expandIcon={isActive => <NavArrowDownIcon width={15} height={15} />}
            className='m-0 w-full'
            ghost
            items={items}
        />
    )
}