import React from 'react'
import { Flight } from '@/shared/schema/models/IFlight'
import { Row, Col } from 'antd'

type Props = {
    flight: Flight
}

export default function FlightCardContent() {
    return (
        <div className='flex gap-4'>
            <div className='w-[120px] h-[120px] bg-slate-200'>Image</div>
            <div className='flex gap-6'>
                <div className='flex flex-col items-center justify-center' >
                    <h3 className='m-0'>8h30</h3>
                    <p className='m-0'>HAN</p>
                </div>
                <div className='flex flex-col items-center justify-center' >
                    <p style={{ borderBottom: '1px solid #555' }} className='m-0'>
                        Bay thang
                    </p>
                    <p className='m-0'>01h:45m</p>
                </div>
                <div className='flex flex-col items-center justify-center'>
                    <h3 className='m-0'>9h45</h3>
                    <p className='m-0'>HUI</p>
                </div>
            </div>

        </div>
    )
}