import { useAppSelector } from '@/shared/hooks/useRedux';
import { Card, Col, Row } from 'antd';
import { ArrowDown } from 'lucide-react';
import FlightCardCollapse from './FlightCardCollapse';
import FlightCardContent from './FlightCardContent';
import FightingIcon from '@/shared/components/icons/FightingIcon';
import EditIcon from '@/shared/components/icons/EditIcon';
import Box from './Box';


export function ItemCardFlightPicked() {
  return (
    <Box className='p-4'>
      <div className='flex justify-between'>
        <div className='flex gap-4 items-center'>
          <FightingIcon />
          <b>Chuyen di Ha Noi &rarr; Hue - Thu 2, ngay 1/7/2023 </b>
        </div>
        <div className='flex gap-4 items-center'>
          <EditIcon />
          <p>Thay doi</p>
        </div>
      </div>
      <div className='flex mobile:flex-col mobile:gap-4 flex-row justify-between items-end'>
        <div className='flex-grow'>
          <FlightCardContent />
        </div>
        <span>
          Economy class
        </span>
      </div>
      <div className='flex w-full items-center justify-start' >
        <FlightCardCollapse />
      </div>
    </Box>
  );
}
