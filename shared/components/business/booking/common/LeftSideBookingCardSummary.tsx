import React from 'react';
import type { CollapseProps } from 'antd';
import { Collapse } from 'antd';
import dayjs from 'dayjs'
import { RepeatIcon } from 'lucide-react';
import { PlaneIcon } from 'lucide-react';
import Box from './Box';
import { twMerge } from 'tailwind-merge'

type titleProps = {
  title: string,
  price: string,
  className?: string,
  labelClassName?: string,
  contentClassName?: string
}

type flightProps = {
  title: string,
  date: React.ReactNode,
  depart: string,
  arrival: string,
  airline: string,
  type: string,
  time: string
}

const Title = (props: titleProps) => {
  return (
    <div className={twMerge('flex justify-between', props.className)}>
      <div className={props.labelClassName}>{props.title}</div>
      <div className={props.contentClassName}>{props.price}</div>
    </div>)
}

const TaxFee = (props: titleProps) => {
  return (
    <div style={{ display: 'grid', gridTemplateColumns: '1fr auto' }}>
      <span> {props.title}</span>
      <span> {props.price}</span>
    </div>)
}

const Flight = (props: flightProps) => {
  return (
    <div>
      <b>{props.title}</b>
      <p>{props.date}</p>
      <p>{props.depart + " -> "}{props.arrival}</p>
      <p><PlaneIcon size={20} /> {props.airline} {props.type}</p>
      <p>{props.time}</p>
    </div>)
}

const Date = (date: string, time: string) => {
  const day = dayjs(date).format('dddd, DD/MM/YYYY')
  const hour = dayjs(time, 'HH:mm').format('HH:mm')
  return (
    <div>
      <p>{hour} - {day}</p>
    </div>
  )
}

const items: CollapseProps['items'] = [
  {
    key: '1',
    label: <Title title={'Flight information'} price='2.000.000 VND' />,
    children: (<>
      <Flight title={'Departing'} date={Date('2023-05-17', '08:30')} depart={'Hanoi (HAN)'} arrival={'Ho Chi Minh (SGN)'} airline={'VN 205'} type={'Ecomomy Classic'} time={'01h15m'} /> <hr />
      <Flight title={'Returning'} date={Date('2023-05-20', '08:30')} depart={'Ho Chi Minh (SGN)'} arrival={'Hanoi (HAN)'} airline={'VN 205'} type={'Ecomomy Classic'} time={'01h15m'} />
    </>),
  },
  {
    key: '2',
    label: <Title title={'Tax, Fees and Carrier Charges'} price='3.800.000 VND' />,
    children: (
      <>
        <TaxFee title={'Value Add Tax, Vietnam'} price={'800.000 VND'} /><hr style={{ margin: '0px' }} />
        <TaxFee title={'Passenger and Baggage Seculity Screening Service Charge, Vietnam'} price={'100.000 VND'} /><hr style={{ margin: '0px' }} />
        <TaxFee title={'Passenger Service Charge, Vietnam'} price={'400.000 VND'} /><hr style={{ margin: '0px' }} />
        <TaxFee title={'System and Admin Surcharge'} price={'2.500.000 VND'} />
      </>)

  },
  {
    key: '3',
    label: <Title title={'Pre-paid Baggage'} price='700.000 VND' />,
    children: '',
  },
];

export default function LeftSideBookingCardSummary() {
  return (
    <Box className='px-5 py-4'>
      <div>
        <div className='truncate text-lg font-bold'>Ha Noi (HAN) <RepeatIcon size={16} /> Ho Chi Minh (SGN)</div>
        <Collapse items={items} bordered={false} ghost defaultActiveKey={['1']} />
        <Title labelClassName='font-semibold text-base' contentClassName='font-bold text-xl' title={'Trip Total'} price='6.500.000 VND' />
      </div>
    </Box>

  )
}