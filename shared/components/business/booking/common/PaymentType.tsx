import React, { PropsWithChildren } from 'react';
import { CreditCard } from 'lucide-react';
import { Ticket } from 'lucide-react';

interface IconTypeProps {
  width: number,
  height: number
}

type IconProps = (props: IconTypeProps) => JSX.Element

type PaymentProps = {
  payment: string,
  icon: IconProps
}

function Payment({ payment, icon }: PropsWithChildren<PaymentProps>) {
  return (
    <div style={{ border: ".1px solid lightgray", display: 'inline-block', margin: '10px' }}>
      {React.createElement(icon, { width: 60, height: 60 })}
      <h4>{payment}</h4>
    </div>
  )
}

export default function PaymentType() {
  return (
    <div style={{ display: 'grid', gridTemplateColumns: 'repeat(3, minmax(0, 1fr))', textAlign: 'center' }}>
      <Payment payment="Agency Credit Limit" icon={CreditCard} />
      <Payment payment="eVoucher" icon={Ticket} />
      <Payment payment="Via Amadeus" icon={Ticket} />
    </div>
  );
}
