import React from 'react'
import Box from './Box'
import { Button } from 'antd'

type Props = {}

export default function ReservationItem({ }: Props) {
    return (
        <Box className='p-5'>
            <div className='flex flex-col gap-2'>
                <div className='flex justify-between font-semibold text-base'>
                    <span>ACPTJN</span>
                    <span>NGUYEN VINH</span>
                </div>
                <div className='font-semibold'>HAN-SGN</div>
                <div className='flex justify-between'>
                    <div>22/05/2021</div>
                    <div className='font-semibold'>TL: Còn 3d17h25m</div>
                </div>
            </div>
            <div className='flex gap-2 justify-end mt-3'>
                <Button>Hủy </Button>
                <Button type='primary'>Thanh toán</Button>
            </div>
        </Box>
    )
}