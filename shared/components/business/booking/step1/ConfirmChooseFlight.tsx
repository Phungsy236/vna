import { useAppDispatch } from "@/shared/hooks/useRedux";
import { nextStep, removeFare } from "@/shared/stores/bookingSlice";
import { Button, Modal } from "antd";
import { PickedFlight } from "./PickedFlight";

type Props = {
    open: boolean,
    setOpen: (open: boolean) => void
}

export function ConfirmChoosenFlight({ open, setOpen }: Props) {
    // const { chooseFlights } = useAppSelector(state => state.bookingSlice);
    // const { currentStep } = useAppSelector(state => state.bookingSlice);
    const dispatch = useAppDispatch();
    function handleNext() {
        dispatch(nextStep());
    }
    function handleClose() {
        dispatch(removeFare("qweqwe"));
    }
    return (
        <Modal open={open} onCancel={() => setOpen(false)} footer={<></>} width={'100vw'}>
            <div className='grid grid-cols-5'>
                <div className='col-span-3'>
                    <PickedFlight />
                </div>
                <div className='col-span-2'>
                    {/* <DetailPickedFlight /> */}
                </div>
            </div>

            <div className='mt-4 flex justify-center'>
                <Button onClick={handleNext} type='primary'>
                    Tiep tuc
                </Button>
                <Button onClick={handleClose}>Quay lai</Button>
            </div>
        </Modal>
    );
}