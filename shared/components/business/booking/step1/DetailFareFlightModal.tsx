import { FeeIcon } from "@/shared/components/icons/FeeIcon";
import FightingIcon from "@/shared/components/icons/FightingIcon";
import { FreeOfChange } from "@/shared/components/icons/FreeOfChangeIcon";
import { NotAllowIcon } from "@/shared/components/icons/NotAllowIcon";
import { useAppDispatch, useAppSelector } from "@/shared/hooks/useRedux";
import { Flight } from "@/shared/schema/models/IFlight";
import { addFare } from "@/shared/stores/bookingSlice";
import { TIME_FORMAT } from "@/shared/utils/constants/appContants";
import { formatNumber } from "@/shared/utils/functions/formatCurrentcy";
import { Button, Modal } from "antd";
import dayjs from "dayjs";

const fakeItem = [{
    id: 1,
    classFlight: 'Econonmy SuperLite',
    price: 1000_000,
    seatAvai: 6,
    ticketChange: 200_000,
    priorityCheckIn: true,
    businesslounge: false
},
{
    id: 2,
    classFlight: 'Econonmy Lite',
    price: 2000_000,
    seatAvai: 6,
    ticketChange: 200_000,
    priorityCheckIn: false,
    businesslounge: true
},
{
    id: 3,
    classFlight: 'Econonmy Classic',
    price: 3000_000,
    seatAvai: 6,
    ticketChange: 200_000,
    priorityCheckIn: false,
    businesslounge: true
},
{
    id: 4,
    classFlight: 'Econonmy Flex',
    price: 4000_000,
    seatAvai: 6,
    ticketChange: 200_000,
    priorityCheckIn: true,
    businesslounge: false
}

]

const Row = ({ children }: { children: React.ReactNode }) => <div className="flex border-collapse w-full">{children}</div>
const Box = ({ children, className }: { children: React.ReactNode, className?: string }) => <div
    className={"flex justify-center items-center border border-solid border-slate-100 border-collapse w-1/5 py-3 flex-wrap " + className} >{children}</div>

export function DetailFareFlightModal({
    open,
    setOpen,
    flight,
}: {
    open: boolean;
    setOpen: (x: boolean) => void;
    flight: Flight;
}) {
    const { startLocation, endLocation, startTime } = useAppSelector(state => state.bookingSlice.valueSearch)
    const dispatch = useAppDispatch();
    function handleChooseFare() {
        dispatch(addFare(flight));
        setOpen(false)
    }

    return (
        <Modal open={open} onCancel={() => setOpen(false)} footer={<></>} width={'100vw'}>
            <div className='flex gap-4 items-center font-bold text-lg'>
                <FightingIcon />
                <p>Chuyến đi {startLocation} &rarr; {endLocation} - {dayjs(startTime).format(TIME_FORMAT)} </p>
            </div>
            <div className="">
                <Row>
                    <Box>
                        <div className="py-2">
                            <div className="flex items-center gap-2 ">
                                <FreeOfChange /> Free of change
                            </div>
                            <div className="flex items-center gap-2">
                                <NotAllowIcon /> Not Allow
                            </div>
                            <div className="flex items-center gap-2">
                                <FeeIcon /> Fees
                            </div>
                        </div>
                    </Box>
                    {fakeItem.map((item, index) => <Box className={`bg-slate-${(index + 1) * 100} desktop:text-xl font-semibold`} key={item.id}>{item.classFlight}</Box>)}
                </Row>
                <Row>
                    <Box>
                    </Box>
                    {fakeItem.map((item) => <Box key={item.id} >
                        <div className="text-center py-2">
                            <div className="font-bold desktop:text-xl py-4 px-8 bg-slate-100">{formatNumber(item.price, 'vi')} VND</div>
                            <div>{item.seatAvai} seat left</div>
                            <Button type="primary" onClick={() => handleChooseFare()}>Select </Button>
                        </div>
                    </Box>)}
                </Row>
                <Row>
                    <Box>
                        Ticket Change
                    </Box>
                    {fakeItem.map((item) => <Box key={item.id} >
                        {item.ticketChange}
                    </Box>)}
                </Row>
                <Row>
                    <Box>
                        Priority checkin
                    </Box>
                    {fakeItem.map((item) => <Box key={item.id} >
                        {item.priorityCheckIn ? <NotAllowIcon /> : <FreeOfChange />}
                    </Box>)}
                </Row>
            </div>
            <ul className="flex gap-8 mt-6">
                <b>Note</b>
                <li>Fares include taxes</li>
                <li>Special service charges </li>
                <li>Taxes, Fee, Charges & Surcharges</li>
            </ul>

        </Modal>
    );
}