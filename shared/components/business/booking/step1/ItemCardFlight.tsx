import { ChevronDown } from 'lucide-react';
import FlightCardCollapse from '../common/FlightCardCollapse';
import FlightCardContent from '../common/FlightCardContent';


export function ItemCardFlight({ setOpenDetail }: { setOpenDetail: (open: boolean) => void; step: string }) {
  return (
    <>
      <div className='flex mobile:flex-col mobile:gap-4 flex-row justify-between '>
        <div className='flex-grow'>
          <FlightCardContent />
        </div>
        <div className='flex justify-center'>
          <div onClick={() => setOpenDetail(true)} className='bg-slate-50 py-4 desktop:px-10 px-4'>
            <div className='flex items-center gap-2'>from <span className=' desktop:text-2xl font-bold'>1.509.000 </span>VND (L)</div>
            <div className='flex justify-center'>
              <span className='flex justify-center p-2 bg-white rounded-full w-max '><ChevronDown /> </span>
            </div>
          </div>
          <div onClick={() => setOpenDetail(true)} className='bg-slate-200 py-4 desktop:px-10 px-4 text-center'>
            <div className='flex items-center gap-2'>from <span className='desktop:text-2xl font-bold'>1.509.000 </span>VND (I)</div>
            <div className='flex justify-center'>
              <span className='flex justify-center p-2 bg-white rounded-full w-max '><ChevronDown /> </span>
            </div>
          </div>
        </div>
      </div>
      <div className='flex w-full items-center justify-start' >
        <FlightCardCollapse />
      </div>
    </>
  );
}
