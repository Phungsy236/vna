import EditIcon from '@/shared/components/icons/EditIcon';
import FightingIcon from '@/shared/components/icons/FightingIcon';
import { Flight } from '@/shared/schema/models/IFlight';
import { Card, Row, Col } from 'antd';
import { useState } from 'react';
import { ItemCardFlight } from './ItemCardFlight';
import { DetailFareFlightModal } from './DetailFareFlightModal';
import { ConfirmChoosenFlight } from './ConfirmChooseFlight';
import Box from '../common/Box';

export function ItemFlight({ step, item }: { step: string; item: Flight }) {
  const [isOpen, setOpen] = useState(false);
  return (
    <>
      <Box>
        <ItemCardFlight setOpenDetail={setOpen} step={step} />
      </Box>
      <DetailFareFlightModal open={isOpen} setOpen={setOpen} flight={item} />
      {/* <ConfirmChoosenFlight /> */}
    </>
  );
}
