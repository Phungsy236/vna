import { useAppSelector } from '@/shared/hooks/useRedux'
import useTrans from '@/shared/hooks/useTrans'
import { FLIGHTTYPE } from '@/shared/schema/typedef/IFlightType'
import { setTypeFlight } from '@/shared/stores/bookingSlice'
import { Button, Card, Segmented } from 'antd'
import { useDispatch } from 'react-redux'
import { ReservationNotPaid } from './ReservationNotPaid'
import SearchForm from './SearchForm'
import ResultSearch from './ResultSearch'
import ArrowRightIcon from '@/shared/components/icons/ArrowRightIcon'
import Box from '../common/Box'

type Props = {}

export default function LayoutStep1({ }: Props) {
    const { trans } = useTrans()
    const { type, resultSearch, isSearched, valueSearch, chooseFlights } = useAppSelector(state => state.bookingSlice)
    const dispath = useDispatch()
    const canNextStep = () => {
        if (type === 'oneWay' && chooseFlights?.length === 1) return true
        else if (type === 'roundTrip' && chooseFlights?.length === 2) return true
        if (type === 'multiStop' && valueSearch.flights?.length === chooseFlights?.length) return true
    }
    return (
        <section className='mt-4'>
            <Segmented size='large' onChange={(value) => dispath(setTypeFlight(value as FLIGHTTYPE))} options={[
                { label: trans.page.booking.step1.roundTrip, value: 'roundTrip' },
                { label: trans.page.booking.step1.oneWay, value: 'oneWay' },
                { label: trans.page.booking.step1.multiStop, value: 'multiStop' }]} value={type} />
            <div className='mt-5'></div>
            <Box className='shadow p-5'  >
                <SearchForm />
            </Box>
            <div className='mt-6'></div>
            {
                isSearched && resultSearch ? <ResultSearch /> : <ReservationNotPaid />
            }
            {
                isSearched &&

                <div className='mt-4 flex justify-center'>
                    <Button type='primary' className='flex items-center' disabled={!canNextStep()}>Next Step <ArrowRightIcon /></Button>
                </div>
            }
        </section>
    )
}