import { Button, Card } from 'antd'
import React from 'react'
import Box from '../common/Box'
import ReservationItem from '../common/ReservationItem'




export function ReservationNotPaid() {
    return (
        <Box className='mt-6 shadow p-4'>
            <>
                <div className='font-bold text-lg pb-4'>Đặt chỗ chưa thanh toán</div>
                <div className='grid gap-4 grid-cols-3'>
                    <ReservationItem />
                    <ReservationItem />
                    <ReservationItem />
                    <ReservationItem />
                </div>
                <div className='flex justify-center mt-4'>
                    <Button type='primary'>Xem tất cả</Button>
                </div>
            </>

        </Box>
    )
}