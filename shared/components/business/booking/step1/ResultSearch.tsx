import FightingIcon from '@/shared/components/icons/FightingIcon';
import { useAppSelector } from '@/shared/hooks/useRedux';
import data from '@/shared/mocks/db.json';
import { TIME_FORMAT } from '@/shared/utils/constants/appContants';
import { Button, Popover } from 'antd';
import classNames from 'classnames';
import dayjs from 'dayjs';
import { FilterIcon } from 'lucide-react';
import { useState } from 'react';
import { ItemFlight } from './ItemFlight';

export default function ResultSearch() {
  const { startLocation, endLocation, startTime } = useAppSelector(state => state.bookingSlice.valueSearch)
  const [fakeOptions, setFakeOption] = useState([{
    id: 1,
    date: "16/05/2023",
    price: 1_500_000,
    active: true,
  },
  {
    id: 2,
    date: "17/05/2023",
    price: 2_500_000,
    active: false,
  },
  {
    id: 3,
    date: "18/05/2023",
    price: 3_500_000,
    active: false,
  },
  {
    id: 4,
    date: "19/05/2023",
    price: 2_500_000,
    active: false
  },


  ])
  return (
    <div className='my-5'>
      {/* Date option */}
      <div className='flex gap-4 items-center font-bold text-lg'>
        <FightingIcon />
        <p>Chuyến đi {startLocation} &rarr; {endLocation} - {dayjs(startTime).format(TIME_FORMAT)} </p>
      </div>
      {/* Date option */}
      <div className='flex overflow-x-auto '>
        {fakeOptions.map(item =>
          <div key={item.id}
            onClick={() => setFakeOption(fakeOptions.map(op => {
              if (op.id === item.id) op.active = true
              else op.active = false
              return op
            }))}
            className={classNames('text-center px-8 py-2',
              { 'bg-slate-200  border-bottom border-b-4 border-black': item.active })}>

            {item.date}
            <div className='font-bold mt-2'>{item.price} VND</div>

          </div>
        )}
      </div>
      <div className='py-4 flex justify-between'>
        <div className='flex gap-2'>
          <Popover><Button className='!flex !items-center !px-6'><FilterIcon /> Sort</Button></Popover>
          <Button type='primary'>Load more</Button>
        </div>
        <div className='desktop:flex desktop:block hidden'>
          <div className='px-10 bg-slate-100  flex items-center'>Economy</div>
          <div className='px-10 bg-slate-400  flex items-center'>Business</div>
        </div>
      </div>
      <div className='flex flex-col gap-4'>
        {
          data.flights.map(flight =>
            //@ts-ignore
            <ItemFlight key={flight.id} item={flight} />
          )
        }
      </div>


    </div >
  );
}
