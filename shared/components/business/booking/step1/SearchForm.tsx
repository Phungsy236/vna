import AmountInput from '@/shared/components/common/AmountInput';
import ArrowDownIcon from '@/shared/components/icons/ArrowDownIcon';
import { CACodeIcon } from '@/shared/components/icons/CACodeIcon';
import FlightClassIcon from '@/shared/components/icons/FlightClassIcon';
import PassengerIcon from '@/shared/components/icons/PassengerIcon';
import { useAppSelector } from '@/shared/hooks/useRedux';
import { IFlightSearch } from '@/shared/schema/typedef/IFlightSearch';
import { setSearchValue } from '@/shared/stores/bookingSlice';
import { APP_BOOKING } from '@/shared/utils/constants/appBooking';
import { Button, DatePicker, Form, MenuProps, Radio, Select, Space } from 'antd';
import { useForm, useWatch } from 'antd/lib/form/Form';
import dayjs from 'dayjs';
import { MinusCircle, PlusIcon, SearchIcon } from 'lucide-react';
import dynamic from 'next/dynamic';
import { useState } from 'react';
import { useDispatch } from 'react-redux';

const DropDownNoSSR = dynamic(() => import('antd/lib/dropdown/dropdown'), { ssr: false })

const fakeLocation = [
    { value: 'Mỹ', lable: "Mỹ" },
    { value: 'Nhật', lable: "Nhật" },
    { value: 'Hàn', lable: "Hàn" },
    { value: 'Việt', lable: "Việt" },
]
const TIME_FORMAT = 'DD/MM/YYYY'


export default function SearchForm() {
    const [formSearch] = useForm<IFlightSearch>();
    const { type, valueSearch } = useAppSelector(state => state.bookingSlice)
    const dispath = useDispatch()
    function handleOnSearch(value: any) {
        console.log(value)
        dispath(setSearchValue(value))
    }
    const numberAdult = useWatch(APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_ADULT, formSearch) || 0
    const numberChildren = useWatch(APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_CHILDREN, formSearch) || 0
    const numberInfant = useWatch(APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_INFANT, formSearch) || 0
    const flightClass = useWatch(APP_BOOKING.FORM_SEARCH_SHAPE.FLIGHT_CLASS, formSearch)
    const departDate = useWatch(APP_BOOKING.FORM_SEARCH_SHAPE.START_DATE, formSearch)
    const [openSelectPassenger, setOpenPassenger] = useState(false)
    const [openSelectClassFlight, setOpenClassFlight] = useState(false)
    const [openSelectCACode, setOpenSelectCAcode] = useState(false)


    const PassengersItems: MenuProps['items'] = [
        {
            key: '1',
            label: <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_ADULT} labelCol={{ span: 6 }} labelAlign='left' colon={false}
                label={<div className='flex flex-col'>
                    <span className='font-bold'>Người lớn</span>
                    <span className='text-xs'>Từ 12 tuổi</span>
                </div>} >
                <AmountInput form={formSearch} inputName={APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_ADULT}
                    initAmount={valueSearch.numberAdult || 0}
                    minAmount={0}
                    maxAmount={APP_BOOKING.MAX_PASSENGER - numberChildren - numberInfant} />
            </Form.Item>
        },
        {
            key: 2,
            label: <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_CHILDREN} labelCol={{ span: 6 }} labelAlign='left' colon={false}
                label={<div className='flex flex-col'>
                    <span className='font-bold'>Trẻ em</span>
                    <span className='text-xs'>2-12 tuổi</span>
                </div>} >
                <AmountInput form={formSearch} inputName={APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_CHILDREN}
                    initAmount={valueSearch.numberChildren || 0}
                    minAmount={0}
                    maxAmount={APP_BOOKING.MAX_PASSENGER - numberAdult - numberInfant} />
            </Form.Item>
        },
        {
            key: 3,
            label: <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_INFANT} labelAlign='left' labelCol={{ span: 6 }} colon={false}
                label={<div className='flex flex-col'>
                    <span className='font-bold'>Trẻ nhỏ</span>
                    <span className='text-xs'>Dưới 2 tuổi</span>
                </div>} >
                <AmountInput form={formSearch}
                    inputName={APP_BOOKING.FORM_SEARCH_SHAPE.NUMBER_INFANT}
                    initAmount={valueSearch.numberInfant || 0}
                    minAmount={0}
                    maxAmount={APP_BOOKING.MAX_PASSENGER - numberChildren - numberAdult} />
            </Form.Item>
        }

    ]
    const ClassFlightItems: MenuProps['items'] = [
        {
            key: 1,
            label: <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.FLIGHT_CLASS} noStyle >
                <Radio.Group>
                    <Space direction='vertical'>
                        {APP_BOOKING.FLIGHT_CLASS.map((flightClass) =>
                            <Radio value={flightClass.value} key={flightClass.value}>{flightClass.label}</Radio>
                        )}
                    </Space>
                </Radio.Group>
            </Form.Item>

        }]

    return (
        <Form form={formSearch} layout='vertical' name='searchForm' className='grid gap-4 grid-cols-4' onFinish={handleOnSearch} initialValues={valueSearch}>
            {type === 'multiStop' ? <>
                <Form.List name="flights" >
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(({ key, name, ...restField }) => (
                                <div key={key} className='col-span-4 gap-4 flex items-center'>
                                    <Form.Item {...restField} name={[name, APP_BOOKING.FORM_SEARCH_SHAPE.START_LOCATION]} className='flex-grow' label='Điểm đi'>
                                        <Select placeholder='Điểm đi' className='w-full' size='large' options={fakeLocation} />
                                    </Form.Item>
                                    <Form.Item {...restField} name={[name, APP_BOOKING.FORM_SEARCH_SHAPE.END_LOCATION]} className='flex-grow' label='Điểm đến'>
                                        <Select placeholder='Điểm đến' className='w-full' size='large' options={fakeLocation} />
                                    </Form.Item>
                                    <Form.Item  {...restField} name={[name, APP_BOOKING.FORM_SEARCH_SHAPE.START_DATE]} className='flex-grow' label='Ngày đi' initialValue={dayjs()} >
                                        <DatePicker className='w-full' size='large' format={TIME_FORMAT} />
                                    </Form.Item>

                                    <Button onClick={() => remove(name)} type='text' className=' w-max'><MinusCircle /></Button>
                                </div>
                            ))}
                            <Form.Item className='justify-self-end col-span-4'>
                                <Button type="dashed" onClick={() => add()} block icon={<PlusIcon />} className='flex items-center justify-center' >
                                    Thêm chặng
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
            </> :
                <>
                    <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.START_LOCATION} className='col-span-1' label='Điểm đi'>
                        <Select placeholder='Điểm đi' className='w-full' size='large' showSearch options={fakeLocation} />
                    </Form.Item>
                    <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.END_LOCATION} className='col-span-1' label='Điểm đến'>
                        <Select placeholder='Điểm đến' className='w-full' size='large' options={fakeLocation} />
                    </Form.Item>
                    <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.START_DATE} className='col-span-1' label='Ngày đi' initialValue={dayjs()} >
                        <DatePicker className='w-full' size='large' format={TIME_FORMAT} />
                    </Form.Item>
                    <Form.Item name={APP_BOOKING.FORM_SEARCH_SHAPE.END_DATE} className='col-span-1' label='Ngày đến'>
                        <DatePicker placeholder='' className='w-full' size='large' format={TIME_FORMAT} disabledDate={(date) => date < dayjs(departDate).add(1, 'day')} />
                    </Form.Item>

                    <DropDownNoSSR menu={{ items: PassengersItems }} forceRender open={openSelectPassenger} onOpenChange={() => setOpenPassenger(!openSelectPassenger)} trigger={['click']} >
                        <div className='flex items-center gap-3 border border-solid rounded-full border-slate-400 p-4'>
                            <PassengerIcon /> {(numberAdult + numberChildren + numberInfant) || 0} passenger <ArrowDownIcon />
                        </div>
                    </DropDownNoSSR>

                    <DropDownNoSSR menu={{ items: ClassFlightItems }} forceRender open={openSelectClassFlight}
                        onOpenChange={() => setOpenClassFlight(!openSelectClassFlight)}
                        trigger={['click']} >
                        <div className='flex items-center gap-3 border border-solid rounded-full border-slate-400 p-4'>
                            <FlightClassIcon />{APP_BOOKING.FLIGHT_CLASS[flightClass || 0]?.label}<ArrowDownIcon />
                        </div>
                    </DropDownNoSSR>

                    <DropDownNoSSR menu={{ items: [] }} forceRender open={openSelectCACode}
                        onOpenChange={() => setOpenSelectCAcode(!openSelectCACode)}
                        trigger={['click']} >
                        <div className='flex items-center gap-3 border border-solid rounded-full border-slate-400 p-4'>
                            <CACodeIcon /><ArrowDownIcon />
                        </div>
                    </DropDownNoSSR>


                </>
            }
            <Form.Item className='col-span-4 justify-self-end'>
                <Button type='primary' htmlType='submit' className='!flex !items-center rounded-none  font-medium'>
                    <SearchIcon /> <span className='ml-4'>Tìm kiếm</span>
                </Button>
            </Form.Item>
        </Form>
    )
}