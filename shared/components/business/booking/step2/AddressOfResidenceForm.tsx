import { Form, Input, Select, Space } from 'antd';
export const AddressOfResidenceForm = ({ name, ...restField }: any) => {
  return (
    <>
      <h1>Dia chi noi cu tru</h1>
      <Form.Item {...restField} name={[name, 'stressAddress']} label='Dia chi cu tru'>
          <Input placeholder='Dia chi cu tru' />
        </Form.Item>
        <Form.Item {...restField} name={[name, 'danh xung']} label='Danh xung'>
          <Select placeholder='Danh xung'>
            <Select.Option value='Ong'>Ong</Select.Option>
            <Select.Option value='Ba'>Ba</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item {...restField} name={[name, 'city_region']} label='Thanh pho/Vung'>
          <Input placeholder='Ho' />
        </Form.Item>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} label='Ngay'>
          <Input placeholder='Ngay' />
        </Form.Item>
        <Form.Item {...restField} label='Thang'>
          <Input placeholder='Thang' />
        </Form.Item>
        <Form.Item {...restField} label='Nam'>
          <Input placeholder='Nam' />
        </Form.Item>
      </Space>
    </>
  );
};