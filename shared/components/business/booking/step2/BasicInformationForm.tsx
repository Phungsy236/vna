import { Form, Input, Select, Space } from 'antd';
export const BasicInformationForm = ({ typePersion, index, name, ...restField }: any) => {
  return (
    <>
      <h1>Thong tin co ban</h1>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} name={[name, `name_${typePersion}_${index}`]} label='Danh xung'>
          <Select placeholder='Danh xung'>
            <Select.Option value='Ong'>Ong</Select.Option>
            <Select.Option value='Ba'>Ba</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item {...restField} name={[name, `mid_name_${typePersion}_${index}`]} label='Ten dem va ten'>
          <Input placeholder='Ten dem va ten' />
        </Form.Item>
        <Form.Item {...restField} name={[name, `surName_${typePersion}_${index}`]} label='Ho'>
          <Input placeholder='Ho' />
        </Form.Item>
      </Space>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} name={[name, `date_${typePersion}_${index}`]} label='Ngay'>
          <Input placeholder='Ngay' />
        </Form.Item>
        <Form.Item {...restField} name={[name, `month_${typePersion}_${index}`]} label='Thang'>
          <Input placeholder='Thang' />
        </Form.Item>
        <Form.Item {...restField} name={[name, `year_${typePersion}_${index}`]} label='Nam'>
          <Input placeholder='Nam' />
        </Form.Item>
      </Space>
      {index === 0 && typePersion === "adults" ? (
        <>
          <h1>Thong tin lien he</h1>
          <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
            <Form.Item
              {...restField}
              label='So dien thoai'
              name={[name, `phoneNumber_${typePersion}_${index}`]}
              rules={[{ required: true, message: 'Missing So dien thoai' }]}
            >
              <Input placeholder='So dien thoai' />
            </Form.Item>
            <Form.Item
              {...restField}
              label='Dia chi email'
              name={[name, `email_${typePersion}_${index}`]}
              rules={[{ required: true, message: 'Missing Dia chi email' }]}
            >
              <Input placeholder='Dia chi email' />
            </Form.Item>
          </Space>
          <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
            <Form.Item
              {...restField}
              name={[name, `phoneNumber_${typePersion}_${index}`]}
              rules={[{ required: true, message: 'Missing last name' }]}
            >
              <Input placeholder='So dien thoai 2' />
            </Form.Item>{' '}
            <Form.Item
              {...restField}
              name={[name, `email_${typePersion}_${index}`]}
              rules={[{ required: true, message: 'Missing last name' }]}
            >
              <Input placeholder='Dia chi email 2' />
            </Form.Item>
          </Space>
        </>
      ) : typePersion === "infant" ? (
        <>
          <Form.Item
            {...restField}
            label='Nguoi lon di kem'
            name={[name, `add_parent_${typePersion}_${index}`]}
            rules={[{ required: true, message: 'Missing So dien thoai' }]}
          >
            <Input placeholder='Nguoi lon di kem' />
          </Form.Item>
        </>
      ) : (
        <></>
      )}
      <h1>Khach hang thuong xuyen</h1>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} name={[name, `type_fight_${typePersion}_${index}`]} label='Hang hang khong'>
          <Input placeholder='Hang hang khong' />
        </Form.Item>
        <Form.Item {...restField} name={[name, `e_ticket_${typePersion}_${index}`]} label='So the'>
          <Input placeholder='So the' />
        </Form.Item>
      </Space>
    </>
  );
};
