import { Button, Collapse, CollapseProps } from 'antd';
import type { CSSProperties } from 'react';
import { OpenCollapseIcon } from '@/shared/components/icons/OpenCollapseIcon';
import { CloseCollapseIcon } from '@/shared/components/icons/CloseCollapseIcon';
import { MainForm } from './MainForm';

export const ConstantCollapses = ({form, index, typePersion}:any) => {
  const onChange = (key: string | string[]) => {
  };
  const panelStyle = {
    marginBottom: 24,
    background: '#fff',
    borderBottom: '1px solid #555',
  };
  const getItems: (panelStyle: CSSProperties) => CollapseProps['items'] = panelStyle => [
    {
      key: '1',
      label: 'Giay to tuy than',
      children: <MainForm index={index} typePersion={typePersion} form={form} type={"identity_doc"}/>,
      style: panelStyle,
    },
    {
      key: '2',
      label: 'Dia chi cu tru',
      children: <MainForm index={index} typePersion={typePersion} form={form} type={"address_residence"}/>,
      style: panelStyle,
    },
    {
      key: '3',
      label: 'Dia diem den',
      children: <MainForm index={index} typePersion={typePersion} form={form} type={"address_destination"}/>,
      style: panelStyle,
    },
    {
      key: '4',
      label: 'Thong tin visa',
      children: <MainForm index={index} typePersion={typePersion} form={form} type={"visa_infor"}/>,
      style: panelStyle,
    },
  ];
  return (
      <Collapse
        bordered={false}
        expandIcon={({ isActive }) => (isActive ? <CloseCollapseIcon /> : <OpenCollapseIcon />)}
        style={{ background: '#fff' }}
        items={getItems(panelStyle)}
        onChange={onChange}
      />
  );
};
