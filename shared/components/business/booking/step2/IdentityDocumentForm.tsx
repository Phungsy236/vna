import { Form, Input, Select, Space } from 'antd';
export const IdentityDocumentForm = ({typePersion, index, name, ...restField }: any) => {
  return (
    <>
      <h1>Loai giay to tuy than</h1>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} name={[name, `docs_${typePersion}_${index}`]} label='Loai giay to tuy than'>
          <Select placeholder='Loai giay to tuy than'>
            <Select.Option value='Ho chieu'>Ho chieu</Select.Option>
            <Select.Option value='VISA'>VISA</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item {...restField} name={[name, `number_${typePersion}_${index}`]} label='So'>
          <Input placeholder='So' />
        </Form.Item>
      </Space>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} name={[name, `country_${typePersion}_${index}`]} label='Quoc gia cap'>
          <Input placeholder='Quoc gia' />
        </Form.Item>
        <Form.Item {...restField} name={[name, `date_${typePersion}_${index}`]} label='Ngay het han'>
          <Input placeholder='Ngay het han' />
        </Form.Item>
      </Space>
    </>
  );
};