import { Button, Card } from 'antd';
import type { CollapseProps } from 'antd';
import { Collapse } from 'antd';
import React, { useEffect, useState } from 'react';
import { MainForm } from './MainForm';
import { ConstantCollapses } from './ConstantCollapses';
import { useForm } from 'antd/lib/form/Form';
import { useAppDispatch, useAppSelector } from '@/shared/hooks/useRedux';
import Box from '../common/Box';

export const LayoutPassengers = () => {
  const { valueSearch } = useAppSelector(state => state.bookingSlice);
  const lengthPasseneger = {
    adult: valueSearch.numberAdult,
    children: valueSearch.numberChildren,
    infant: valueSearch.numberInfant,
  };
  const [listItemCollapse, setListItemCollapse] = useState<CollapseProps['items']>();
  const [form] = useForm();
  // const [isDependence, ]form dependence
  useEffect(() => {
    const totalPassengerAdult = lengthPasseneger.adult || 0;
    const totalPassengerChildren = lengthPasseneger.children || 0;
    const totalPassengerInfant = lengthPasseneger.infant || 0;
    const passengerAdult = new Array(totalPassengerAdult);
    const passengerChildren = new Array(totalPassengerChildren);
    const passengerInfant = new Array(totalPassengerInfant);

    for (let i = 0; i < totalPassengerAdult; i++) {
      passengerAdult[i] = i;
    }
    for (let i = 0; i < totalPassengerChildren; i++) {
      passengerChildren[i] = i;
    }
    for (let i = 0; i < totalPassengerInfant; i++) {
      passengerInfant[i] = i;
    }

    const resultAdult = passengerAdult.map((passenger) => {
      return defaultItem('Nguoi lon', passenger, "adults");
    });
    const resultChildren = passengerChildren.map((passenger) => {
      return defaultItem('Tre em', passenger, "children");
    });
    const resultInfant = passengerInfant.map((passenger) => {
      return defaultItem('Tre so sinh', passenger, "infant");
    });
    const totalResult = resultAdult.concat(resultChildren);
    setListItemCollapse(totalResult.concat(resultInfant));
  }, []);
  const onChange = (key: string | string[]) => {
    // console.log(key);
    // console.log(key[1].split(" ")[2])
  };
  const defaultItem = (title: string, index: number, typePersion: string) => {
    return {
      key: `${title} ${index + 1}`,
      label: `${title} ${index + 1}`,
      children: (
        <>
          <MainForm form={form} type={"basic_infor"} index={index} typePersion={typePersion} />
          <ConstantCollapses index={index} typePersion={typePersion} form={form} />
        </>
      ),
    };
  };
  const handleSubmit = () => {
    form.validateFields().then((values) => {
      console.log(values);
    });
  };
  return (
    <>
      <Box className='mt-8 p-4'>
        <div className='mb-4 text-lg font-bold'>Thong tin hanh khach</div>
        <Collapse items={listItemCollapse} defaultActiveKey={['1']} onChange={onChange} />
      </Box>

    </>
  );
};
