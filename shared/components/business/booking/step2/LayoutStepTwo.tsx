import { useAppSelector } from '@/shared/hooks/useRedux';
import { Affix, Button, Col, Row } from 'antd';
import { LayoutPassengers } from './LayoutPassengers';
import ResultSearch from '../step1/ResultSearch';
import { ItemCardFlightPicked } from '../common/ItemCardFlightPicked';
import LeftSideBookingCardSummary from '../common/LeftSideBookingCardSummary';
import CACode from '../common/CACode';

export const LayoutStepTwo = () => {

  function handleFinishStep2() {

  }
  return (
    <>
      {(
        <section className='mt-4'>
          <Row wrap justify={'space-between'} gutter={[16, 16]}>
            <Col span={16}>
              <Row justify={'space-between'} align={'middle'} gutter={[16, 16]}>
                <Col span={24}>
                  <CACode />
                </Col>
                <Col span={24}>
                  <ItemCardFlightPicked />
                  <div className='mt-4'></div>
                  <ItemCardFlightPicked />
                </Col>
                <Col span={24}>
                  <LayoutPassengers />
                </Col>
              </Row>
              <div className='mt-4 flex gap-4 justify-end'>

                <Button>Quay lai</Button>
                <Button type='primary' onClick={handleFinishStep2}>Tiep tuc</Button>
              </div>
            </Col>
            <Col span={8}>
              <Affix offsetTop={100}>
                <LeftSideBookingCardSummary />
              </Affix>
            </Col>
          </Row>
        </section>
      )}
    </>
  );
};
