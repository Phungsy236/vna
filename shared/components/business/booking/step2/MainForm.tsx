import React, { useEffect, useState } from 'react';
import { Button, Form, Input, Select, Space } from 'antd';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';
import { BasicInformationForm } from './BasicInformationForm';
import { IdentityDocumentForm } from './IdentityDocumentForm';
import { AddressOfResidenceForm } from './AddressOfResidenceForm';
import { AddressOfDestinationForm } from './AddressOfDestinationForm';
import { VISAInformatioForm } from './VISAInformatioForm';
import { useAppSelector } from '@/shared/hooks/useRedux';
dayjs.locale('zh-cn');

export const MainForm = ({ form, type, index, typePersion }: any) => {
  const [isAdd, setIsAdd] = useState(false);
  const adults = 2;
  const onFinish = (values: any) => {
    console.log('Received values of form:', values);
  };
  return (
    <Form
      form={form}
      layout='vertical'
      name='dynamic_form_nest_item'
      onFinish={onFinish}
      style={{ maxWidth: 600 }}
      autoComplete='off'
    >
      {type === 'basic_infor' ? (
        <Form.List name='basic_infor'>
          {fields => (
            <>
              {fields.map(({ key, name, ...restField }) => (
                <BasicInformationForm
                  typePersion={typePersion}
                  index={index}
                  key={key}
                  form={form}
                  name={name}
                  restField={restField}
                />
              ))}
            </>
          )}
        </Form.List>
      ) : type === 'identity_doc' ? (
        <Form.List name='identity_doc'>
          {fields => (
            <>
              {fields.map(({ key, name, ...restField }) => (
                <IdentityDocumentForm typePersion={typePersion}
                  index={index} key={key} form={form} name={name} restField={restField} />
              ))}
            </>
          )}
        </Form.List>
      ) : type === 'address_residence' ? (
        <Form.List name='address_residence'>
          {(fields, { add }) => (
            <>
              {fields.map(({ key, name, ...restField }) => (
                <AddressOfResidenceForm key={key} form={form} name={name} restField={restField} />
              ))}
              <Form.Item>
                <Button type='dashed' onClick={() => add()} block>
                  Add field
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
      ) : type === 'address_destination' ? (
        <Form.List name='address_destination'>
          {(fields, { add }) => (
            <>
              {fields.map(({ key, name, ...restField }) => (
                <AddressOfDestinationForm key={key} form={form} name={name} restField={restField} />
              ))}
              <Form.Item>
                <Button type='dashed' onClick={() => add()} block>
                  Add field
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
      ) : type === 'visa_infor' ? (
        <Form.List name='visa_infor'>
          {(fields, { add }) => (
            <>
              {fields.map(({ key, name, ...restField }) => (
                <VISAInformatioForm key={key} form={form} name={name} restField={restField} />
              ))}
              <Form.Item>
                <Button type='dashed' onClick={() => add()} block>
                  Add field
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
      ) : (
        <></>
      )}
    </Form>
  );
};
