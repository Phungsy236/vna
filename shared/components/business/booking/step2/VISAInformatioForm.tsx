import { Form, Input, Select, Space } from 'antd';
export const VISAInformatioForm = ({ name, ...restField }: any) => {
  return (
    <>
      <h1>Thong tin VISA</h1>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} name={[name, 'danh xung']} label='Danh xung'>
          <Select placeholder='Danh xung'>
            <Select.Option value='Ong'>Ong</Select.Option>
            <Select.Option value='Ba'>Ba</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item {...restField} name={[name, 'Ten dem va ten']} label='Ten dem va ten'>
          <Input placeholder='Ten dem va ten' />
        </Form.Item>
        <Form.Item {...restField} name={[name, 'Ho']} label='Ho'>
          <Input placeholder='Ho' />
        </Form.Item>
      </Space>
      <Space style={{ display: 'flex', marginBottom: 8 }} align='baseline'>
        <Form.Item {...restField} label='Ngay'>
          <Input placeholder='Ngay' />
        </Form.Item>
        <Form.Item {...restField} label='Thang'>
          <Input placeholder='Thang' />
        </Form.Item>
        <Form.Item {...restField} label='Nam'>
          <Input placeholder='Nam' />
        </Form.Item>
      </Space>
    </>
  );
};