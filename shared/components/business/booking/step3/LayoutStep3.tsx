import React from 'react'
import { PickedFlight } from '../step1/PickedFlight'
import { Card, Button, Row, Col, Affix } from 'antd'
import { useDispatch } from 'react-redux'
import { nextStep, prevStep } from '@/shared/stores/bookingSlice'
import { ItemCardFlightPicked } from '../common/ItemCardFlightPicked'
import Box from '../common/Box'
import PrepaidBaggage from './PrePaidBadge'
import SeatMap from './SeatMap'
import LeftSideBookingCardSummary from '../common/LeftSideBookingCardSummary'

type Props = {}

export default function LayoutStep3({ }: Props) {
    const dispath = useDispatch()
    return (
        <section className='mt-4'>
            <Row wrap justify={'space-between'} gutter={[16, 16]}>
                <Col span={16}>
                    <Box className='flex flex-col gap-4 border-none '>
                        <ItemCardFlightPicked />
                        <ItemCardFlightPicked />
                    </Box>

                    <Box className='mt-4 px-5 py-4 flex flex-col gap-4'>
                        <div className='text-lg font-bold'>Ancillary services</div>
                        <PrepaidBaggage />
                        <SeatMap />
                    </Box>
                    <div className='flex justify-end gap-4 mt-4'>
                        <Button onClick={() => dispath(prevStep())}>Quay lai</Button>
                        <Button onClick={() => dispath(nextStep())} type='primary'>Tiep tuc</Button>
                    </div>
                </Col>
                <Col span={8}>
                    <Affix offsetTop={100}>
                        <LeftSideBookingCardSummary />
                    </Affix>
                </Col>
            </Row>
        </section>
    )
}