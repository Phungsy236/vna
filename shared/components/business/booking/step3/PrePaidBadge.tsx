
import AmountInput from '@/shared/components/common/AmountInput'
import { PrepaidBaggageIcon } from '@/shared/components/icons/PrePaidBadgeIcon'
import { useAppDispatch } from '@/shared/hooks/useRedux'
import useTrans from '@/shared/hooks/useTrans'
import { formatNumber } from '@/shared/utils/functions/formatCurrentcy'
import { Button, Modal } from 'antd'
import { useForm } from 'antd/lib/form/Form'
import classNames from 'classnames'
import React, { useState } from 'react'
import { twMerge } from 'tailwind-merge'

type Props = {}

const Row = ({ children }: { children: React.ReactNode }) => <div className="flex border-collapse w-full gap-5 !divide-x  !divide-slate-100">{children}</div>
const Box = ({ children, className }: { children?: React.ReactNode, className?: string }) => <div
    className={twMerge("flex justify-center items-center flex-1 py-3 flex-wrap", className)} >{children}</div>


export default function PrepaidBaggage({ }: Props) {
    const [openSelect, setOpenSelect] = useState(false)
    const adults = [
        {
            id: 1,
            name: 'sypv'
        },
        {
            id: 2,
            name: 'thuanvv'
        },
        {
            id: 3,
            name: 'dungtq'
        },
        {
            id: 4,
            name: 'dungcamlang'
        }
    ]
    const baggages = [
        {
            id: 1,
            name: '10kg'
        },
        {
            id: 2,
            name: '20kg'
        },
        {
            id: 3,
            name: '15kg'
        },
    ]
    const flights = [{
        id: 1,
        name: 'Hà Nội (HAN) -- TP HCM (SGN)',
        baggages: 0,
        prices: 0,
        active: true
    },
    {
        id: 2,
        name: 'TP HCM (SGN) -- Hà Nội (HAN)',
        baggages: 1,
        prices: 0,
        active: false
    }
    ]

    const dispatch = useAppDispatch()
    const { currentcy } = useTrans()
    const [form] = useForm()
    return (
        <>
            <div className='border border-solid border-slate-200 p-5'>
                <div className='flex justify-between p-4'>
                    <div className='flex items-center gap-3'>
                        <PrepaidBaggageIcon />
                        <span className='font-bold text-base'>Prepaid baggage</span>
                    </div>
                    <Button onClick={() => setOpenSelect(true)}>Add baggage</Button>
                </div>
                <div>
                    Hành lý trả trước  là dịch vụ mua hành lý quá cước và thanh toán trước để hưởng mức giá ưu đãi hơn so với giá mua tại sân bay. Phí hành lý trả trước đã mua sẽ không đươc phép hoàn
                </div>
            </div>
            <Modal title={<></>} footer={<></>} open={openSelect} width={'100vw'} onCancel={() => setOpenSelect(false)}>
                <div className='grid grid-cols-12 '>
                    <div className='col-span-3 '>
                        <div className='flex items-center flex-2 font-bold py-4 gap-3'>
                            <PrepaidBaggageIcon />
                            Add baggage
                        </div>
                        <div>
                            <div className='font-bold'>Select flight</div>
                            <div>Thêm hành lý ký gửi vào chuyến bay của bạn</div>
                        </div>

                        {/* casc chuyen bay */}
                        {flights.map(flight => <div className={classNames(`p-4 cursor-pointer`, {
                            'bg-slate-200': flight.active
                        })} key={flight.id}>
                            <div className='font-bold'>{flight.name}</div>
                            <div className='flex justify-between'>
                                <span>{flight.baggages} baggages</span>
                                <span>{formatNumber(flight.prices, 'VND')} {currentcy}</span>
                            </div>
                        </div>)}


                    </div>

                    <div className='col-span-9 bg-slate-200 py-4 px-5'>
                        <div className='py-4'>
                            <div className='font-bold'>
                                Hà Nội (HAN) -- TP HCM (SGN)
                            </div>
                            <span className='text-sm '>
                                VN 0205: 05:00 10/05/2023 - 07:05 10/05/2023
                            </span>
                        </div>

                        {/* Table pick  */}
                        <div className={`bg-white`}>
                            <Row>
                                <Box></Box>
                                {baggages.map((item) =>
                                    <Box key={item.id}>
                                        <b>
                                            {item.name}
                                        </b>

                                    </Box>
                                )}
                                <Box></Box>
                                <Box></Box>
                            </Row>
                            {adults.map(person => <Row key={person.id}>
                                <Box className='justify-start pl-2'><b>{person.name}</b> </Box>
                                {baggages.map(bagage => <Box key={bagage.id}><AmountInput form={form} initAmount={0} inputName={person.name + '-' + bagage.name} /></Box>)}

                                <Box>0 pieces</Box>
                                <Box><b>{formatNumber(230000, 'VN')}{currentcy} </b></Box>
                            </Row>)}
                        </div>

                    </div>
                </div>

                {/* Footer */}
                <div className='flex justify-between pt-4'>
                    <div className='p-1'>
                        <div>
                            Total (6 baggages)
                        </div>
                        <div className='font-bold text-2xl'>
                            1.430.000 VNĐ
                        </div>
                    </div>
                    <Button onClick={() => setOpenSelect(false)} type='primary'>Complete</Button>
                </div>
            </Modal >
        </>

    )
}