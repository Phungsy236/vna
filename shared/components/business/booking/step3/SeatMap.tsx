import FlightClassIcon from '@/shared/components/icons/FlightClassIcon'
import useTrans from '@/shared/hooks/useTrans'
import { formatNumber } from '@/shared/utils/functions/formatCurrentcy'
import { Button, Modal } from 'antd'
import classNames from 'classnames'
import { DeleteIcon, Trash2Icon } from 'lucide-react'
import React, { useState } from 'react'
import Deck from './SeatMap/Deck'
import SeatMapDisplay from './SeatMap/SeatMapDisplay'

type Props = {}

export default function SeatMap({ }: Props) {
    const [openModal, setOpenModal] = useState(false)
    const adults = [
        {
            id: 1,
            name: 'sypv',
            active: true
        },
        {
            id: 2,
            name: 'thuanvv'
        },
        {
            id: 3,
            name: 'dungtq'
        },
        {
            id: 4,
            name: 'dungcamlang'
        },
        {
            id: 5,
            name: 'dungem'
        },
        {
            id: 5,
            name: 'hunganh'
        }
    ]
    const flights = [{
        id: 1,
        name: 'Hà Nội (HAN) -- TP HCM (SGN)',
        baggages: 0,
        prices: 0,
        active: true
    },
    {
        id: 2,
        name: 'TP HCM (SGN) -- Hà Nội (HAN)',
        baggages: 1,
        prices: 0,
        active: false
    }]
    const { currentcy } = useTrans()

    return (
        <div className='border border-solid border-slate-200 p-5'>
            <div className='flex justify-between p-4'>
                <div className='flex items-center gap-3'>
                    <FlightClassIcon />
                    <span className='font-bold text-base'>Seat</span>
                </div>
                <Button onClick={() => setOpenModal(true)}>Select seat</Button>
            </div>
            <ul>
                <li>
                    Chỗ ngồi chỉ được xác nhận cho đến khi Quý khách hoàn thành thanh toán. Phí chọn chỗ ngồi không được phép hoàn. Trong trường hợp Quý khách không chọn chỗ trước, chỗ ngồi của Quý khách sẽ được hệ thống xếp tự động trong vòng 25 giờ trước chuyến bay
                </li>
                <li>
                    Để thay đổi chỗ ngồi đã mua. Quý khách liên hệ Vietnam Airlines để hỗ trợ xử lý
                </li>

            </ul>
            <Modal title={<></>} footer={<></>} open={openModal} width={'100vw'} onCancel={() => setOpenModal(false)}>
                <div className='grid grid-cols-12 '>
                    <div className='col-span-3 '>
                        <div className='flex items-center flex-2 font-bold py-4 gap-3'>
                            <FlightClassIcon />
                            Choose seats
                        </div>
                        <div>
                            <div className='font-bold'>Select flight</div>
                            <div>Add seats to your flight</div>
                        </div>

                        {/* casc chuyen bay */}
                        {flights.map(flight => <div className={classNames(`p-4 cursor-pointer`, {
                            'bg-slate-200': flight.active
                        })} key={flight.id}>
                            <div className='font-bold'>{flight.name}</div>
                            <div className='flex justify-between'>
                                <span>{flight.baggages} baggages</span>
                                <span>{formatNumber(flight.prices, 'VND')} {currentcy}</span>
                            </div>
                        </div>)}
                    </div>

                    <div className='col-span-9 bg-slate-200 py-4 px-5'>
                        <div className='py-4'>
                            <div className='font-bold'>
                                Hà Nội (HAN) -- TP HCM (SGN)
                            </div>
                            <span className='text-sm '>
                                VN 0205: 05:00 10/05/2023 - 07:05 10/05/2023
                            </span>
                        </div>

                        {/* Table pick  */}
                        <div className={`bg-white`}>
                            <div className='flex overflow-x-auto'>
                                {adults.map(person => <div key={person.id}
                                    className={classNames('border border-solid border-slate-200 border-collapse py-4 px-5 min-w-[200px]', {
                                        'bg-slate-300': person.active
                                    })}>
                                    <b>{person.name}</b>
                                    <div className='flex justify-between items-center mt-2'>
                                        <Button className='roun ded p-2'>12A</Button>
                                        <Trash2Icon />
                                    </div>
                                </div>)}
                            </div>
                            {/* symbol explain */}
                            <div className='flex gap-6 items-center'>
                                <div className='bg-[#499167] w-12 h-12'></div> <b> Avalable</b>
                                <div className='bg-[#FE5F55] w-12 h-12'></div> <b>Not Avalable</b>
                            </div>
                            <div>
                                <SeatMapDisplay />
                            </div>
                        </div>

                    </div>
                </div>

                {/* Footer */}
                <div className='flex justify-between pt-4'>
                    <div className='p-1'>
                        <div>
                            Total (6 baggages)
                        </div>
                        <div className='font-bold text-2xl'>
                            1.430.000 VNĐ
                        </div>
                    </div>
                    <Button onClick={() => setOpenModal(false)} type='primary'>Complete</Button>
                </div>
            </Modal >
        </div>
    )
}