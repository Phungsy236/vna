import Exit from "./Exit";
import Facility from "./Facilities";
import Seat from "./Seat";
import { SEAT_SIZE } from "./SeatMapDisplay";

const Deck = (props: any) => {
    const width = props.deck.deckConfiguration.width;
    const length = props.deck.deckConfiguration.length;
    const displaySeats = (seatList: any) => {
        return <div>{seatList.map((seat: any, index: number) =>
            <Seat key={index} number={seat.number} x={seat.coordinates.x} y={seat.coordinates.y} availability={seat.travelerPricing[0].seatAvailabilityStatus} />
        )}</div>
    }
    const displayExits = (exitRows: any , deckWidth : any) => {
        return <div>{exitRows.map((row: any, index: number) => <Exit key={index} row={row} deckWidth = {deckWidth}/>)}</div>
    }
    const displayFacilities = (facilityList: any) => {
        return <div id="facilities">{facilityList.map((facility: any, index: number) =>
            <Facility key={index} code={facility.code} x={facility.coordinates.x} y={facility.coordinates.y} />
        )}</div>
    }
    return (
        <div id="deck" style={{ width: `${width * SEAT_SIZE}px`, height: `${length * SEAT_SIZE}px` }}>
            {displaySeats(props.deck.seats)}
            {displayExits(props.deck.deckConfiguration.exitRowsX , props.deck.deckConfiguration.width )}
            {displayFacilities(props.deck.facilities)}
        </div>
    )
}
export default Deck;