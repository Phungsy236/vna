import { CSSProperties } from "react";
import { SEAT_SIZE } from "./SeatMapDisplay";

const Exit = (props: any) => {
    const styleLeft = {
        position: "absolute",
        left: `${-2 * SEAT_SIZE}px`,
        top: `${props.row * SEAT_SIZE}px`,
        width: SEAT_SIZE,
        height: SEAT_SIZE,
        backgroundColor: "#499167",
    }
    const styleRight = {
        position: "absolute",
        left: `${props.deckWidth * SEAT_SIZE + SEAT_SIZE}px`,
        top: `${props.row * SEAT_SIZE}px`,
        width: SEAT_SIZE,
        height: SEAT_SIZE,
        backgroundColor: "#499167"
    }
    return <div className="exit" >
        <span style={styleLeft as CSSProperties} className="flex justify-center items-center font-bold">EXIT</span>
        <span style={styleRight as CSSProperties} className="flex justify-center items-center font-bold">EXIT</span>
    </div>
}
export default Exit;