import { SEAT_SIZE } from "./SeatMapDisplay";

const Facility = (props: any) => {
  const left = `${props.y * SEAT_SIZE}px `;
  const top = `${props.x * SEAT_SIZE}px`;
  return (
    <div className='facility flex justify-center items-center' style={{ position: "absolute", left: left, top: top, width: SEAT_SIZE, height: SEAT_SIZE, backgroundColor: "#F5EE9E" }}>
      <p>{props.code}</p>
    </div>
  )
}
export default Facility;