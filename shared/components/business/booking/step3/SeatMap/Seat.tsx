import { SEAT_SIZE } from "./SeatMapDisplay";

const Seat = (props: any) => {
    const color = props.availability === 'AVAILABLE' ? "#499167" : "#FE5F55";

    return (
        <div className='absolute flex justify-center items-center border border-white border-solid cursor-pointer' style={{
            position: "absolute",
            left: `${props.y * SEAT_SIZE}px`,
            top: `${props.x * SEAT_SIZE}px`,
            backgroundColor: color,
            color: "white",
            lineHeight: 0,
            width: SEAT_SIZE,
            height: SEAT_SIZE,
        }}>
            <p>{props.number}</p>
        </div>
    )
}
export default Seat;