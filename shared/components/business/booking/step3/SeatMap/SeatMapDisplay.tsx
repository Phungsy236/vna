import seatMap from './data.json';
import Deck from './Deck';


export const SEAT_SIZE = 40
function SeatMapDisplay() {
  return (
    <div className='flex justify-center mt-4'>
      <div className='relative' >
        {seatMap.data[0].decks.map((deck, i) => (
          <Deck deck={deck} key={i} />
        ))}
      </div>
    </div>

  );
}
export default SeatMapDisplay;