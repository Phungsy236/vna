import { nextStep, prevStep } from '@/shared/stores/bookingSlice'
import { Affix, Button, Col, Row } from 'antd'
import { useDispatch } from 'react-redux'
import Box from '../common/Box'
import { ItemCardFlightPicked } from '../common/ItemCardFlightPicked'
import CACode from '../common/CACode'
import { LayoutPassengers } from '../step2/LayoutPassengers'
import BaggageRule from '../common/BaggageRule'
import FareRule from '../common/FareRule'
import EmailReceiveNoti from '../common/EmailReceiveNoti'
import LeftSideBookingCardSummary from '../common/LeftSideBookingCardSummary'

type Props = {}

export default function LayoutStep4({ }: Props) {
    const dispath = useDispatch()
    return (
        <section className='mt-4'>
            <Row wrap justify={'space-between'} gutter={[16, 16]}>
                <Col span={16}>
                    <CACode />
                    <div className='flex flex-col gap-4 mt-8 border-none '>
                        <ItemCardFlightPicked />
                        <ItemCardFlightPicked />
                    </div>

                    <LayoutPassengers />
                    <FareRule className='mt-8' />
                    <BaggageRule className='mt-8' />
                    <EmailReceiveNoti className='mt-8' />
                    <div className='mt-8'>Time limit for payment: <b>10:00:00</b> </div>
                    <div className='flex justify-end gap-4 mt-4'>
                        <Button onClick={() => dispath(prevStep())}>Quay lai</Button>
                        <Button onClick={() => dispath(nextStep())} type='primary'>Pay later</Button>
                        <Button onClick={() => dispath(nextStep())} type='primary'>Pay Now</Button>
                    </div>
                </Col>
                <Col span={8}>
                    <Affix offsetTop={100}>
                        <LeftSideBookingCardSummary />
                    </Affix>
                </Col>
            </Row>
        </section>
    )
}