import useTrans from '@/shared/hooks/useTrans';
import { URLS } from '@/shared/utils/constants/appMenu';
import { APP_REGEX } from '@/shared/utils/constants/appContants';
import { Button, Checkbox, Collapse, DatePicker, Form, FormInstance, Input, Select, Spin, Switch } from 'antd';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import CountryInput from '../../common/CountryInput';
import LanguageInput from '../../common/LanguageInput';
import useLocationForm from '@/shared/hooks/useLocationForm';
import PhoneCodeInput from '../../common/PhoneCodeInput';
import TimezoneInput from '../../common/TimezoneInput';
import { ICA, useCreateCA, useGetDetailCA, useUpdateCA } from '@/shared/schema/models/ICA';
import CurrentcyInput from '../../common/CurrentcyInput';
import BankInput from '../../common/BankInput';

type Props = {
  form: FormInstance;
  editId?: React.Key;
  viewOnly?: boolean;
};

export default function FormCA({ form, editId, viewOnly }: Props) {
  const { trans } = useTrans();
  const router = useRouter();

  const { data, isLoading: isLoadingDetail } = useGetDetailCA({ id: editId!, options: { enabled: !!editId } });
  const { FormLocation } = useLocationForm({ form });
  const createCA = useCreateCA(() => router.push(URLS.SYSTEM_MANAGE.CA_MANAGE));
  const updateCA = useUpdateCA(() => router.push(URLS.SYSTEM_MANAGE.CA_MANAGE));

  function onFinish(value: any) {
    const formValue = { ...value };
    if (editId) updateCA.mutate({ CA: formValue, id: editId });
    else {
      createCA.mutate(formValue);
    }
  }
  useEffect(() => {
    if (editId) {
      const tmp = {
        ...data,
        province: data?.province.provinceId,
        district: data?.district.districtId,
        ward: data?.ward.wardId,
      };
      form.setFieldsValue(tmp);
    }
    return () => form.resetFields();
  }, [data, form, editId]);

  function renderFormCorporate() {
    return (
      <div className='grid grid-cols-3 gap-x-20 rounded-lg border border-solid border-gray-300 p-4'>
        <Form.Item
          name={'vietnameseName'}
          label={trans.page.CA.corporateInformation.vnCAName}
          rules={[
            {
              required: false,
              message: trans.common.form.require,
            },
            {
              pattern: APP_REGEX.VietnameseRegex,
              message: 'Nhập tên tiếng Việt',
            },
          ]}
        >
          <Input placeholder='Input Vietnamese Name' />
        </Form.Item>
        <Form.Item
          name='englishName'
          label={trans.page.CA.corporateInformation.enCAName}
          rules={[
            {
              required: false,
              message: trans.common.form.require,
            },
            {
              pattern: APP_REGEX.EnglishRegex,
              message: 'Nhập tên tiếng Anh',
            },
          ]}
        >
          <Input placeholder='Input English Name' />
        </Form.Item>
        <LanguageInput />
        <CountryInput />
        {FormLocation()}
        <Form.Item
          name={'address'}
          label={trans.page.CA.corporateInformation.address}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='address' />
        </Form.Item>

        <Form.Item
          name='abbreviatedName'
          label={trans.page.CA.corporateInformation.abbreviatedName}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Input Abbreviated Name' />
        </Form.Item>

        <Form.Item
          name='phoneNumber'
          label={trans.page.CA.phoneNumber}
          rules={[
            { required: false, message: trans.common.form.require },
            {
              pattern: APP_REGEX.VNPhoneRegex,
              message: trans.common.form.invalidPhone,
            },
          ]}
        >
          <Input addonBefore={<PhoneCodeInput />} placeholder='phoneNumber' />
        </Form.Item>
        <Form.Item
          name='email'
          label={trans.page.CA.email}
          rules={[
            { required: false, message: trans.common.form.require },
            { type: 'email', message: trans.common.form.invalidEmail },
          ]}
        >
          <Input placeholder='email' />
        </Form.Item>

        <Form.Item
          name='website'
          label={trans.page.CA.corporateInformation.website}
          rules={[
            { required: false, message: trans.common.form.require },
            {
              pattern: APP_REGEX.WebsiteRegex,
              message: 'Nhập đúng định dạng website',
            },
          ]}
        >
          <Input placeholder='Input website link' />
        </Form.Item>
        <Form.Item
          name='fax'
          label='Fax'
          rules={[
            { required: false, message: trans.common.form.require },
            {
              pattern: APP_REGEX.FaxRegex,
              message: 'Nhập đúng định dạng Fax',
            },
          ]}
        >
          <Input placeholder='Input fax' />
        </Form.Item>
        <Form.Item
          name='taxCode'
          label={trans.page.CA.corporateInformation.taxCode}
          rules={[
            { required: false, message: trans.common.form.require },
            {
              pattern: APP_REGEX.TaxRegex,
              message: 'Nhập đúng định dạng Tax',
            },
          ]}
        >
          <Input placeholder='Input tax code' />
        </Form.Item>
        <Form.Item
          name='businessRegistrationNo'
          label={trans.page.CA.corporateInformation.businessCode}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Business registration No' />
        </Form.Item>
        <BankInput />
        <Form.Item
          name='bankAccount'
          label={trans.page.CA.corporateInformation.bankAccount}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Input Bank account' />
        </Form.Item>

        <Form.Item
          name='accountHolder'
          label={trans.page.CA.corporateInformation.accountHolder}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Account holder' />
        </Form.Item>

        <Form.Item
          name='issueDate'
          label={trans.page.CA.corporateInformation.issueDate}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <DatePicker placeholder='Input Issue date' className='w-full' />
        </Form.Item>
        <Form.Item
          name='issuedBy'
          label={trans.page.CA.corporateInformation.issueBy}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Input issue by' />
        </Form.Item>
      </div>
    );
  }
  function renderLegalRepresent() {
    return (
      <div className='grid grid-cols-3 gap-x-20 rounded-lg border border-solid border-gray-300 p-4'>
        <Form.Item
          name='fullNameLegal'
          label={trans.page.CA.legalRepresentation.fullName}
          rules={[{ required: false, message: trans.common.form.require },{
            pattern: APP_REGEX.VietnameseRegex,
            message: 'Nhập tên tiếng Việt',
          }]}
        >
          <Input placeholder='Input Full Name' />
        </Form.Item>
        <Form.Item
          name='typeId'
          label={trans.page.CA.legalRepresentation.typeOfIdentityDocument}
          // rules={[{ required:false, message: trans.common.form.require }]}
        >
          <Select placeholder='Input type of identity document' />
        </Form.Item>
        <Form.Item name='legalId' label='ID' rules={[{ required: false, message: trans.common.form.require }]}>
          <Input placeholder='Input ID' />
        </Form.Item>
        <Form.Item
          name='position'
          label={trans.page.CA.legalRepresentation.regPosition}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Input ID' />
        </Form.Item>
        <Form.Item
          name='issueDateLegal'
          label={trans.page.CA.corporateInformation.issueDate}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <DatePicker placeholder='Input Issue date' className='w-full' />
        </Form.Item>
        <Form.Item
          name='issueByLegal'
          label={trans.page.CA.corporateInformation.issueBy}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Input Issue by' />
        </Form.Item>

        <Form.Item
          name='addressLegal'
          label={trans.page.CA.corporateInformation.address}
          rules={[{ required: false, message: trans.common.form.require }]}
        >
          <Input placeholder='Input Address' />
        </Form.Item>
      </div>
    );
  }
  function renderOther() {
    return (
      <>
        <div className='col-span-3 my-4 text-xl font-bold'>{trans.page.CA.form.registrationArea}</div>
        <div className='grid grid-cols-2 gap-x-20 rounded-lg border border-solid border-gray-300 p-4'>
          <TimezoneInput />
          <Form.Item name='active' label={trans.common.status} valuePropName='checked' initialValue={true}>
            <Switch checkedChildren={trans.common.active} unCheckedChildren={trans.common.deActive} />
          </Form.Item>
        </div>

        <div className='col-span-3 my-4 text-xl font-bold'>{trans.page.CA.form.committedRevenue}</div>
        <div className='grid grid-cols-2 gap-x-20 rounded-lg border border-solid border-gray-300 p-4'>
          <Form.Item
            name='international'
            label={trans.page.CA.registration.interRevenue}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <CurrentcyInput placeholder='Input International revenue' />
          </Form.Item>
          <Form.Item
            name='domestic'
            label={trans.page.CA.registration.domRevenue}
            rules={[{ required: false, message: trans.common.form.require }]}
          >
            <CurrentcyInput placeholder='Input domestic revenue' />
          </Form.Item>
        </div>
        <div className='col-span-3 my-4 text-xl font-bold'>{trans.page.CA.form.agentRegistration}</div>
        <div className='grid grid-cols-1 gap-x-20 rounded-lg border border-solid border-gray-300 p-4'>
          <Form.Item
            name='agents'
            label={trans.page.CA.registration.agentID}
            // rules={[{ required:false, message: trans.common.form.require }]}
          >
            <Select showSearch mode='multiple' />
          </Form.Item>
        </div>
      </>
    );
  }
  return (
    <div className='w-full'>
      <Spin spinning={!!editId && !!isLoadingDetail}>
        <Form
          preserve={false}
          form={form}
          onFinish={onFinish}
          layout='vertical'
          labelWrap
          size='large'
          colon={false}
          scrollToFirstError
          disabled={viewOnly}
        >
          <Collapse
            ghost
            defaultActiveKey={[1, 2, 3]}
            items={[
              {
                key: 1,
                label: <div className='col-span-3 text-xl font-bold '>{trans.page.CA.form.corporateInformation}</div>,
                children: renderFormCorporate(),
              },
              {
                key: 2,
                label: <div className='col-span-3 text-xl font-bold'>{trans.page.CA.form.legalRepresentation}</div>,
                children: renderLegalRepresent(),
              },
              {
                key: 3,
                label: <div className='col-span-3 text-xl font-bold'>{trans.page.CA.form.registration}</div>,
                children: renderOther(),
              },
            ]}
          ></Collapse>

          {!viewOnly && (
            <Form.Item className='col-span-3 flex justify-end'>
              <Button
                htmlType='reset'
                className='!rounded-none'
                onClick={() => {
                  form.resetFields();
                  router.push(URLS.SYSTEM_MANAGE.CA_MANAGE);
                }}
              >
                {trans.common.back}
              </Button>

              <Button
                type='primary'
                className='ml-4 mt-4 !rounded-none'
                htmlType='submit'
                loading={createCA.isLoading || updateCA.isLoading}
              >
                {trans.common.save}
              </Button>
            </Form.Item>
          )}
        </Form>
      </Spin>
      {viewOnly && (
        <div className='flex justify-end gap-4'>
          <Button
            onClick={() => {
              router.push(URLS.SYSTEM_MANAGE.CA_MANAGE);
            }}
          >
            {trans.common.close}
          </Button>
          <Button
            type='primary'
            onClick={() => {
              router.push(URLS.SYSTEM_MANAGE.CA_MANAGE + '/update/' + editId);
            }}
          >
            {trans.common.edit}
          </Button>
        </div>
      )}
    </div>
  );
}
