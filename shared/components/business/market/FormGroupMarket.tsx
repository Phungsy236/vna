import useTrans from '@/shared/hooks/useTrans';
import { useCreateCategory, useGetDetailCategory, useUpdateCategory } from '@/shared/schema/models/ICategory.mock';
import {
  useCreateGroupMarket,
  useGetBasicActiveListGroupMarket,
  useGetBasicActiveListMarket,
  useGetDetailGroupMarket,
  useGetDetailMarket,
  useGetListMarket,
  useUpdateGroupMarket,
} from '@/shared/schema/models/IMarket';
import { Button, Form, FormInstance, Input, Modal, Select, Switch } from 'antd';
import React, { useEffect, useState } from 'react';

type Props = {
  form: FormInstance;
  editId?: React.Key;
};

export default function FormGroupMarket({ form, editId }: Props) {
  const { trans } = useTrans();
  const createGroupMarket = useCreateGroupMarket(() => Modal.destroyAll());
  const updateGroupMarket = useUpdateGroupMarket(() => Modal.destroyAll());
  const { data: markets } = useGetBasicActiveListMarket();
  const { data: groupMarkets } = useGetListMarket();
  const { data } = useGetDetailGroupMarket({ id: editId!, options: { enabled: editId !== undefined } });
  function onFinish(value: any) {
    if (editId) {
      const formData = {
        ...value,
        id: editId,
        markets: groupMarkets?.data && groupMarkets?.data.content.filter(obj => value.markets.includes(obj.id)),
      };
      updateGroupMarket.mutate({ groupMarket: formData, id: editId });
    } else {
      const formData = {
        ...value,
        markets: groupMarkets?.data && groupMarkets?.data.content.filter(obj => value.markets.includes(obj.id)),
      };
      createGroupMarket.mutate(formData);
    }
  }
  useEffect(() => {
    if (editId && data) {
      const formData = {
        ...data,
        markets: data.marketList.map(item => {
          return {
            value: item.id,
            label: item.groupMarketName,
          };
        }),
      };
      form.setFieldsValue(formData);
    }
  }, [data, form]);
  return (
    <div className='monitor:w-[600px] w-[200px] mobile:w-[300px] ipad:w-[400px] tablet:w-[500px] desktop:w-[550px]'>
      <div className='mb-2 text-2xl font-bold'>{editId ? 'Chỉnh sửa group market' : 'Tao moi group market'}</div>
      <Form className='mx-auto w-full' preserve={false} form={form} onFinish={onFinish} layout='vertical' colon>
        <Form.Item
          name='groupMarketName'
          label='Group Market Name'
          rules={[{ required: true, message: trans.common.form.require }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name='description'
          label='Description'
          rules={[{ required: true, message: trans.common.form.require }]}
        >
          <Input />
        </Form.Item>

        <Form.Item name='markets' label='Markets' rules={[{ required: true, message: trans.common.form.require }]}>
          <Select mode='multiple' options={markets && markets} />
        </Form.Item>

        <Form.Item className='items-start' valuePropName='checked' name='active' label={trans.common.status} rules={[{ required: true }]}>
          <Switch></Switch>
        </Form.Item>

        <Form.Item className='flex justify-center '>
          <Button
            type='primary'
            htmlType='submit'
            className='mr-2'
            loading={createGroupMarket.isLoading || updateGroupMarket.isLoading}
          >
            {trans.common.save}
          </Button>
          <Button
            htmlType='reset'
            onClick={() => {
              Modal.destroyAll();
            }}
          >
            {trans.common.close}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
