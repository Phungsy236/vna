import useTrans from '@/shared/hooks/useTrans';
import {
  useCreateMarket,
  useGetBasicActiveListGroupMarket,
  useGetDetailGroupMarket,
  useGetDetailMarket,
  useUpdateMarket,
} from '@/shared/schema/models/IMarket';
import { Button, Form, FormInstance, Input, Modal, Select, Switch } from 'antd';
import React, { useEffect, useState } from 'react';

type Props = {
  form: FormInstance;
  editId?: React.Key;
};

export default function FormMarket({ form, editId }: Props) {
  const { trans } = useTrans();
  const [groupMarketId, setGroupMarketId] = useState<React.Key>();
  const createMarket = useCreateMarket(() => Modal.destroyAll());
  const updateMarket = useUpdateMarket(() => Modal.destroyAll());
  const { data: listGroupMarket } = useGetBasicActiveListGroupMarket();
  const { data: groupMarket } = useGetDetailGroupMarket({
    id: groupMarketId!,
    options: { enabled: groupMarketId !== undefined },
  });
  const { data } = useGetDetailMarket({ id: editId!, options: { enabled: editId !== undefined } });

  function onFinish(value: any) {
    if (editId) {
      const formData = {
        ...value,
        id: editId,
        groupMarketDto: groupMarket,
      };
      updateMarket.mutate({ market: formData, id: editId });
    } else {
      const formData = {
        ...value,
        groupMarketDto: groupMarket,
      };
      createMarket.mutate(formData);
    }
  }
  useEffect(() => {
    if (editId && data) {
      const formData = {
        ...data,
        status: data.active,
        groupMarketDto: data.groupMarket && data.groupMarket.id
      };
      form.setFieldsValue(formData);
    }
  }, [data, form]);
  return (
    <div className='monitor:w-[600px] w-[200px] mobile:w-[300px] ipad:w-[400px] tablet:w-[500px] desktop:w-[550px]'>
      <div className='mb-2 text-2xl font-bold'>{editId ? 'Chỉnh sửa market' : 'Tao moi market'}</div>
      <Form className='mx-auto w-full' preserve={false} form={form} onFinish={onFinish} layout='vertical' colon>
        <Form.Item
          name='marketName'
          label='Market Name'
          rules={[{ required: true, message: trans.common.form.require }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name='description'
          label='Description'
          rules={[{ required: true, message: trans.common.form.require }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name='groupMarketDto'
          label='Group Market'
          rules={[{ required: true, message: trans.common.form.require }]}
        >
          <Select options={listGroupMarket && listGroupMarket} onChange={value => setGroupMarketId(value)} />
        </Form.Item>

        <Form.Item className='items-start' valuePropName='checked' name='active' label={trans.common.status} rules={[{ required: true }]}>
          <Switch></Switch>
        </Form.Item>

        <Form.Item className='flex justify-center '>
          <Button
            type='primary'
            htmlType='submit'
            className='mr-2'
            loading={createMarket.isLoading || updateMarket.isLoading}
          >
            {trans.common.save}
          </Button>
          <Button
            htmlType='reset'
            onClick={() => {
              Modal.destroyAll();
            }}
          >
            {trans.common.close}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
