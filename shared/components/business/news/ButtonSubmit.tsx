import useTrans from '@/shared/hooks/useTrans';
import { useCreateAgent, useUpdateAgent } from '@/shared/schema/models/IAgent';
import { Button, Modal } from 'antd';
import { FormInstance } from 'antd/lib/form/Form';
import { SetStateAction } from 'react';

interface Props {
  form: FormInstance;
  editId: React.Key | undefined,
}
const ButtonSubmit = ({ form, editId }: Props) => {
  const { trans } = useTrans();
  const createAgent = useCreateAgent(() => Modal.destroyAll())
  const updateAgent = useUpdateAgent(() => Modal.destroyAll())
  return (
    <>
      <Button
        type='primary'
        htmlType='submit'
        className='mr-2'
        loading={createAgent.isLoading || updateAgent.isLoading}
      >
        {trans.common.save}
      </Button>
      <Button
        htmlType='reset'
        onClick={() => {
          // setChildViewMode('list');
          form.resetFields();
          Modal.destroyAll();
        }}
      >
        {trans.common.close}
      </Button>
    </>
  );
};

export default ButtonSubmit;
