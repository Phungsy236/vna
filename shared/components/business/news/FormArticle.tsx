import useTrans from '@/shared/hooks/useTrans';
import { useCreateArticle, useGetDetailArticle, useUpdateArticle } from '@/shared/schema/models/IArticle';
import { useGetAllListCategory } from '@/shared/schema/models/ICategory.mock';
import convertSlug from '@/shared/utils/functions/convertSlug';
import { Button, Form, FormInstance, Input, Modal, Select, Switch } from 'antd';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import InputUpload from '../../common/UploadInput';

const LexicalEditor = dynamic(() => import('@/shared/components/core/editor/LexicalEditor'), {
  ssr: false,
});

type Props = {
  form: FormInstance;
  editId?: React.Key;
};

export default function FormArticle({ form, editId }: Props) {
  const { trans } = useTrans();
  const router = useRouter()
  const [isSlug, setIsSlug] = useState<string>()
  const { data, remove } = useGetDetailArticle({ id: editId!, options: { enabled: !!editId } });
  const category = useGetAllListCategory();
  const createArticle = useCreateArticle(() => Modal.destroyAll());
  const updateArticle = useUpdateArticle(() => Modal.destroyAll());

  function onFinish(value: any) {
    if (!value) return;
    const bodyRequestCreateArticle = {
      ...value,
      preview: isSlug,
      category: category && category.data?.find(data => data.id === value.category),
    };
    const bodyRequestUpdateArticle = {
      ...value,
      id: editId,
      preview: convertSlug(form.getFieldValue('title') as string) || isSlug,
      category: category && category.data?.find(data => data.id === value.category),
    };
    if (editId) {
      updateArticle.mutate({ id: editId, article: bodyRequestUpdateArticle })
      router.back()
    } else {
      createArticle.mutate(bodyRequestCreateArticle);
      router.back()
    }
  }

  useEffect(() => {
    if (editId && data) {
      const dataForm = {
        ...data,
        preview: data.preview,
        category: data.category.id
      };
      form.setFieldsValue(dataForm);
    }
  }, [data, form]);

  useEffect(() => {
    return () => {
      form.resetFields()
      remove()
    }
  }, [])

  // useBeforeUnload(true, 'stop');
  return (
    <div className='w-full'>
      {/* <div className='mb-2 text-2xl font-bold'>{editId ? 'Chỉnh sửa bài viết' : trans.page.article.formName}</div> */}
      <Form
        preserve={false}
        form={form}
        onFinish={onFinish}
        layout='vertical'
        labelWrap
        size='large'
        colon={false}
        scrollToFirstError
        className='grid grid-cols-4 gap-x-4'
      >
        <Form.Item
          name='title'
          className='col-span-2'
          label={trans.page.article.articleTitle}
          rules={[{ required: true }]}
        >
          <Input onChange={() => {
            const slug = convertSlug(form.getFieldValue('title') as string)
            setIsSlug(slug)
          }} />
        </Form.Item>

        <Form.Item
          name='preview'
          className='col-span-2'
          label={"Slug"}
        >
          <Input disabled placeholder={isSlug} />
        </Form.Item>

        <Form.Item
          name='category'
          label={trans.page.article.catagory}
          className='col-span-2'
          rules={[{ required: true }]}
        >
          <Select
            optionFilterProp='label'
            options={category && category.data?.map(cate => ({ value: cate.id, label: cate.categoryName }))}
          ></Select>
        </Form.Item>

        <Form.Item
          name='image'
          label={trans.page.article.image}
          className='col-span-4 text-center'
          rules={[{ required: true }]}
        >
          <InputUpload initSrc={data?.image} className='w-full h-[200px] rounded-md' />
        </Form.Item>

        <Form.Item
          name='description'
          label={trans.page.article.description}
          className='col-span-4'
          rules={[{ required: true }]}
        >
          <Input.TextArea rows={6} />
        </Form.Item>

        <Form.Item
          name='content'
          className='col-span-4'
          label={trans.page.article.content}
          rules={[{ required: true }]}
        >
          <div className='rounded-xl border-solid border-gray-300 px-4'>
            <LexicalEditor content={data && (data.content as string)} form={form}></LexicalEditor>
          </div>
        </Form.Item>

        <Form.Item name='author' label={trans.page.article.author} className='col-span-2' rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item name="active" label={trans.common.status} className='col-span-2 flex justify-end' valuePropName='checked'
          rules={[{ required: false, message: trans.common.form.require }]} initialValue={true} >
          <Switch checkedChildren={trans.common.active} unCheckedChildren={trans.common.deActive} />
        </Form.Item>

        <Form.Item className='col-span-4 flex justify-center '>
          <Button
            type='primary'
            htmlType='submit'
            className='mr-2'
            loading={createArticle.isLoading || updateArticle.isLoading}
          >
            {trans.common.save}
          </Button>
          <Button
            htmlType='reset'
            onClick={() => {
              router.back()
            }}
          >
            {trans.common.close}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
