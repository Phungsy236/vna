import useTrans from '@/shared/hooks/useTrans';
import { useCreateCategory, useGetDetailCategory, useUpdateCategory } from '@/shared/schema/models/ICategory.mock';
import { Button, Form, FormInstance, Input, Modal, Select, Switch } from 'antd';
import React, { SetStateAction, useEffect } from 'react';

type Props = {
  form: FormInstance;
  editId?: React.Key;
};

export default function FormCategory({ form, editId }: Props) {
  const { trans } = useTrans();
  const { data } = useGetDetailCategory({ id: editId!, enabled: !!editId });
  const createCategory = useCreateCategory(() => Modal.destroyAll());
  const updateCategory = useUpdateCategory(() => Modal.destroyAll());
  function onFinish(value: any) {
    if (editId) updateCategory.mutate(value);
    createCategory.mutate(value);
  }
  useEffect(() => {
    if (editId && data) {
      form.setFieldsValue(data);
    }
  }, [data, form]);
  return (
    <div className='monitor:w-[600px] w-[200px] mobile:w-[300px] ipad:w-[400px] tablet:w-[500px] desktop:w-[550px]'>
      <div className='mb-2 text-2xl font-bold'>{editId ? 'Chỉnh sửa thư mục' : trans.page.category.formName}</div>
      <Form className='mx-auto w-full' preserve={false} form={form} onFinish={onFinish} layout='vertical' colon>
        <Form.Item
          name='categoryName'
          label={trans.page.category.categoryName}
          rules={[{ required: true, message: trans.common.form.require }]}
        >
          <Input />
        </Form.Item>
        <Form.Item className='items-start' name='status' label={trans.common.status} rules={[{ required: true }]}>
          <Switch checkedChildren={trans.common.active} unCheckedChildren={trans.common.deActive}></Switch>
        </Form.Item>

        <Form.Item className='flex justify-center '>
          <Button
            type='primary'
            htmlType='submit'
            className='mr-2'
            loading={createCategory.isLoading || updateCategory.isLoading}
          >
            {trans.common.save}
          </Button>
          <Button
            htmlType='reset'
            onClick={() => {
              Modal.destroyAll();
            }}
          >
            {trans.common.close}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
