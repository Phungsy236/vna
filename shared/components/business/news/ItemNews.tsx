import { IArticle } from '@/shared/schema/models/IArticle';
import { TIME_FORMAT } from '@/shared/utils/constants/appContants';
import dayjs from 'dayjs';
import { useRouter } from 'next/router';
import { PreImage } from '../../common/PreImage';

interface Props {
  data: IArticle;
}
export const ItemNews = ({ data }: Props) => {
  const router = useRouter();
  return (
    <>
      {data && (
        <div
          className="cursor-pointer"
          onClick={() => {
            const slug = typeof data.preview === 'string' ? data.preview.trim() : '';
            router.push(`/news/${slug}`);
          }}
        >
          <PreImage src={data.image} height={150} />
          <span>{dayjs(data.createdDate).format(TIME_FORMAT)}</span>
          <h1>{data.title}</h1>
          <p>{data.description}</p>
        </div>
      )}
    </>
  );
};
