import { IArticle } from '@/shared/schema/models/IArticle';
import { TIME_FORMAT } from '@/shared/utils/constants/appContants';
import { Col, Row } from 'antd';
import dayjs from 'dayjs';
import { useRouter } from 'next/router';
import { PreImage } from '../../common/PreImage';
interface Props {
  data: IArticle;
}
export const LatestNews = ({ data }: Props) => {
  const router = useRouter();
  return (
    <>
      {data && (
        <Row
          className='cursor-pointer'
          gutter={[16,16]}
          justify='start'
          align='top'
          onClick={() => {
            const slug = typeof data.preview === 'string' ? data.preview.trim() : '';
            router.push(`/news/${slug}`);
          }}
        >
          <Col xs={24} sm={24} lg={12}>
            <PreImage src={data.image} height={100} />
          </Col>
          <Col xs={24} sm={24} lg={12}>
            <Col span={24}>
              <h1 className='m-0 p-0'>{data.title}</h1>
            </Col>
            <Col span={24}>
              <span>{dayjs(data.updatedDate).format(TIME_FORMAT)}</span>
            </Col>
          </Col>
        </Row>
      )}
    </>
  );
};
