import { ICategory } from '@/shared/schema/models/ICategory.mock';
import { Space } from 'antd';
import CheckableTag from 'antd/lib/tag/CheckableTag';
import { useState } from 'react';

interface Props {
  data: ICategory[];
  setIsCategoryName: (value: string) => void;
}

export const TagCategory = ({ data, setIsCategoryName }: Props) => {
  const [tagChecked, setTagChecked] = useState<boolean>(true);
  const [isCheckedCateName, setIsCheckedCateName] = useState<string>("Internal News");
  const handleChange = (checked: boolean, category: ICategory) => {
    setTagChecked(checked);
    if (checked) {
      setIsCategoryName(category.categoryName);
      setIsCheckedCateName(category.categoryName)
    } else {
      setIsCategoryName('');
    }
  };

  return (
    <>
      <Space size={[0, 8]} wrap>
        {data.map(item => {
          return (
            <CheckableTag
              className='rounded-lg border-2 border-slate-300 px-3 py-1'
              key={item.id}
              checked={tagChecked && isCheckedCateName === item.categoryName}
              onChange={checked => handleChange(checked, item)}
            >
              {item.categoryName}
            </CheckableTag>
          );
        })}
      </Space>
    </>
  );
};
