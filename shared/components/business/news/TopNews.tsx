import { IArticle } from '@/shared/schema/models/IArticle';
import { Button } from 'antd';
import { useRouter } from 'next/router';
import { PreImage } from '../../common/PreImage';
import useTrans from '@/shared/hooks/useTrans';
interface Props {
  data: IArticle;
}
export const TopNews = ({ data }: Props) => {
  const router = useRouter();
  const {trans} = useTrans()
  const switchDetailNews = () => {
    const slug = typeof data.preview === 'string' ? data.preview.trim() : '';
    return router.push(`/news/${slug}`);
  };
  return (
    <>
      {data && (
        <div
          onClick={() => {
            switchDetailNews();
          }}
          className='mb-10 cursor-pointer'
        >
          <PreImage src={data.image} height={400} />
          <div className='flex flex-col items-center justify-center'>
            <h1>{data.title}</h1>
            <p>{data.description}</p>
            <Button
              className='cursor-pointer rounded-lg'
              onClick={() => {
                switchDetailNews();
              }}
            >
              {trans.page.article.findOut}
            </Button>
          </div>
        </div>
      )}
    </>
  );
};
