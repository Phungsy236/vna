import { useAppSelector } from '@/shared/hooks/useRedux'
import useTrans from '@/shared/hooks/useTrans'
import { ICreateRole, useCreateRole, useGetDetailRole, useUpdateRole } from '@/shared/schema/models/IRole'
import { getAllActionPermission, setDefaultCheckPermission } from '@/shared/stores/roleSlice'
import { TIME_FORMAT } from '@/shared/utils/constants/appContants'
import { URLS } from '@/shared/utils/constants/appMenu'
import { Button, Form, FormInstance, Input } from 'antd'
import dayjs from 'dayjs'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import TableRole from './TableRole/TableRole'

type Props = {
    form: FormInstance,
    editId?: React.Key,
    viewOnly?: boolean,
}

export default function FormRole({ form, editId, viewOnly = false }: Props) {
    const { trans } = useTrans()
    const router = useRouter()
    const { data } = useGetDetailRole({ id: editId!, options: { enabled: !!editId } })
    const dispatch = useDispatch()
    const createRole = useCreateRole(() => router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE))
    const updateRole = useUpdateRole(() => router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE))
    const actionPer = useAppSelector(state => state.roleSlice.catePermissions)
    function onFinish(value: any) {
        const submitObj: ICreateRole = {
            roleDto: {
                id: editId,
                roleName: value.roleName,
                description: value.description
            },
            permissionActionDtoList: getAllActionPermission(actionPer)
        }
        if (editId) updateRole.mutate({ Role: submitObj, id: editId })
        else { createRole.mutate(submitObj) }
    }
    useEffect(() => {
        if (editId) {
            const formValue = {
                ...data,
                ...data?.roleDto,
                updateDate: dayjs(data?.updatedDate).format(TIME_FORMAT),
                createDate: dayjs(data?.createdDate).format(TIME_FORMAT)
            }
            form.setFieldsValue(formValue)
            if (data?.permissionActionDtoList) {
                dispatch(setDefaultCheckPermission(data?.permissionActionDtoList.filter(item => item.active)))
            }
        }
        return () => form.resetFields()
    }, [data, form, editId])

    return (
        <section className='w-full'>
            <Form
                // preserve={false}
                form={form}
                onFinish={onFinish}
                layout='vertical'
                colon
                disabled={viewOnly}
                size='large'
            >
                <Form.Item name="roleName" label={trans.page.role.roleName} rules={[
                    { required: true, message: trans.common.form.require },
                    { max: 24, message: "Max field is 24 characters" }

                ]} >
                    <Input placeholder='Role Name' size='large' />
                </Form.Item>
                <Form.Item name="description" label={trans.page.role.description} rules={[{ required: true, message: trans.common.form.require }, {
                    max: 256, message: "Max field is 256 characters"
                }]} >
                    <Input.TextArea placeholder='Role Description' autoSize />
                </Form.Item>
                <Form.Item label={trans.page.role.permission}>
                    <TableRole />
                </Form.Item>
                {viewOnly && <div className='grid grid-cols-3 gap-4'>
                    <Form.Item name="createDate" label={trans.common.createAt}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="updateDate" label={trans.common.updateAt}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="updateBy" label={trans.common.updateBy}>
                        <Input />
                    </Form.Item>
                </div>}
                {!viewOnly &&
                    <Form.Item className='flex justify-end ' >

                        <Button htmlType="reset" onClick={() => { router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE) }} className='!rounded-none'>
                            {trans.common.close}
                        </Button>
                        <Button type="primary" htmlType="submit" className='ml-4 !rounded-none' loading={createRole.isLoading || updateRole.isLoading}>
                            {trans.common.save}
                        </Button>
                    </Form.Item>}
            </Form>
            {
                viewOnly &&
                <div className='flex justify-end gap-4'>
                    <Button onClick={() => { router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE) }}>
                        {trans.common.close}
                    </Button>
                    <Button type='primary' onClick={() => { router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE + '/update/' + editId) }}>
                        {trans.common.edit}
                    </Button>
                </div>

            }
        </section >
    )
}