import { PermissionList, PermissionListDTO, useGetAllPermissions } from '@/shared/schema/models/IRole'
import { Button, Table } from 'antd'
import { ColumnsType } from 'antd/lib/table/InternalTable'
import React from 'react'
import fakeData from '@/shared/mocks/db.json'
import useTrans from '@/shared/hooks/useTrans';
import { useRouter } from 'next/router'
import { URLS } from '@/shared/utils/constants/appMenu'



export default function PermisisonTable() {
    const { trans } = useTrans()
    const router = useRouter()
    const { data, isLoading } = useGetAllPermissions()
    const column: ColumnsType<PermissionListDTO> = [
        {
            title: trans.page.role.function,
            dataIndex: 'permissionName',
            key: 'permissionName'
        },
        {
            title: trans.page.role.description,
            dataIndex: 'description',
            key: "description",
        },

    ]
    return (
        <>
            <Table columns={...column} dataSource={data} loading={isLoading} pagination={false} />
            <div className='mt-4 text-end'>
                <Button onClick={() => router.push(URLS.SYSTEM_MANAGE.ROLE_MANAGE)}>{trans.common.back}</Button>
            </div>
        </>

    )
}