import { useAppSelector } from '@/shared/hooks/useRedux'
import { Checkbox, Divider } from 'antd'
import { useDispatch } from 'react-redux'
import Row from './Row'
import { useGetAllActionPermissions } from '@/shared/schema/models/IRole'
import { useEffect } from 'react'
import { setCatePermissions } from '@/shared/stores/roleSlice'
import useTrans from '@/shared/hooks/useTrans'

type Props = { hideButtonSubmit?: boolean, disable?: boolean, isRootRole?: boolean }

export default function TableRole({ }: Props) {
    // console.log(_.uniq(dummyData.map(item => item.tableName)))
    // const { data } = useGetAllActionPermissions()
    // const dispatch = useDispatch()
    // useEffect(() => {
    //     if (data) dispatch(setCatePermissions(data))
    // }, [data, dispatch])
    const catePermissions = useAppSelector(state => state.roleSlice.catePermissions)
    const {trans} = useTrans()
    return (
        <div>
            <div>
                <div className='w-full  rounded-t-lg'>
                    <div className='bg-gray-100 grid grid-cols-6 place-items-center  py-4  '>
                        <span className=''></span>
                        {/* CHECK ALL ACTION */}
                        <span className='font-bold text-center'>
                            <div>{trans.page.role.create}</div>
                        </span>
                        <span className='font-bold text-center'>
                            <div>{trans.page.role.edit}</div>
                        </span>
                        <span className='font-bold text-center'>
                            <div>{trans.page.role.view}</div>
                        </span>
                        <span className='font-bold text-center'>
                            <div>{trans.page.role.delete}</div>
                        </span>
                        <span className='font-bold text-center'>
                            <div>{trans.page.role.approve}</div>
                        </span>
                    </div>
                    {
                        catePermissions.map(category => <Row key={category.id} permisions={category} />)
                    }
                </div>

            </div>
        </div>
    )
}