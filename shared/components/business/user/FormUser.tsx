import useTrans from '@/shared/hooks/useTrans';
import { useGetBasicActiveListAgent } from '@/shared/schema/models/IAgent';
import { useCreateUser, useGetDetailUser, useUpdateUser } from '@/shared/schema/models/IAppUser';
import { useGetBasicActiveListCA } from '@/shared/schema/models/ICA';
import { useGetBasicActiveListRole } from '@/shared/schema/models/IRole';
import { URLS } from '@/shared/utils/constants/appMenu';
import { APP_REGEX } from '@/shared/utils/constants/appContants';
import { Button, Form, FormInstance, Input, Modal, Radio, Select, Switch } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useAppSelector } from '@/shared/hooks/useRedux';
import PhoneCodeInput from '../../common/PhoneCodeInput';

type Props = {
  form: FormInstance;
  editId?: React.Key;
  isOnlyView?: boolean;
};

const { Option } = Select;

export default function FormUser({ form, editId, isOnlyView = false }: Props) {
  const { trans } = useTrans();
  const { data } = useGetDetailUser({ id: editId!, options: { enabled: !!editId } });

  const radioValue = useWatch('organizationId', form);
  const { data: optionsAgent } = useGetBasicActiveListAgent();
  const { data: optionsRole } = useGetBasicActiveListRole();
  const { data: optionsCA } = useGetBasicActiveListCA();


  useEffect(() => {
    if (editId && data) {

      const dataForm = {
        organizationId: parseInt(data?.organizationDto?.organization?.id),
        agentOrCAId: data?.agent?.agentId || data?.organizationDto?.organization?.id,
        roleId: data?.roles?.map(role => role.id),
        // updatedBy: user?.user.fullName,
        ...data,
      };
      form.setFieldsValue(dataForm);
    }
    return () => form.resetFields();
  }, [data, form, editId]);

  return (
    <>
      <Form.Item name='fullName' label={trans.page.users.fullName} rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item
        name='phoneNumber'
        label={trans.page.agent.phoneNumber}
        rules={[
          { required: true, message: trans.common.form.require },
          {
            pattern: APP_REGEX.VNPhoneRegex,
            message: trans.common.form.invalidPhone,
          },
        ]}
      >
        <Input
          addonBefore={
            <PhoneCodeInput />
          }
          placeholder='phoneNumber'
        />
      </Form.Item>

      <Form.Item
        name='email'
        label={trans.page.agent.email}
        rules={[{ required: true, message: trans.common.form.require }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name='userName'
        label={trans.page.users.userName}
        rules={[{ required: true, message: trans.common.form.require }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='organizationId'
        label={trans.page.users.organizationID}
        className='col-span-2 mobile:col-span-1 ipad:col-span-2'
        rules={[{ required: true, message: trans.common.form.require }]}
      >
        <Radio.Group defaultValue={radioValue || 3} className='col-span-2 mobile:col-span-1 ipad:col-span-2'>
          <Radio value={1}>{trans.page.users.agent}</Radio>
          <Radio value={2}>{trans.page.users.CA}</Radio>
          <Radio value={3}>{trans.page.users.none}</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name='password' label={trans.page.users.password} rules={[{ required: true }]}>
        <Input.Password />
      </Form.Item>
      {radioValue === 1 && (
        <Form.Item
          name='agentOrCAId'
          className='col-span-2 mobile:col-span-1 ipad:col-span-2'
          label={trans.page.users.agent}
        >
          <Select
            showSearch
            optionFilterProp='label'
            options={optionsAgent && optionsAgent?.data.map(agent => ({ value: agent.id, label: agent.agentParentName }))}
          ></Select>
        </Form.Item>
      )}
      {radioValue === 2 && (
        <Form.Item
          name='agentOrCAId'
          className='col-span-2 mobile:col-span-1 ipad:col-span-2'
          label='Corporate Account'
        >
          <Select
            showSearch
            optionFilterProp='label'
            options={optionsCA && optionsCA?.data.map(ca => ({ value: ca.id, label: ca.englishName }))}
          ></Select>
        </Form.Item>
      )}
      {editId &&
        <Form.Item name='updatedBy' label={trans.page.users.password} rules={[{ required: true }]}>
          <Input disabled />
        </Form.Item>
      }
      <Form.Item name='status' className='text-left' label={trans.common.status}>
        <Switch checkedChildren={trans.common.active} unCheckedChildren={trans.common.deActive} />
      </Form.Item>

      <Form.Item name='roleId' className='col-span-2 mobile:col-span-1 ipad:col-span-2' label={trans.page.users.roles}>
        {/* <Select mode='multiple' optionFilterProp='label'></Select> */}
        <Select
          mode='multiple'
          optionFilterProp='label'
          options={optionsRole?.data.map(agent => ({ value: agent.id, label: agent.roleName }))}
        ></Select>
      </Form.Item>
    </>
  );
}
