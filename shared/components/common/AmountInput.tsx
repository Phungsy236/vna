import { Button, FormInstance, InputNumber, Space } from 'antd'
import { MinusIcon, PlusIcon } from 'lucide-react'
import React, { useState } from 'react'

type Props = {
    form: FormInstance,
    inputName: string,
    initAmount: number,
    maxAmount?: number,
    minAmount?: number
}

export default function AmountInput({ form, inputName, initAmount, maxAmount, minAmount }: Props) {
    const [amount, setAmount] = useState(initAmount)
    return (
        <Space.Compact block className='w-max flex items-center' size='small' >
            <Button onClick={() => {
                if (minAmount !== undefined) {
                    if (amount > minAmount) {
                        setAmount(value => value - 1)
                        form.setFieldValue(inputName, amount - 1)
                    }
                } else {
                    setAmount(value => value - 1)
                    form.setFieldValue(inputName, amount - 1)
                }

            }}>
                <MinusIcon size={12} />
            </Button>
            <InputNumber controls={false} value={amount} className='w-14 h-6' />
            <Button type='primary' onClick={() => {
                if (maxAmount !== undefined) {
                    if (amount < maxAmount) {
                        setAmount(value => value + 1)
                        form.setFieldValue(inputName, amount + 1)
                    }
                } else {
                    setAmount(value => value + 1)
                    form.setFieldValue(inputName, amount + 1)
                }

            }}>
                <PlusIcon size={12} />
            </Button>
        </Space.Compact>
    )
}