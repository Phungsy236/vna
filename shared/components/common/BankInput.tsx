import useTrans from '@/shared/hooks/useTrans'
import { useGetCountries, useGetListBank } from '@/shared/schema/models/IMetaData'
import { Form, Select } from 'antd'
import React from 'react'

type Props = {
    inputName?: string,
    label?: string
    key?: any
}

export default function BankInput({ inputName, label, key }: Props) {
    const { trans } = useTrans()
    const { data: banks, isLoading: loadingBank } = useGetListBank()

    return (
        <Form.Item name={inputName || "bankName"} label={label || trans.page.agent.agentInformation.bankName} key={key}
            rules={[{ required: true, message: trans.common.form.require }]} >
            <Select placeholder='bankName' loading={loadingBank} options={banks} showSearch />
        </Form.Item>
    )
}