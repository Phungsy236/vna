import useTrans from '@/shared/hooks/useTrans'
import { useGetCountries } from '@/shared/schema/models/IMetaData'
import { Form, Select } from 'antd'
import React from 'react'

type Props = {
    inputName?: string,
    label?: string
    key?: any
}

export default function CountryInput({ inputName, label, key }: Props) {
    const { trans } = useTrans()
    const { data: countries, isLoading: loadingCountries } = useGetCountries()

    return (
        <Form.Item name={inputName || "country"} label={label || trans.page.agent.country} key={key}
            rules={[{ required: true, message: trans.common.form.require }]} >
            <Select placeholder='country' loading={loadingCountries} options={countries?.data.map(ct => ({ value: ct.name, label: ct.name }))} showSearch />
        </Form.Item>
    )
}