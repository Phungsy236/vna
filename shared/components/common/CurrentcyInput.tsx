import { InputNumber, InputNumberProps } from 'antd'
import React from 'react'

type Props = {

}

export default function CurrentcyInput(props: Props & InputNumberProps) {
  const formatCurrency = (value: any) => {
    return `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const parseCurrency = (value: any) => {
    return value.replace(/\$\s?|(,*)/g, '');
  };
  return (
    <InputNumber
      className='w-full'
      prefix={props.prefix}
      defaultValue={props.defaultValue}
      formatter={formatCurrency}
      parser={parseCurrency}
      {...props}
    />
  )
}