import useTrans from '@/shared/hooks/useTrans'
import { useGetLanguages } from '@/shared/schema/models/IMetaData'
import { Form, Select } from 'antd'
import React from 'react'

type Props = {}

export default function LanguageInput({ }: Props) {
    const {trans} = useTrans()
    const {data , isLoading} = useGetLanguages()
    return (
        <Form.Item name="language" label={trans.page.agent.agentInformation.language}
            rules={[{ required: true, message: trans.common.form.require }]} >
            <Select showSearch placeholder='language' loading={isLoading} options={data}/>
        </Form.Item>
    )
}