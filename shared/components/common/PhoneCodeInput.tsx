import { useGetCountries } from '@/shared/schema/models/IMetaData'
import { Form, Select } from 'antd'
import React from 'react'
import _ from 'lodash'

type Props = {
    inputName?: string,
    label?: string
}

export default function PhoneCodeInput({ inputName, label }: Props) {
    const { data: countries, isLoading: loadingCountries } = useGetCountries()
    const mergedArray = _.flatten(countries?.data);
    const uniqueObjects = _.uniqBy(mergedArray, 'code');
    return (
        <Form.Item name={inputName || "phoneCode"} noStyle initialValue={'+84'}>
            <Select style={{ width: 100 }} loading={loadingCountries} options={uniqueObjects.map(ct => ({ value: ct.code, label: ct.code }))} showSearch />
        </Form.Item>
    )
}