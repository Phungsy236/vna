import useTrans from '@/shared/hooks/useTrans'
import { useGetTimeZone } from '@/shared/schema/models/IMetaData'
import { Form, Select } from 'antd'
import React from 'react'

type Props = {
    inputName? : string ,
    label? : string 
}

export default function TimezoneInput({inputName , label }: Props) {
    const { trans } = useTrans()
    const { data: timezone, isLoading: loadingTimeZone } = useGetTimeZone()

    return (
        <Form.Item name={inputName||"timeZone"} label={label|| trans.page.agent.timeZone}
            rules={[{ required: true, message: trans.common.form.require }]} >
            <Select placeholder='timeZone' loading={loadingTimeZone} options={timezone?.data.map(tz => ({ value: tz.timeZone, label: tz.timeZone }))} showSearch />
        </Form.Item>
    )
}