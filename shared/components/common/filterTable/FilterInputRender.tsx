import { Button, DatePicker, Form, Input, Select, Space } from 'antd'
import React, { SetStateAction } from 'react'
import { InputType } from './FilterTable'

type Props = {
  inputs: InputType[],
  setInputs: React.Dispatch<SetStateAction<InputType[]>>
  searchFunction: (value: any) => void
}

export default function FilterInputRender({ inputs, searchFunction, setInputs }: Props) {

  function RenderInput({ input }: { input: InputType }) {
    switch (input.inputType) {
      case 'text':
      case 'number':
        return <Form.Item
          initialValue={input.value}
          name={input.field} className='w-full'>
          <Input placeholder={`Search ${input.label}`} allowClear className='w-full'

          />
        </Form.Item>
        break;
      case 'select':
        return <Form.Item name={input.field} className='w-full' initialValue={input.value}>
          <Select placeholder={`Search ${input.label}`} allowClear className='w-full' options={input.options}

          />
        </Form.Item>
      case 'date':
        return <Form.Item name={input.field} className='w-full' initialValue={input.value}>
          <DatePicker placeholder={`Search ${input.label}`} allowClear className='w-full' />
        </Form.Item>
      case 'date-range':
        return <Form.Item name={input.field} className='w-full' initialValue={input.value}>
          <DatePicker.RangePicker className='w-full' />
        </Form.Item>
      default:
        break;
    }
  }
  function convertFormValuetoInputs(value: Record<string, any>) {
    const result = inputs.map(itemA => ({
      ...itemA,
      value: value[itemA.field]
    }));
    return result;
  }
  return (
    <Form size='large' onFinish={value => {
      searchFunction(convertFormValuetoInputs(value))
    }
    }>
      <div className='grid gap-4 grid-cols-2 lg:grid-cols-4  border border-solid border-gray-300 p-4 rounded-lg'>
        {
          inputs.filter(item => item.active).map(item => <RenderInput input={item} key={item.id} />)
        }
        <div className='ml-auto col-start-2 lg:col-start-4 '>
          <Space>
            <Button htmlType='reset' className='!rounded-none'>Clear</Button>
            <Button type='primary' htmlType='submit' className='!rounded-none'>Search</Button>
          </Space>
        </div>
      </div>
    </Form>
  )
}