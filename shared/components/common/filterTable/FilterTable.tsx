import { Filter } from '@/shared/schema/typedef/ISearchParams'
import { Button, Checkbox, Drawer, Space } from 'antd'
import { FilterIcon } from 'lucide-react'
import { Dispatch, SetStateAction, useState } from 'react'

export type InputType = {
    id: React.Key,
    field: string,
    label: string,
    active: boolean,
    fieldType: Filter['fieldType'],
    op: Filter['op'],
    value?: any
} & ({
    inputType: 'select',
    options: { key: React.Key, value: string }[]
} | {
    inputType: 'text' | 'number' | "date" | "date-range"
} | {
    inputType: "date-range" | "date",
    disable?: any
})

type Props = {
    setInputs: Dispatch<SetStateAction<InputType[]>>,
    inputs: InputType[]
}
/**
 * 
 * @param searchFunction : function will trigged when 
 * @returns 
 */
export default function FilterTable({ setInputs, inputs }: Props) {

    const [isOpen, setOpen] = useState(false)
    function selectAll() {
        setInputs(inputs => inputs.map(item => {
            item.active = true
            return item
        }))
    }
    function clearAll() {
        setInputs(input => input.map(item => {
            item.active = false
            return item
        }))
    }

    return <>
        <Button onClick={() => setOpen(true)}>
            <FilterIcon className='text-gray-600' />
        </Button>


        <Drawer
            title="Danh sách bộ lọc"
            placement={"right"}
            width={500}
            onClose={() => setOpen(false)}
            open={isOpen}
            extra={
                <Space>
                    <Button onClick={clearAll}>Xóa bộ lọc</Button>
                    <Button type="primary" onClick={selectAll} >
                        Chọn tất cả
                    </Button>
                </Space>
            }
        >
            <div className='flex flex-col gap-2'>
                {inputs.map(item =>
                    <Checkbox className='relative' key={item.id} checked={item.active} onClick={(_e) => {
                        setInputs(inputs.map(tmp => {
                            if (tmp.id === item.id) {
                                tmp.active = !tmp.active
                            }
                            return tmp
                        }))
                    }}> {item.label}</Checkbox>)}
            </div>


        </Drawer>
    </>


}