import { CLEAR_EDITOR_COMMAND } from 'lexical'
import {useLexicalComposerContext} from '@lexical/react/LexicalComposerContext'
import { FormInstance } from 'antd/lib/form/Form'
interface Props{
  form: FormInstance
}
export function Actions({form}: Props) {
  const [editor] = useLexicalComposerContext()
  function handleOnSave() {
    console.log(editor.getRootElement())
    form.setFieldValue("content", editor.getRootElement()?.innerHTML)
    // console.log(JSON.stringify(editor.getEditorState()))
  }

  function handleOnClear() {
    editor.dispatchCommand(CLEAR_EDITOR_COMMAND, undefined)
    editor.focus()
  }

  return (
    <div className="editor-actions">
      <button onClick={handleOnSave}>Save</button>
      <button onClick={handleOnClear}>Clear</button>
    </div>)
}
