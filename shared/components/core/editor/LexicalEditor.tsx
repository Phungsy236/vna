import { useRef, useState } from 'react';
import { useEffect } from 'react';
import useMediaQuery from './hooks/useMediaQuery';

// @ts-ignore
import { $createParagraphNode, $insertNodes, EditorState, LexicalEditor } from 'lexical';

import { LexicalComposer } from '@lexical/react/LexicalComposer';
import { ListPlugin } from '@lexical/react/LexicalListPlugin';
import { HorizontalRulePlugin } from '@lexical/react/LexicalHorizontalRulePlugin';
import { ClearEditorPlugin } from '@lexical/react/LexicalClearEditorPlugin';
import { RichTextPlugin } from '@lexical/react/LexicalRichTextPlugin';
import { HistoryPlugin } from '@lexical/react/LexicalHistoryPlugin';
import { OnChangePlugin } from '@lexical/react/LexicalOnChangePlugin';
import { TablePlugin } from '@lexical/react/LexicalTablePlugin';
import { CheckListPlugin } from '@lexical/react/LexicalCheckListPlugin';
import { MarkdownShortcutPlugin } from '@lexical/react/LexicalMarkdownShortcutPlugin';
import { TRANSFORMERS } from '@lexical/markdown';
import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import LexicalErrorBoundary from '@lexical/react/LexicalErrorBoundary';
import { $generateNodesFromDOM } from '@lexical/html';
import { $getRoot } from 'lexical';

import Nodes from './nodes';
import EditorTheme from './themes/EditorTheme';

import DragDropPaste from './plugins/DragDropPastePlugin';
import FloatingLinkEditorPlugin from './plugins/FloatingLinkEditorPlugin';
import LinkPlugin from './plugins/LinkPlugin';
import ToolbarPlugin from './plugins/ToolbarPlugin';
import TreeViewPlugin from './plugins/TreeViewPlugin';
import ContentEditable from './ui/ContentEditable';
import Placeholder from './ui/Placeholder';
import LexicalAutoLinkPlugin from './plugins/AutoLinkPlugin/index';
import CodeHighlightPlugin from './plugins/CodeHighlightPlugin';
import InlineImagePlugin from './plugins/InlineImagePlugin';

import { FormInstance } from 'antd/lib/form/Form';
// import { Actions } from './Action';
// import CodeActionMenuPlugin from './plugins/CodeActionMenuPlugin'

const loadContent = () => {
  // 'empty' editor
  const value =
    '{"root":{"children":[{"children":[],"direction":null,"format":"","indent":0,"type":"paragraph","version":1}],"direction":null,"format":"","indent":0,"type":"root","version":1}}';

  return value;
};

// Lexical React plugins are React components, which makes them
// highly composable. Furthermore, you can lazy load plugins if
// desired, so you don't pay the cost for plugins until you
// actually use them.
interface PropsChild {
  content: string
}
function MyCustomAutoFocusPlugin({ content }: PropsChild) {
  const [editor] = useLexicalComposerContext();

  useEffect(() => {
    // Focus the editor when the effect fires!
    editor.focus();
    editor.update(() => {
      let nodes = [];
      const html = content
      // Parse html
      if (html) {
        const parser = new DOMParser();
        const dom = parser.parseFromString(html, 'text/html');
        const root = $getRoot();
        root.clear();
        const nodes = $generateNodesFromDOM(editor, dom)
          .map((node) => {
            if (node.getType() === 'text') {
              if (node.getTextContent().trim() === '') {
                return null;
              } else {
                return $createParagraphNode().append(node);
              }
            }

            if (node.getType() === 'linebreak') {
              return null
            }

            return node;
          })
          .filter((node) => !!node);

        // Select the root
        $getRoot().select();

        // Insert them at a selection.
        // @ts-ignore
        $insertNodes(nodes);
        // root.append(...nodes);
      }
    });
  }, [editor, content]);

  return null;
}

// Catch any errors that occur during Lexical updates and log them
// or throw them as needed. If you don't throw them, Lexical will
// try to recover gracefully without losing user data.
function onError(error: any) {
  console.error(error);
}
interface Props {
  form: FormInstance;
  content: string | undefined
}
export default function LexicalEditor({ form, content }: Props) {
  const isSmallWidthViewPort = useMediaQuery('(max-width: 1025px)');
  const [floatingAnchorElem, setFloatingAnchorElem] = useState<HTMLDivElement | null>(null);
  const placeholder = <Placeholder>Enter some rich text...</Placeholder>;
  const initialEditorState = loadContent();
  const editorStateRef = useRef<EditorState>();
  const initialConfig = {
    namespace: 'VNA_Editor',
    editorState: initialEditorState,
    theme: EditorTheme,
    onError,
    nodes: [...Nodes],
    showTreeView: true,
  };

  function handleOnChange(editorState: EditorState, editor: LexicalEditor) {
    editorStateRef.current = editorState;
    form.setFieldValue('content', editor.getRootElement()?.innerHTML);
  }
  const onRef = (_floatingAnchorElem: HTMLDivElement) => {
    if (_floatingAnchorElem !== null) {
      setFloatingAnchorElem(_floatingAnchorElem);
    }
  };

  return (
    <LexicalComposer initialConfig={initialConfig}>
      <div className='editor-shell'>
        <ToolbarPlugin />
        <div className='editor-container tree-view'>
          <ClearEditorPlugin />
          <LexicalAutoLinkPlugin />
          <InlineImagePlugin />
          <CheckListPlugin />
          <RichTextPlugin
            contentEditable={
              <div className='editor-scroller'>
                <div className='editor' ref={onRef}>
                  <ContentEditable />
                </div>
              </div>
            }
            placeholder={placeholder}
            ErrorBoundary={LexicalErrorBoundary}
          />
          <OnChangePlugin onChange={handleOnChange} />
          <HistoryPlugin />
          <MyCustomAutoFocusPlugin content={content as string} />
          <DragDropPaste />
          <ListPlugin />
          <CodeHighlightPlugin />
          <TablePlugin hasCellMerge={true} hasCellBackgroundColor={true} />
          <HorizontalRulePlugin />
          <LinkPlugin />
          {floatingAnchorElem && !isSmallWidthViewPort && <FloatingLinkEditorPlugin anchorElem={floatingAnchorElem} />}
          <MarkdownShortcutPlugin transformers={TRANSFORMERS} />
          {/* <Actions form={form}/> */}
          {/* <TreeViewPlugin /> */}
        </div>
      </div>
    </LexicalComposer>
  );
}
