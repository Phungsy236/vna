import useBreakPointTw from '@/shared/hooks/useBreakPointTw';
import { useAppSelector } from '@/shared/hooks/useRedux';
import { toggleMenu } from '@/shared/stores/appSlice';
import { menuListMock } from '@/shared/utils/constants/appMenu';
import { SCREENTYPE } from '@/shared/utils/constants/appContants';
import { Affix, Drawer, Layout, theme as antTheme } from 'antd';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import HeaderComponent from './header';
import MenuComponent, { MenuItem } from './menu';
import { useAbility } from '@casl/react';
import { AbilityContext } from '@/pages/_app';



const { Sider, Content } = Layout;
function findFilteredMenu(ability: any) {
  function checkItem(permission: string | undefined) {
    if (permission) {
      const check = ability.can('view', permission)
      return check
    } else return true
  }
  const result: MenuItem[] = [];
  menuListMock.forEach(item => {
    if (item.children) {
      const tmp = { ...item }
      tmp.children = item.children.filter(children => {
        return checkItem(children.permission)
      })
      result.push(tmp)
    } else {
      if (item.permission) {
        if (checkItem(item.permission)) result.push(item)
        return
      } else result.push(item)

    }
  })
  return result
}
const DashBoardLayout = ({
  children,
  breadcrumb,
}: {
  children: React.ReactNode;
  breadcrumb?: React.ReactNode;
}) => {
  const router = useRouter();
  const [openKey, setOpenkey] = useState<string>();
  const [selectedKey, setSelectedKey] = useState<string>(router.pathname);
  const token = antTheme.useToken();
  const collapsed = useAppSelector(state => state.appSlice.isCollapseMenu);

  const dispatch = useDispatch();
  const { screen } = useBreakPointTw();
  useEffect(() => {
    if (!collapsed) {
      setOpenkey(router.pathname.split('/')[1]);
    }
    if (router.locale !== 'vi') {
      setSelectedKey(router.pathname.replace(`/${router.locale}`, ''));
    } else {
      // only get 2 level
      const first = router.pathname.split('/')[1];
      const second = router.pathname.split('/')[2];
      setSelectedKey('/' + first + '/' + second);
    }
  }, [router.pathname, router.locale, collapsed]);
  const [isSmallScreen, setSmallScreen] = useState(false);
  useEffect(() => {
    if ([SCREENTYPE.MOBILE, SCREENTYPE.IPAD].includes(screen)) setSmallScreen(true);
    else setSmallScreen(false);
  }, [screen]);
  /* Map menu base on permissions */
  const ability = useAbility(AbilityContext)
  const [filteredMenu, setFilteredMenu] = useState<MenuItem[]>([])
  useEffect(() => {
    setFilteredMenu(findFilteredMenu(ability))
  }, [ability])


  return (
    <>
      <Layout>
        {isSmallScreen ? (
          <Drawer
            width='230'
            placement='left'
            bodyStyle={{ padding: 0, height: '100%', overflowX: 'hidden' }}
            closable={false}
            onClose={() => {
              dispatch(toggleMenu(!collapsed));
            }}
            open={!collapsed}
          >
            <div className='px-4 pt-4 text-xl font-bold'>Menu</div>
            <MenuComponent
              menuList={filteredMenu}
              openKey={openKey}
              onChangeOpenKey={k => setOpenkey(k)}
              selectedKey={selectedKey}
              onChangeSelectedKey={k => setSelectedKey(k)}
            />
          </Drawer>
        ) : (
          <Affix offsetTop={0}>
            <Sider
              width={220}
              className='layout-page-sider min-h-screen '
              trigger={null}
              collapsible
              style={{ backgroundColor: token.token.colorBgContainer }}
              collapsedWidth={isSmallScreen ? 0 : 80}
              collapsed={collapsed}
              breakpoint='md'
            >
              <div className='h-16 overflow-hidden' onClick={() => router.push('/')}>
                <img
                  src='https://careerfinder.vn/wp-content/uploads/2020/05/vietnam-airline-logo.jpg'
                  alt='logo'
                  className='relative h-14 w-full object-contain'
                />
              </div>
              <MenuComponent
                menuList={filteredMenu}
                openKey={openKey}
                onChangeOpenKey={k => setOpenkey(k)}
                selectedKey={selectedKey}
                onChangeSelectedKey={k => setSelectedKey(k)}
              />
            </Sider>
          </Affix>
        )}

        <Layout>
          <HeaderComponent collapsed={collapsed} />
          {/* {breadcrumb} */}
          <Content className='m-6 min-h-screen rounded-lg bg-white mobile:m-4'>
            {/* <TagsView /> */}
            <div className='p-2'>{children}</div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default DashBoardLayout;
