import type { FC } from 'react';

import { useAppDispatch, useAppSelector } from '@/shared/hooks/useRedux';
import useTrans from '@/shared/hooks/useTrans';
import { logout, toggleMenu } from '@/shared/stores/appSlice';
import { formatNumber } from '@/shared/utils/functions/formatCurrentcy';
import { LockOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Badge, Dropdown, Layout, Modal, Space, Typography, theme as antTheme } from 'antd';
import { AlignJustifyIcon, BellRingIcon } from 'lucide-react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import UserIcon from '../../icons/UserIcon';

const { Header } = Layout;

interface HeaderProps {
  collapsed: boolean;
}

const HeaderComponent: FC<HeaderProps> = ({ collapsed }) => {
  const router = useRouter();
  const token = antTheme.useToken();
  const dispatch = useAppDispatch();
  const { user } = useAppSelector((state => state.appSlice))
  const { changeLanguage, lang, currentcy, trans } = useTrans();

  const onActionClick = () => {
    Modal.confirm({
      title: 'Sure logout',
      onOk: () => {
        dispatch(logout());
        router.push('/login');
      },
    });
  };
  function NumberBadge({ label, amount }: { label: string; amount: string }) {
    return (
      <div className='mobile:hidden'>
        <Space direction='horizontal' className='rounded-[10px] bg-slate-100 py-2 pl-3 pr-1 leading-none ' wrap>
          <Typography.Text className='font-medium'>{label}</Typography.Text>
          <Typography.Text strong className='h-full rounded-lg bg-white  p-2 text-base font-bold'>
            {amount}
          </Typography.Text>
        </Space>
      </div>

    );
  }
  function RenderFlag() {
    switch (lang) {
      case 'vi':
        return <Image src={'/vietnam.png'} alt='vn' width={24} height={24} />;
      case 'en':
        return <Image src={'/united-kingdom.png'} alt='vn' width={24} height={24} />;
      default:
        return <Image src={'/vietnam.png'} alt='vn' width={24} height={24} />;
    }
  }
  return (
    <Header className='layout-page-header bg-2' style={{ backgroundColor: token.token.colorBgContainer }}>
      <div className='lg:hidden' onClick={() => dispatch(toggleMenu(!collapsed))}>
        <span
          id='sidebar-trigger'
          className='flex cursor-pointer items-center justify-center rounded-lg p-2 hover:bg-slate-200'
        >
          <AlignJustifyIcon />
        </span>
      </div>
      <div className='flex justify-end w-full pr-4'>
        <div className='flex items-center gap-6'>
          {/* <NumberBadge label={trans.menu.header.Points} amount={formatNumber(10000, lang)} />
          <NumberBadge
            label={trans.menu.header.AvailableBalance}
            amount={formatNumber(10000000, lang) + ' ' + currentcy}
          />
          <NumberBadge
            label={trans.menu.header.AccountBalance}
            amount={formatNumber(1234556666, lang) + ' ' + currentcy}
          /> */}

          <Dropdown
            placement='bottomRight'
            menu={{
              items: [
                {
                  key: 'vn',
                  label: <div>Vietnamese</div>,
                  onClick: () => {
                    if (lang !== 'vi') changeLanguage('vi');
                  },
                },
                {
                  key: 'en',
                  label: <div>English </div>,
                  onClick: () => {
                    if (lang !== 'en') changeLanguage('en');
                  },
                },
              ],
            }}
          >
            <div className='flex flex-col items-center'>
              <RenderFlag />
            </div>
          </Dropdown>
          {/*  TODO : replace with api later */}
          <Dropdown
            placement='bottomRight'
            menu={{
              items: [
                {
                  key: '1',
                  label: <div>{trans.common.notification} 1</div>,
                },
                {
                  key: '2',
                  label: <div>{trans.common.notification} 2 </div>,
                },
              ],
            }}
          >
            <Badge count={2}>
              <BellRingIcon />
            </Badge>
          </Dropdown>
          <Dropdown
            placement='bottomRight'
            menu={{
              items: [
                {
                  key: '1',
                  icon: <UserOutlined />,
                  label: <span>{trans.menu.header.AccountInfo}</span>,
                  onClick: () => router.push('/profile'),
                },
                {
                  key: '2',
                  icon: <LockOutlined />,
                  label: <span>{trans.menu.header.ChangePass}</span>,
                  onClick: () => router.push('/change-password'),
                },
                {
                  key: '3',
                  icon: <LogoutOutlined />,
                  label: <span>{trans.menu.header.Logout}</span>,
                  onClick: () => onActionClick(),
                },
              ],
            }}
          >
            <div className='flex items-center justify-center gap-1 cursor-pointer'>
              <Avatar>
                <UserIcon />
              </Avatar>
              <p className='ipad:hidden text-ellipsis truncate h-max leading-none'>{user && user.user?.fullName}</p>
            </div>
          </Dropdown>
        </div>
      </div>
    </Header>
  );
};

export default HeaderComponent;
