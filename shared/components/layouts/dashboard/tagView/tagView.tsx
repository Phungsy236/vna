import type { FC } from 'react';

import { addTag, removeTag, setActiveTag } from '@/shared/stores/tagViewSlice';
import { Tabs } from 'antd';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { useAppSelector } from '@/shared/hooks/useRedux';
import { useRouter } from 'next/router';
import TagsViewAction from './tagViewAction';
import { flatMenu, menuListMock } from '@/shared/utils/constants/appMenu';
import useTrans from '@/shared/hooks/useTrans';

const TagsView: FC = () => {
  const { tags, activeTagId } = useAppSelector(state => state.tagViewSlice);
  const dispatch = useDispatch();
  const { pathname, push } = useRouter()
  const { trans } = useTrans()

  // onClick tag
  const onChange = (key: string) => {
    const tag = tags.find(tag => tag.code === key);
    if (tag) {
      setCurrentTag(tag.code);
    }
  };

  // onRemove tag
  const onClose = (targetKey: string) => {
    dispatch(removeTag(targetKey));
  };

  const setCurrentTag = useCallback(
    (id?: string) => {
      const tag = tags.find(item => {
        if (id) {
          return item.code === id;
        } else {
          return item.code === pathname;
        }
      });

      if (tag) {
        dispatch(setActiveTag(tag.path));
      }
    },
    [dispatch, pathname, tags],
  );

  useEffect(() => {
    push(activeTagId);
  }, [activeTagId]);

  useEffect(() => {
    if (menuListMock.length) {
      const menu = flatMenu().find(m => m.path === pathname);
      if (menu) {
        const { Icon, ...rest } = menu
        dispatch(
          addTag({
            ...rest,
            closable: menu.code !== 'dashboard',
          }),
        );
      }
    }
  }, [dispatch, pathname]);

  return (
    <div id="pageTabs" style={{ padding: '6px 4px' }}>
      <Tabs
        tabBarGutter={6}
        tabBarStyle={{ margin: 0, fontWeight: 700 }}
        onChange={onChange}
        activeKey={activeTagId}
        type="editable-card"
        hideAdd
        onEdit={(targetKey, action) => action === 'remove' && onClose(targetKey as string)}
        tabBarExtraContent={<TagsViewAction />}
        items={tags.map(tag => {
          return {
            key: tag.code,
            //@ts-ignore
            label: trans.menu[tag.label],
            closable: tag.closable,

          };
        })}
      />
    </div>
  );
};

export default TagsView;
