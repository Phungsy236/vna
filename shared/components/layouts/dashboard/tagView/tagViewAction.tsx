import type { FC } from 'react';

import { SettingOutlined } from '@ant-design/icons';
import { Dropdown } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import useTrans from '@/shared/hooks/useTrans';

import { useAppSelector } from '@/shared/hooks/useRedux';
import { removeTag, removeOtherTag, removeAllTag } from '@/shared/stores/tagViewSlice';

const TagsViewAction: FC = () => {
  const { activeTagId } = useAppSelector(state => state.tagViewSlice);
  const dispatch = useDispatch();
  const { trans } = useTrans()

  return (
    <Dropdown
      menu={{
        items: [
          {
            key: '0',
            onClick: () => dispatch(removeTag(activeTagId)),
            label: trans.common.tabClose.currentTab,
          },
          {
            key: '1',
            onClick: () => dispatch(removeOtherTag()),
            label: trans.common.tabClose.otherTab,
          },
          {
            key: '2',
            onClick: () => dispatch(removeAllTag()),
            label: trans.common.tabClose.allTab,
          },
          {
            key: '3',
            type: 'divider',
          },

        ],
      }}
    >
      <span id="pageTabs-actions">
        <SettingOutlined className="tagsView-extra" />
      </span>
    </Dropdown>
  );
};

export default TagsViewAction;
