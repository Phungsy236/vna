import React from "react";
import { ConfigProvider } from "antd";
import { APP_THEME } from "../../../shared/utils/constants/appTheme";


const withTheme = (node: JSX.Element) => (
    <>
        <ConfigProvider
            theme={{ ...APP_THEME.theme.light }}
        >
            {node}
        </ConfigProvider>
    </>
)

export default withTheme;