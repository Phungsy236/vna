import { Form, Input, Select } from 'antd';
import React, { useState } from 'react'
import { useGetDistricts, useGetProvinces, useGetWards } from '../schema/models/IMetaData'
import useTrans from './useTrans';
import { FormInstance, useWatch } from 'antd/lib/form/Form';

type Props = {
    form: FormInstance
    provinceKeyName?: string,
    districtKeyName?: string,
    wardKeyName?: string,
    required?: boolean,
    initProvince?: number,
    initDistrict?: number,
}

export default function useLocationForm({ form, provinceKeyName, districtKeyName, wardKeyName, initProvince, initDistrict }: Props) {
    console.log(initProvince)
    const provinceId = useWatch(provinceKeyName || 'province', form)
    const districtId = useWatch(districtKeyName || 'district', form)


    const { data: provinces, isLoading: loadingProvinces } = useGetProvinces()
    const { data: districts, isLoading: loadingDistricts } = useGetDistricts(provinceId || initProvince, { enabled: !!provinceId || !!initProvince })
    const { data: wards, isLoading: loadingWards } = useGetWards(provinceId || initProvince, districtId || initDistrict, { enabled: !!districtId || !!initDistrict && !!provinceId || !!initProvince })
    const { trans } = useTrans()
    const FormLocation = () => <>
        <div className='flex gap-4 w-full'>
            <Form.Item name={provinceKeyName || "province"} className='w-2/3' label={trans.page.agent.agentInformation.province}
                rules={[{ required: true, message: trans.common.form.require }]} >
                <Select showSearch placeholder='province' options={provinces} loading={loadingProvinces} onChange={() => {
                    form.setFieldValue(provinceKeyName || "district", null)
                    form.setFieldValue(wardKeyName || "ward", null)
                }}
                    
                    optionFilterProp='label'

                />
            </Form.Item>
            <Form.Item name="zipCode" className='w-1/3' label={trans.page.agent.agentInformation.zipcode}
                rules={[{ required: true, message: trans.common.form.require }]}   >
                <Input placeholder='zipCode' />
            </Form.Item>
        </div>
        <Form.Item name={districtKeyName || "district"} label={trans.page.agent.agentInformation.district}
            rules={[{ required: true, message: trans.common.form.require }]} dependencies={['province']}>
            <Select showSearch placeholder='district' options={districts} loading={loadingDistricts && !!provinceId} onChange={() => {
                form.setFieldValue(wardKeyName || "ward", null)
            }}
                optionFilterProp='label' />
        </Form.Item>
        <Form.Item name={wardKeyName || "ward"} label={trans.page.agent.agentInformation.ward}
            rules={[{ required: true, message: trans.common.form.require }]} dependencies={['district', 'ward']}>
            <Select showSearch placeholder='ward' options={wards} loading={loadingWards && !!districtId} optionFilterProp='label' />
        </Form.Item>
    </>

    return { FormLocation }


}