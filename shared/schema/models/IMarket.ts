import React from "react";
import usePagination from "@/shared/hooks/usePagination";
import { UseQueryOptions, useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import useTrans from "@/shared/hooks/useTrans";
import { notification } from 'antd'
import { axiosInstanceDev } from "../typedef/Axios";
import { IBaseResponse, IBaseResponseWithCount } from "../typedef/IBaseResponse";
import { MAX_EXPORT_SIZE } from "@/shared/utils/constants/appContants";
import exportExcel from "@/shared/utils/functions/exportExel";

const QUERY_KEY_CHILDREN = 'getListMarket'
const QUERY_KEY_PARENT = 'getListGroupMarket'


export interface IGroupMarket {
    active?: any;
    isDelete: boolean;
    createdDate?: any;
    updatedDate: string;
    createdBy?: any;
    updatedBy?: any;
    id: string;
    groupMarketName: string;
    description: string;
    marketList: any[],
}
interface IGroupMarketCreateUpdate {
  groupMarketName: string;
  description: string;
  markets: IMarket[];
  active: boolean,
}
interface IMarketCreateUpdate {
  marketName: string;
  description: string;
  groupMarketDto: IGroupMarket[];
  active: boolean,
}
export interface IMarket {
  id: string;
  marketName: string;
  description: string;
  groupMarket: IGroupMarket;
  createdBy: string;
  updatedDate: string;
  updatedBy: string;
  active: boolean;
}


export const useGetBasicActiveListGroupMarket = () => {
    return useQuery({
        queryKey: [QUERY_KEY_PARENT, "basic-active"],
        queryFn: () => axiosInstanceDev.get<IBaseResponse<IGroupMarket[]>>('/group-market/get-all-active'),
        select(data) {
            const res = data.data
            const result = res.map(item => {
                return {
                    value: item.id,
                    label: item.groupMarketName
                }
            })
            return result
        },
    })
}

export const useGetListGroupMarket = () => {
    return usePagination<IBaseResponse<IBaseResponseWithCount<IGroupMarket[]>>>({
        queryKey: [QUERY_KEY_PARENT],
        apiFn: (params) => axiosInstanceDev.post<IBaseResponse<IBaseResponseWithCount<IGroupMarket[]>>>('/group-market/search', { ...params }),
        defaultParams: {
            page: 0,
            size: 10,
            sorts: [{ field: 'updatedDate', direction: 'DESC' }]
        },

    })
}

export const useGetDetailGroupMarket = ({ id, options }: { id: React.Key, options: Partial<UseQueryOptions> }) => {
    return useQuery({
        queryKey: [QUERY_KEY_PARENT, 'get-detail'],
        queryFn: () => axiosInstanceDev.get<IBaseResponse<IGroupMarket>>('/group-market/get-by-id/' + id),
        select(data) {
            return data.data
        },
        enabled: options?.enabled
    })
}

export const useCreateGroupMarket = (onSuccessHandle?: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: (groupMarket: IGroupMarketCreateUpdate) => axiosInstanceDev.post('/group-market/create', groupMarket),
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_PARENT] })
            notification.success({ message: "Tao moi group market thanh cong" })
            if (onSuccessHandle) onSuccessHandle()
        },
    })
}
export const useUpdateGroupMarket = (onSuccessHandle: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: ({ groupMarket, id }: { groupMarket: IGroupMarketCreateUpdate, id: React.Key }) => axiosInstanceDev.put('/group-market/update/' + id, groupMarket),
        onSuccess: () => {
            notification.success({ message: trans.common.notify.editSuccess(trans.page.CA._) })
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_PARENT] })
            if (onSuccessHandle) onSuccessHandle()
        },
    })
}

export const useDeleteGroupMarket = (onSuccessHandle?: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: ({ listIds }: { listIds: React.Key[] }) => axiosInstanceDev.put('/group-market/delete', { listIds }),
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_PARENT] })
            notification.success({ message: "Xoa group market thanh cong"  })
            if (onSuccessHandle) onSuccessHandle()
        },
        onError: (err) => {
            console.log(err)
            notification.error({ message: "Xoa group market khong thanh cong" })
        }
    })
}

export const useChangeStatusGroupMarket = (onSuccessHandle?: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: (id: React.Key) => axiosInstanceDev.put(`/group-market/change-status/${id}`,),
        onSuccess: () => {
            notification.success({ message: "Thay doi trang thai group market thanh cong" })
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_PARENT] })
            if (onSuccessHandle) onSuccessHandle()
        },
    })
}

export const useExportDataGroupMarketMutation = () => {
    return useMutation({
        mutationFn: (type: 'all' | 'current') => axiosInstanceDev.post<IBaseResponse<IBaseResponseWithCount<IGroupMarket[]>>>('/group-market/search', {
            page: 0,
            size: type === 'all' ? MAX_EXPORT_SIZE : 10,
            sorts: [{ field: 'updatedDate', direction: 'DESC' }],
            filters: [{ field: "isSubAgent", op: 'EQUAL', fieldType: 'BOOLEAN', value: true }]
        }),
        onSuccess: (data) => {
            exportExcel({ data: data.data.content, fileName: 'Dữ liệu group market' })
        }
    })
}

//// Above Market

export const useGetBasicActiveListMarket = () => {
    return useQuery({
        queryKey: [QUERY_KEY_CHILDREN, "basic-active"],
        queryFn: () => axiosInstanceDev.get<IBaseResponse<IMarket[]>>('/market/get-all-active'),
        select(data) {
            const res = data.data
            const result = res.map(item => {
                return {
                    value: item.id,
                    label: item.marketName
                }
            })
            return result
        },
    })
}

export const useGetListMarket = () => {
    return usePagination<IBaseResponse<IBaseResponseWithCount<IMarket[]>>>({
        queryKey: [QUERY_KEY_CHILDREN],
        apiFn: (params) => axiosInstanceDev.post<IBaseResponse<IBaseResponseWithCount<IMarket[]>>>('/market/search', { ...params }),
        defaultParams: {
            page: 0,
            size: 10,
            sorts: [{ field: 'updatedDate', direction: 'DESC' }]
        },

    })
}

export const useGetDetailMarket = ({ id, options }: { id: React.Key, options: Partial<UseQueryOptions> }) => {
    return useQuery({
        queryKey: [QUERY_KEY_CHILDREN, 'get-detail'],
        queryFn: () => axiosInstanceDev.get<IBaseResponse<IMarket>>('/market/get-by-id/' + id),
        select(data) {
            return data.data
        },
        enabled: options?.enabled
    })
}

export const useCreateMarket = (onSuccessHandle?: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: (market: IMarketCreateUpdate) => axiosInstanceDev.post('/market/create', market),
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_CHILDREN] })
            notification.success({ message: "Tao moi market thanh cong" })
            if (onSuccessHandle) onSuccessHandle()
        },
    })
}

export const useUpdateMarket = (onSuccessHandle: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: ({ market, id }: { market: IMarketCreateUpdate, id: React.Key }) => axiosInstanceDev.put('/market/update/' + id, market),
        onSuccess: () => {
            notification.success({ message: trans.common.notify.editSuccess(trans.page.CA._) })
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_CHILDREN] })
            if (onSuccessHandle) onSuccessHandle()
        },
    })
}

export const useDeleteMarket = (onSuccessHandle?: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: ({ listIds }: { listIds: React.Key[] }) => axiosInstanceDev.put('/market/delete', { listIds }),
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_CHILDREN] })
            notification.success({ message: "Xoa market thanh cong"  })
            if (onSuccessHandle) onSuccessHandle()
        },
        onError: (err) => {
            console.log(err)
            notification.error({ message: "Xoa market khong thanh cong" })
        }
    })
}

export const useChangeStatusMarket = (onSuccessHandle?: () => void) => {
    const queryClient = useQueryClient()
    const { trans } = useTrans()
    return useMutation({
        mutationFn: (id: React.Key) => axiosInstanceDev.put(`/market/change-status/${id}`,),
        onSuccess: () => {
            notification.success({ message: "Thay doi trang thai market thanh cong" })
            queryClient.invalidateQueries({ queryKey: [QUERY_KEY_CHILDREN] })
            if (onSuccessHandle) onSuccessHandle()
        },
    })
}


export const useExportDataMarketMutation = () => {
    return useMutation({
        mutationFn: (type: 'all' | 'current') => axiosInstanceDev.post<IBaseResponse<IBaseResponseWithCount<IGroupMarket[]>>>('/market/search', {
            page: 0,
            size: type === 'all' ? MAX_EXPORT_SIZE : 10,
            sorts: [{ field: 'updatedDate', direction: 'DESC' }],
            filters: [{ field: "isSubAgent", op: 'EQUAL', fieldType: 'BOOLEAN', value: true }]
        }),
        onSuccess: (data) => {
            exportExcel({ data: data.data.content, fileName: 'Dữ liệu market' })
        }
    })
}


