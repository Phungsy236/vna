export interface IFlightSearch {
    flights:
    {
        startLocation: any,
        endLocation: any,
        startTime: string,
    }[],
    startLocation: any,
    endLocation: any,
    startTime: Date | undefined,
    endTime: Date | undefined,
    numberAdult: number,
    numberChildren: number,
    numberInfant: number
}