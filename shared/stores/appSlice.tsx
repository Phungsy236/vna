import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { IInforUser } from '../schema/models/IAppUser';
import { APP_ROLE } from '../utils/constants/appRole';
import { deleteCookie, getCookie } from 'cookies-next';
import { APP_SAVE_KEY } from '../utils/constants/appContants';


type APPSTATE = {
    user: IInforUser | undefined,
    isLogined: boolean,
    isCollapseMenu: boolean,
    isRouteLoading: boolean,
}

const initialState: APPSTATE = {
    user: undefined,
    isLogined: false,
    isCollapseMenu: false,
    isRouteLoading: false
}
export const appSlice = createSlice({
    name: 'appSlice',
    initialState,
    reducers: {
        login: (state, action: PayloadAction<any | undefined>) => {
            state.user = action.payload
            state.isLogined = true
        },
        logout: (state) => {
            state.user = undefined
            state.isLogined = false
            deleteCookie(APP_SAVE_KEY.REFRESH_TOKEN_KEY)
            deleteCookie(APP_SAVE_KEY.TOKEN_KEY)
            deleteCookie(APP_SAVE_KEY.LOGIN_STATUS)
        },
        toggleMenu: (state, action: PayloadAction<boolean>) => {
            state.isCollapseMenu = action.payload
        },
        setLoading: (state, action: PayloadAction<boolean>) => {
            state.isRouteLoading = action.payload
        }
        // useGetInfoByToken: (state) => {
        //     if (getCookie(APP_SAVE_KEY.LOGIN_STATUS) === "true" && state.isLogined === false) {

        //     }
        // }

    }
}
)
export const { login, logout, toggleMenu, setLoading } = appSlice.actions
export default appSlice.reducer
