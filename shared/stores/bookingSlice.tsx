import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { FLIGHTTYPE } from '../schema/typedef/IFlightType';
import { IFlightSearch } from '../schema/typedef/IFlightSearch';
import dataFake from '@/shared/mocks/db.json'
import { BOOKING_STATE, Flight } from '../schema/models/IFlight';



const initialState: BOOKING_STATE = {
    currentStep: 1,
    type: 'oneWay',
    valueSearch: {
        numberAdult: 1,
        numberChildren: 0,
        numberInfant: 0,
        flights: [
            { startLocation: null, endLocation: null, startTime: new Date().toISOString() },
            { startLocation: null, endLocation: null, startTime: new Date().toISOString() }
        ]
    },
    isSearched: false,
    resultSearch: dataFake.flights as any,
    chooseFlights: undefined,
    traveler: []

}
export const bookingSlice = createSlice({
    name: 'bookingSlice',
    initialState,
    reducers: {
        setStep: (state, action: PayloadAction<number>) => {
            state.currentStep = action.payload
        },
        nextStep: (state) => {
            state.currentStep += 1
        },
        prevStep: (state) => {
            state.currentStep -= 1
        },
        setTypeFlight: (state, action: PayloadAction<FLIGHTTYPE>) => {
            state.type = action.payload
        },
        setSearchValue: (state, action: PayloadAction<BOOKING_STATE['valueSearch']>) => {
            state.valueSearch = action.payload
            state.isSearched = true
        },

        addFare: (state, action: PayloadAction<any>) => {
            //TODO : check type if type is oneWay ==> show confirm,
            // if rounTrip ==> result search screen
            // if multiStop ==> result search screen until match number stop
        },
        removeFare: (state, action: PayloadAction<any>) => {
            // state.chooseFlight = undefined
        },
        updateAdult: () => { },
        updateChildren: () => { },
        updateInfant: () => { }


    }
}
)
export const { nextStep, prevStep, setTypeFlight, setSearchValue, addFare, removeFare, updateInfant, updateChildren, updateAdult, setStep } = bookingSlice.actions
export default bookingSlice.reducer
