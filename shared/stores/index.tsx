import appSlice from './appSlice'
import { configureStore } from '@reduxjs/toolkit'
import { enableMapSet } from 'immer'
import tagViewSlice from './tagViewSlice'
import bookingSlice from './bookingSlice'
import roleSlice from './roleSlice'

enableMapSet()
export const store = configureStore({
    reducer: {
        appSlice: appSlice,
        tagViewSlice: tagViewSlice,
        bookingSlice: bookingSlice,
        roleSlice: roleSlice
    }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
