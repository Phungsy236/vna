import type { PayloadAction } from '@reduxjs/toolkit';

import { createSlice } from '@reduxjs/toolkit';

const DASHBOARDKEY = '/'

export type TagItem = {
    code: string;
    label: string
    path: string;
    closable: boolean;
};

export interface TagState {
    tags: TagItem[];
    activeTagId: TagItem['code'];
}

const initialState: TagState = {
    activeTagId: '',
    tags: [],
};

const tagsViewSlice = createSlice({
    name: 'tagsView',
    initialState,
    reducers: {
        setActiveTag(state, action: PayloadAction<string>) {
            state.activeTagId = action.payload;
        },
        addTag(state, action: PayloadAction<TagItem>) {
            if (!state.tags.find(tag => tag.code === action.payload.code)) {
                state.tags.push(action.payload);
            }
            state.activeTagId = action.payload.code;
        },
        updateTagPath(state, action: PayloadAction<{code:string , path : string}>) {
            const index = state.tags.findIndex(tag => tag.code === action.payload.code)
            if (index !== -1) {
                state.tags[index].path = action.payload.path
            }
        },
        removeTag(state, action: PayloadAction<string>) {
            const targetKey = action.payload;
            // dashboard cloud't be closed
            if (targetKey === DASHBOARDKEY || state.tags.length === 1) {
                return;
            }

            const activeTagId = state.activeTagId;
            let lastIndex = 0;
            state.tags.forEach((tag, i) => {
                if (tag.path === targetKey) {
                    state.tags.splice(i, 1);
                    lastIndex = i - 1;
                }
            });
            const tagList = state.tags.filter(tag => tag.code !== targetKey);
            if (tagList.length && activeTagId === targetKey) {
                if (lastIndex >= 0) {
                    state.activeTagId = tagList[lastIndex].code;
                } else {
                    state.activeTagId = tagList[0].path;
                }
            }
        },
        removeAllTag(state) {
            state.activeTagId = DASHBOARDKEY;
            state.tags = state.tags.filter(item => item.code === DASHBOARDKEY);
        },

        removeOtherTag(state) {
            const dashboardTab = state.tags.filter(item => item.code === DASHBOARDKEY)
            const activeTag = state.tags.find(tag => tag.path === state.activeTagId);
            const activeIsDashboard = activeTag!.code === DASHBOARDKEY;
            state.tags = activeIsDashboard ? dashboardTab : [...dashboardTab, activeTag!];
        },
    },
});

export const { setActiveTag, addTag, removeTag, removeAllTag, removeOtherTag , updateTagPath } = tagsViewSlice.actions;

export default tagsViewSlice.reducer;
