export const APP_BOOKING = {
    TYPE: {
        ONE_WAY: 'oneway',

    },
    MAX_PASSENGER: 9,
    FORM_SEARCH_SHAPE: {
        START_LOCATION: 'startLocation',
        END_LOCATION: 'endLocation',
        START_DATE: 'startDate',
        END_DATE: 'endDate',
        NUMBER_CHILDREN: 'numberChildren',
        NUMBER_ADULT: 'numberAdult',
        NUMBER_INFANT: 'numberInfant',
        FLIGHT_CLASS: 'flightClass'
    },
    FLIGHT_CLASS: [
        // value equal index of item in array
        {
            label: 'economy',
            value: 0
        },
        {
            label: 'deluxe economy',
            value: 1
        },
        {
            label: 'business',
            value: 2
        }
    ]


}