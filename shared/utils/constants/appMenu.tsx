import AddOnServiceIcon from '@/shared/components/icons/AddOnServiceIcon';
import BookingIcon from '@/shared/components/icons/BookingIcon';
import BookingManageIcon from '@/shared/components/icons/BookingManageIcon';
import FundsTransferIcon from '@/shared/components/icons/FundsTransferIcon';
import HomeIcon from '@/shared/components/icons/HomeIcon';
import NewsManageIcon from '@/shared/components/icons/NewsManageIcon';
import PointManageIcon from '@/shared/components/icons/PointManageIcon';
import ReportIcon from '@/shared/components/icons/ReportIcon';
import SystemManageIcon from '@/shared/components/icons/SystemManageIcon';
import { MenuItem, MenuList } from '@/shared/components/layouts/dashboard/menu';
import { PERMISSION_CODES } from './appContants';

export const URLS = {
  HOME: '/',
  BOOKING: '/booking',
  ADD_ON_SERVICE: '/add-on-service',
  TOP_UP: '/top-up',
  BOOKING_MANAGE: '/booking-management',
  POINT_MANAGE: '/point-management',
  REPORT: {
    _: '/report',
    CONFIG_TIMEZONE: '/config-timezone',
    AGENT: '/agent',
    CORPORATE_ACCOUNT: '/corporate-account',
  },
  FUNDS_TRANSFER: '/funds-transfer',
  NEWS: {
    _: '/news',
    CATEGORY: '/news/catalogue-manage',
    ARTICLE: '/news/article-manage',
  },
  SYSTEM_MANAGE: {
    _: '/system-management',
    USER_MANAGE: '/system-management/user-management',
    AGENT_MANAGE: '/system-management/agent-management',
    AGENT_MANAGE_CREATE: '/system-management/agent-management/create',
    SUB_AGENT_MANAGE: '/system-management/sub-agent-management',
    CA_MANAGE: '/system-management/ca-management',
    MARKET_MANAGE: '/system-management/market-management',
    ROLE_MANAGE: '/system-management/role-management',
    TOP_UP_MANAGE: '/system-management/top-up-management',
    REPORTS: '/system-management/reports',
  },
  CA_MANAGE: '/ca-management',
  ADMINISTRATIVE_REPORT: '/administrative-report',
  OPERATIONAL_REPORT: '/operational-report',
};

export const menuListMock: MenuItem[] = [
  {
    code: 'dashboard',
    label: 'home',
    Icon: <HomeIcon />,
    path: URLS.HOME,
  },
  { code: 'news', label: 'news', Icon: <NewsManageIcon />, path: URLS.NEWS._ },
  {
    code: 'booking',
    label: 'booking',
    Icon: <BookingIcon />,
    path: URLS.BOOKING,
  },
  {
    code: 'add-on-service',
    label: 'addOnService',
    Icon: <AddOnServiceIcon />,
    path: URLS.ADD_ON_SERVICE,
  },
  {
    code: 'booking-management',
    label: 'bookingManagement',
    Icon: <BookingManageIcon />,
    path: URLS.BOOKING_MANAGE,
  },
  {
    code: 'point-management',
    label: 'pointManagement',
    Icon: <PointManageIcon />,
    path: URLS.POINT_MANAGE,
  },
  {
    code: 'report',
    label: 'report',
    Icon: <ReportIcon />,
    path: URLS.REPORT._,
    children: [
      {
        code: 'config-timezone',
        label: 'configTimezone',
        path: URLS.REPORT.CONFIG_TIMEZONE,
      },
      {
        code: 'agent',
        label: 'agent',
        path: URLS.REPORT.AGENT,
      },
      {
        code: 'corporate-account',
        label: 'corporateAccount',
        path: URLS.REPORT.CORPORATE_ACCOUNT,
      },
    ],
  },
  {
    code: 'funds-transfer',
    label: 'fundsTransfer',
    Icon: <FundsTransferIcon />,
    path: URLS.FUNDS_TRANSFER,
  },
  {
    code: 'market-mangement',
    label: 'marketManagement',
    Icon: <FundsTransferIcon />,
    path: URLS.SYSTEM_MANAGE.MARKET_MANAGE,
  },
  {
    code: 'news',
    label: 'newsManagement',
    Icon: <NewsManageIcon />,
    path: URLS.NEWS._,
    children: [
      {
        code: 'news',
        label: 'articleManagement',
        path: URLS.NEWS.ARTICLE,
      },
      {
        code: 'news',
        label: 'catalogueManagement',
        path: URLS.NEWS.CATEGORY,
      },
    ],
  },
  {
    code: 'system-management',
    label: 'systemManagement',
    Icon: <SystemManageIcon />,
    path: URLS.SYSTEM_MANAGE._,
    children: [
      {
        code: 'user-management',
        label: 'userManagement',
        path: URLS.SYSTEM_MANAGE.USER_MANAGE,
        permission: PERMISSION_CODES.USER_MANAGE,
      },
      {
        code: 'agent-management',
        label: 'agentManagement',
        path: URLS.SYSTEM_MANAGE.AGENT_MANAGE,
        permission: PERMISSION_CODES.AGENT_MANAGE,
      },
      {
        code: 'sub-agent-management',
        label: 'subAgentManagement',
        path: URLS.SYSTEM_MANAGE.SUB_AGENT_MANAGE,
        permission: PERMISSION_CODES.SUBAGENT_MANAGE,
      },
      {
        code: 'ca-management',
        label: 'caManagement',
        path: URLS.SYSTEM_MANAGE.CA_MANAGE,
        permission: PERMISSION_CODES.CA_MANAGE,
      },
      {
        code: 'role-management',
        label: 'roleManagement',
        path: URLS.SYSTEM_MANAGE.ROLE_MANAGE,
        permission: PERMISSION_CODES.ROLE_MANAGE,
      },
      // {
      //     code: 'role-create',
      //     label: "roleCreate",
      //     path: URLS.SYSTEM_MANAGE.ROLE_CREATE,
      //     isHide: true
      // },

      {
        code: 'top-up-management',
        label: 'topUpManagement',
        path: URLS.SYSTEM_MANAGE.TOP_UP_MANAGE,
      },
      {
        code: 'reports',
        label: 'reports',
        path: URLS.SYSTEM_MANAGE.REPORTS,
      },
    ],
  },
];
export function flatMenu() {
  let result: MenuItem[] | [] = [];
  menuListMock.forEach(menu => {
    const { children, ...rest } = menu;
    result = [...result, rest];
    if (children) {
      children.forEach(child => {
        result = [...result, child];
      });
    }
  });
  return result;
}
