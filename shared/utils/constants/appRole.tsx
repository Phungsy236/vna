export enum APP_ROLE {
    SYS_ADMIN = 'systemadmin',
    USER = 'user',
}