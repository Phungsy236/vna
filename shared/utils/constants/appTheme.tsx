import { ThemeConfig } from "antd";



export const APP_THEME = {
    color: {
        PRIMARYCOLOR: '#016390'
    },
    theme: {
        light: {
            token: {
                colorPrimary: '#016390',
            },
            components: {
                Button: {
                    borderRadius: 2,
                    padding: 16,
                    controlHeight: 40,
                },
              
            }
        } as ThemeConfig,
        dark: {
            token: {
                colorPrimary: '#F3732A',
               
            },
            components: {
                Button: {
                    borderRadius: 2,
                    padding: 16,
                    controlHeight: 40
                }
               
            }
        } as ThemeConfig
    }
}