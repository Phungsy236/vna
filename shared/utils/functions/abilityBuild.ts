import { PermissionAction } from '@/shared/schema/models/IAppUser';
import { AbilityBuilder, Ability } from '@casl/ability'

type Actions = 'create' | 'edit' | 'view' | 'delete' | 'approve';
export default function AbilityConfig(permissions?: PermissionAction[] | undefined) {
    const { can, build } = new AbilityBuilder(Ability<[Actions, any]>);
    if (permissions) {
        permissions.filter(item => item.active && item.permissionCode).forEach(per => can(per.action.toLowerCase() as Actions, per.permissionCode));
    }
    return build();
}
