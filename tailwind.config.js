/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx,mdx}', './shared/components/**/*.{js,ts,jsx,tsx,mdx}'],
  theme: {
    extend: {
      
      screens: {
        mobile: { min: '350px', max: '767px' },
        // => @media (min-width: 350px and max-width: 767px) { ... }

        ipad: { min: '768px', max: '1023px' },
        // => @media (min-width: 768px and max-width: 1023px) { ... }

        tablet: { min: '1024px', max: '1279px' },
        // => @media (min-width: 1024px and max-width: 1279px) { ... }

        desktop: { min: '1280px' },
        // => @media (min-width: 1280px and max-width: 1535px) { ... }

        // monitor: { min: '1536px' },
        // // => @media (min-width: 1536px) { ... }
      },
    },
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  },
};
